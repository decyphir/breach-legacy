# README #

Breach is a Matlab toolbox for simulation-based design of dynamical/CPS/Hybrid systems.

## Install
- Setup a C/C++ compiler using mex -setup
- Run InstallBreach
- Add/save path to Breach folder 
- Run InitBreach 

## Getting Started

Type 'BrDemo.' and Tab to get a list of available demo scripts. Corresponding documentation can be found in

Doc/pdf
Doc/html

Also consult Examples/README for some details about available examples (not all working/up-to-date). 

## Contact/Support

Contact alex at decyphir dot com to report bugs, 
for help, complaints, support, etc. 
