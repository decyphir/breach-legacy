function [t, X] = read_trace(fname)
  
  xDoc = xmlread(fname);
  lvdataNode = xDoc.getDocumentElement;
  
  % get trace array
  
  ta = lvdataNode.getFirstChild; 
  ta = getNextSiblingByName(ta, 'Array');

  % number of elements in the trace
  nb_el_node = ta.getFirstChild;
  nb_el_node = getNextSiblingByName(nb_el_node, 'Dimsize');
 
  nb_el = str2num(nb_el_node.getTextContent);
   
  %nb_el = min(nb_el,10);
  el = ta.getFirstChild;
 
  X = zeros(12, nb_el);
  for i = 1:nb_el
    el = getNextSiblingByName(el,'Cluster');
    [tc, Xc] = read_trace_elem(el);
    t(i) = tc;
    X(:,i)= Xc;
  end
  
  
 
