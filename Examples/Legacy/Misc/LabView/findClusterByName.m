function node = findClusterByName(node, st)

clusters = node.getElementsByTag('cluster');
nb_clu = clusters.getLength;
node =[];

for i = 1:nb_clu;
  
  name = hasName(clusters.item(i-1));
  if strcmp(name, st)
    node = clusters.item(i-1);
  end
  
end


