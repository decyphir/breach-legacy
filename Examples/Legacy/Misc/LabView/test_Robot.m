% Initialize a system with 10 parameters
Init_Robot;

% creates nominal parameter set and trajectory
P = CreateParamSet(Sys);
Pf = ComputeTraj(Sys,P, 0:.01:10);

% plots nominal trajectory
figure;
SplotVar(Pf);

