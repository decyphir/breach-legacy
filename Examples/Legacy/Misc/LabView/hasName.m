function name=hasName(lvnode)

name = '';
node_name = lvnode.getFirstChild;

if ~isempty(node_name)
  node_name = getNextSiblingByName(node_name, 'Name');

  if (~isempty(node_name))
    name = char(node_name.getTextContent);
  end
end
