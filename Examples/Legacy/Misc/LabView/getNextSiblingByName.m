function node = getNextSiblingByName(node, name)

node = node.getNextSibling;
while (~strcmp(node.getNodeName, name))
  node = node.getNextSibling;
end
