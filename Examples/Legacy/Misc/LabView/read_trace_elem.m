function [t, x] = read_trace_elem(el)

c =  el.getFirstChild;
c =  getNextSiblingByName(c, 'NumElts');
%nb_el = str2double(c.getTextContent);
c = c.getNextSibling;

while (~isempty(c))
  name = hasName(c);
  if (~isempty(name))
    if regexp(name, 'Position')
      xpos = read_position(c);
    elseif regexp(name, 'Rotation')
      xrot = read_rotation(c);
    elseif regexp(name, 'Time')
      t = read_time(c);
    end
  end
  c = c.getNextSibling;
end

x = [xpos'; xrot'];


function xpos = read_position(node)

val_items = node.getElementsByTagName('Val');

xpos(1) = str2double(val_items.item(0).getTextContent);
xpos(2) = str2double(val_items.item(1).getTextContent);
xpos(3) = str2double(val_items.item(2).getTextContent);

function xrot = read_rotation(node)

val_items = node.getElementsByTagName('Val');

for i=1:9
  xrot(i) = str2double(val_items.item(i-1).getTextContent);
end

function t = read_time(node)

val_items = node.getElementsByTagName('Val');
t = str2double(val_items.item(0).getTextContent);
