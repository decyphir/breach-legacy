name ='Robot';
signals = {'x', 'y', 'z', 'th11','th21','th31','th21','th22','th23','th31','th32','th33'};

params = {'x0','y0','z0'};
p0 =  [0 0 0];

Sys = CreateExternSystem(name, signals, params, p0, @sim_Robot);
Sys.tspan = 0:.01:10; 
