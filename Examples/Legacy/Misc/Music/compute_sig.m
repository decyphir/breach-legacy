function [sig_om sig_t t win_t] =  compute_sig(win, Dt,Ts)
  %   
  %  compute_sig(win, Dt,Ts) compute the sizes of Heisenberg boxes for window win 
  %
    
  if (nargin==2)
    Ts=1/44100;
  end;
  t = -Dt/2:Ts:Dt/2;     
  win_t = MakeWindow(win, (Dt/Ts-1)/2);
  
  if (numel(win_t)<numel(t))
    t = t(1:numel(win_t));
  else
    win_t = win_t(1:numel(t));
  end
  
  sig_t = sqrt(sum((t.^2).*win_t*Ts));
  sig_om = 1/(2*sig_t);
