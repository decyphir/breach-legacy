t = 0:1/44100:2;

f1 = 10;
f2 = 50;
f3 = 300;

t1 =0;
t2 =1;
t3 =2;

s1p = sin(2*pi*f1/10*t);
s2p = .8*sin(2*pi*f2/10*t);
s3p = .3*sin(2*pi*f3/10*t);

s1 = sin(2*pi*f1*t);
s2 = .8*sin(2*pi*f2*t);
s3 = .3*sin(2*pi*f3*t);

subplot(5,2,3);
plot(t,s1p);
set(gca, 'YTick',[], 'XTick', [], 'YLim', [-1 1]);

subplot(5,2,5);
plot(t,s2p);
set(gca, 'YTick',[], 'XTick', [],'YLim', [-1 1]);

subplot(5,2,7);
plot(t,s3p);
set(gca, 'YTick',[], 'XTick', [],'YLim', [-1 1]);

subplot(5,2, [1 2]);
s = s1+s2+s3;
sp = s1p+s2p+s3p;
plot(t, sp);
set(gca,  'XTick', [], 'YTick', [], 'YLim', [-2.5 2.5]);

nu = 0:.1:320;
fs = wfft44(s, 1.5, 0, nu );
subplot(5, 2, [4 6 8]);
plot(nu, fs, 'LineWidth', 1);
set(gca, 'XLim',[0 320]);

pos = get(gca, 'Position');
pos(4) = .9* pos(4);
set(gca, 'YTick',[], 'XTick', [f1 f2 f3], 'XTickLabel', {'','',''}, 'Position', pos );

subplot(5,2, [9 10]);
sp = s1p+s2p;
plot(t, sp);
set(gca,  'XTick', [], 'YTick', [], 'YLim', [-2.5 2.5]);


%set(gcf,'PaperPosition', [0 0 15 11]);
%print(gcf, '-depsc', 'fig_fourier_for_dummies.eps');

