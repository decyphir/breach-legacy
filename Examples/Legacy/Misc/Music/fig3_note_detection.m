%  Script for testing frequency tolerance (E. Bartocci)
%
%   Note: this script uses an older version of the pitch routine (pitch_f)
%   kept here to reproduce fig 3 in the paper 
%  

spectrum = linspace(fE, fFS, 100);
max_power = linspace(0, 0, 100);

for i=1:numel(spectrum)   
    max_power(i) = max(pitch_f(single_F4, spectrum(i)));
end

close all;

figure
plot(spectrum, max_power, 'b', 'LineWidth', 2);
%save('freq_tol1.mat', 'spectrum');
%save('freq_tol2.mat', 'max_power');

xlabel('frequency (Hz)');
ylabel('max spectogram value');

figure
hold on

pitch_fE  =  pitch_f(single_F4, fE);
pitch_fF  =  pitch_f(single_F4, fF);
pitch_fFS =  pitch_f(single_F4, fFS);

time = 0:Ts:Ts*(numel(single_F4)-1);

plot(time, pitch_fE,  'r', 'LineWidth', 2);
plot(time, pitch_fF,  'k', 'LineWidth', 2);
plot(time, pitch_fFS, 'b', 'LineWidth', 2);

hold off

legend('E4','F4', 'FS4');
xlabel('time (sec)');

figure

traj = PF4.traj; 

ind_thF  = FindParam(Sys, 'thF'); % threshold for F
traj.param(ind_thF) =  0.005;  

F4    = 'pitch_f(s[t],fF)  > thF';
E4    = 'pitch_f(s[t],fE)  > thF';
FS4   = 'pitch_f(s[t],fFS) > thF';

STL_Formula('mF4' , F4);
STL_Formula('mE4' , E4);
STL_Formula('mFS4', FS4);


val  = STL_Eval(Sys, mF4,  PF4, traj);
val1 = STL_Eval(Sys, mE4, PF4, traj);
val2 = STL_Eval(Sys, mFS4, PF4,  traj);

bool_val  = val>0;
bool_val1 = val1>0;
bool_val2 = val2>0;

subplot(2,1,1);
plot(traj.time, traj.X, 'c');
title('Signal s[t] and boolean detection of notes ');    
% superpose the boolean interpretation of the predicate
hold on;
plot(traj.time, bool_val, 'k', 'LineWidth',2);    
plot(traj.time, bool_val1, 'b', 'LineWidth',2);   
plot(traj.time, bool_val2- 0.02, 'r', 'LineWidth',2);   
axis([0 .5 -1.1 1.1]);

subplot(2,1,2);
plot(traj.time, val, 'c', 'LineWidth',1.5);
hold on;     
M = max(abs(val));
plot(traj.time, bool_val*M/2,  'k', 'LineWidth',2);    
plot(traj.time, bool_val1*M/2, 'b', 'LineWidth',2);
plot(traj.time, bool_val1*M/2 - 0.0001, 'r', 'LineWidth',2);

title(['Quantitative/Boolean satisfaction of predicate mF4' ]);


