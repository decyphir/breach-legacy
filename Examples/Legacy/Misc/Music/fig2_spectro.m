close all;

Ts = 1/44100;
t1 = 0:Ts:1;
t2 = 0:Ts:1.5; 
t3 = 0:Ts:1;

tsil = 0:Ts:.5;
sil = zeros(size(tsil));

tall = 0:Ts:(0.5+1+.5+1.5+.5+1+.5)+6*Ts;

f1 = 10;
f2 = 20;
f3 = 40;

f1p = 5;
f2p = 10;
f3p = 40;

s1 = sin(2*pi*f1*t1);
s2 = .8*sin(2*pi*f2*t2);
s3 = .3*sin(2*pi*f3*t3) + s1 ;


s1p = sin(2*pi*f1p*t1);
s2p = .8*sin(2*pi*f2p*t2);
s3p = .3*sin(2*pi*f3p*t3) + s1 ;

s1p = sin(2*pi*f1p*t1);
s2p = .8*sin(2*pi*f2p*t2);
s3p = .3*sin(2*pi*f3p*t3) + s1p ;

s = [sil s1 sil  s2 sil s3 sil];

sp = [sil s1p sil  s2p sil s3p sil];

fs= 14; % Font Size
lw = 1; % Line Width
lwa =1; % for axis


%%% Plot Signal 

subplot(5,4, [2 3 4])
plot(tall, sp,'LineWidth',lw);
grid on;
axis tight
set(gca, 'XLim', [0 5.5], 'FontSize', fs, 'LineWidth',lwa);

Dt = 1;
T0 = 0:.01:numel(s)*Ts; 
nu = 0:.1:50;
tic 
[Hx TT NNU]  = wfft44(s,Dt, T0, nu,2^15 );
toc


%%% Plot Spectrogram 

subplot(5,4,[6 7 8 10 11 12 14 15 16]);
h = surf(TT,NNU, Hx', 'EdgeColor', 'None');

h = contour(TT,NNU, Hx', [ 0.02:.005:.055 .06:.02:.2] );
grid on;
%colorbar;
%axis tight;

set(gca, 'XLim', [0 5.5], 'FontSize', fs);
view(0,90);

%%% plot Fourier Transform

[nuft ft] = fft44(s, nu(end));
subplot(5,4,[5 9 13]);
plot(ft*Ts, nuft ,'LineWidth',lw);
set(gca, 'XLim', [0 1.5], 'FontSize', fs,'LineWidth',lwa);
grid on;


%%% plot predicate  
hx1  = wfft44(s,Dt, T0, f1);

thrhld = max(hx1)/2;


subplot(5,4, [18 19 20]);
plot(T0, hx1,'LineWidth',lw) 
hold on;
plot(T0, thrhld*ones(size(T0)),'--','LineWidth',lw);
plot(T0, thrhld*((hx1)>thrhld),'k-','LineWidth',2*lw);
grid on;
%axis tight;
set(gca, 'XLim', [0 5.5], 'YLim', [-.01 0.25],  'FontSize', fs,'LineWidth',lwa);




pos = get(gcf, 'PaperPosition');
pos(4) = 1.5*pos(4);
set(gcf,'PaperPosition', pos);
print(gcf, '-depsc', 'fig_spectrogram.eps');
