
%% Notes

STL_Formula('A', 'pitch(s[t],fA) > thA'); 
STL_Formula('B', 'pitch(s[t],fB) > thB'); 
STL_Formula('C', 'pitch(s[t],fC) > thC'); 
STL_Formula('D', 'pitch(s[t],fD) > thD'); 
STL_Formula('E', 'pitch(s[t],fE) > thE'); 
STL_Formula('F', 'pitch(s[t],fF) > thF'); 
STL_Formula('G', 'pitch(s[t],fG) > thG'); 

%% silence

STL_Formula('mute','(ev_[0,p2] (alw_[0, p2] (abs(s[t]) < p3))) or (alw_[0, p2] (abs(s[t-2*p2]) < p3))');

%% formulae

STL_Formula('phi1', 'alw_[0,2] (C) ');
STL_Formula('phi2', 'alw_[0,1] (D) ');
STL_Formula('phi3', '(alw_[0,1] (C)) and (alw_[2,3] (D)) ');
STL_Formula('phi4', 'C until (D until (E until (F until (G until (mute until (A until (B)))))))');
