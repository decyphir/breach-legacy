init_examples;
close all;


% plot signals
figure;
subplot(2,1,1);
plot(traj_violin.time, traj_violin.X);
title('violin');
axis([0 6.5 -1.1 1.1]);

subplot(2,1,2)
plot(traj_org.time, traj_org.X);
title('organ');
axis([0 6.5 -1.1 1.1]);

% formula 1

figure
violin_val  = STL_Eval(Sys, phi1, Pviol, traj_violin);
org_val  = STL_Eval(Sys, phi1,Porg, traj_org);

subplot(2,1,1);
plot(time, violin_val>0,'LineWidth', 2 );   
title(disp(phi1), 'Interpreter','None');
axis([0 6.5 -1.1 1.1]);

subplot(2,1,2);
plot(time, org_val>0,'LineWidth', 2 );   
axis([0 6.5 -1.1 1.1]);


% formula 2

figure
violin_val  = STL_Eval(Sys, phi2, Pviol, traj_violin);
org_val  = STL_Eval(Sys, phi2, Porg,  traj_org);

subplot(2,1,1);
plot(time, violin_val>0,'LineWidth', 2 );   
title(disp(phi2), 'Interpreter','None');
axis([0 6.5 -1.1 1.1]);

subplot(2,1,2);
plot(time, org_val>0,'LineWidth', 2 );   
axis([0 6.5 -1.1 1.1]);

% formula 3

figure
violin_val  = STL_Eval(Sys, phi3,Pviol, traj_violin);
org_val  = STL_Eval(Sys, phi3, Porg, traj_org);

subplot(2,1,1);
plot(time, violin_val>0,'LineWidth', 2 );   
title(disp(phi3), 'Interpreter','None');
axis([0 6.5 -1.1 1.1]);

subplot(2,1,2);
plot(time, org_val>0,'LineWidth', 2 );   
axis([0 6.5 -1.1 1.1]);

% formula 4

figure
violin_val  = STL_Eval(Sys, phi4, Pviol, traj_violin);
org_val  = STL_Eval(Sys, phi4, Porg, traj_org);

subplot(2,1,1);
plot(time, violin_val>0,'LineWidth', 2 );   
title('C until D until E until F until G until mute until A until B');
axis([0 6.5 -1.1 1.1]);

subplot(2,1,2);
plot(time, org_val>0,'LineWidth', 2 );   
axis([0 6.5 -1.1 1.1]);
