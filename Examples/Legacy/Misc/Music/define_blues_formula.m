%
% Defines a simplified blues formula
%

STL_Formula('E','pitch(s[t], fE) > thE' );
STL_Formula('A','pitch(s[t], fA) > thA' );
STL_Formula('B','pitch(s[t], fB) > thB' );

STL_Formula('phi_blues', '(E) and ((ev_[4*p0, 6*p0] (A)) and (ev_[8*p0, 9*p0] ((B) and (ev_[0, 2*p0] ((A) and (ev_[0, 2*p0] (E)))))))');

STL_Formula('evA', 'ev_[4*p0, 6*p0] (A)');
STL_Formula('turn', 'ev_[8*p0, 9*p0] ((B) and (ev_[0, 2*p0] ((A) and (ev_[0, 2*p0] (E)))))');

%save('Music_Example_properties.mat','-append', 'phi_blues');
