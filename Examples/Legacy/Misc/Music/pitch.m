function y = pitch(x, f1)
%  
% PITCH Computes the windowed power spectrum of a signal around a given
%       frequency for note detection purposes
%
%   y = pitch(x, f0)
%       - x  : the signal to be analyzed
%       - f0 : nominal frequency to be detectable
   
  %  Average and normalize     
  av = mean(x);
  if (av~=0) 
    x = x-av;
  end
    
  M = max(abs(x));
  if (M~=1)
    x = x/M;
  end
    
  % Compute window size 
  Ts = 1/44100;
  f0 = f1 * 2^(-1/12);
  window_size = (4*44100)/abs(f0-f1);
  mnd = window_size  * Ts;
  Tx = 0:Ts:(numel(x)-1)*Ts;
  Tmax = Tx(end);

  T0 = 0:mnd/8:Tmax;
  Nu = wfft44(x, mnd, T0, f1);
  y = interp1(T0,Nu,Tx);
   
  inan = isnan(y);
  y(inan) = 0;
 
end
  
