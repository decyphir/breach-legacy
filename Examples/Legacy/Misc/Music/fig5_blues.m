%  Script for testing blues pattern in music (E. Bartocci, A. Donze')
%
%  This script is tryind to find the pattern E3 E3 E3 E3 A3 E3 B3 A3 E3
%  
%

% init note model and breach system

init_examples;


valE = STL_Eval(Sys,E,Pblues, traj);
valB = STL_Eval(Sys,B,Pblues, traj);
valA = STL_Eval(Sys,A,Pblues, traj);

valevA = STL_Eval(Sys, evA, Pblues, traj);
valturn = STL_Eval(Sys, turn, Pblues, traj);

figure

bool_val = valE>0;
subplot(3,1,1);
plot(traj.time, traj.X);

%title('Signal s[t] and boolean detection of the note (in red)');    
% superpose the boolean interpretation of the predicate
hold on;
plot(traj.time, bool_val, 'r', 'LineWidth',2);    

%axis([0 .6 -1.1 1.1]);

subplot(3,1,2);
val = valevA*Ts;
bval = val>0;
plot(traj.time, val);
hold on;     
M = max(abs(val));
plot(traj.time, bval*M/2, 'r', 'LineWidth',2);    
%title(['Quantitative/Boolean satisfaction of predicate  ' disp(mu, -1)]);
axis tight;
%print(gcf,'-depsc','fine_tuned_pred.eps')

subplot(3,1,3);
val = valturn*Ts;
bval = val>0;
plot(traj.time, val);
hold on;     
M = max(abs(val));
plot(traj.time, bval*M/2, 'r', 'LineWidth',2);    
%title(['Quantitative/Boolean satisfaction of predicate  ' disp(mu, -1)]);
axis tight;

%print(gcf,'-depsc','fig_blues.eps')

