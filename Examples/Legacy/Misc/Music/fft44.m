function [nu y] = fft44(x, maxF)
% FFT44 helper functions to compute the power spectrum of x sampled at
% 44100 Hz
  
  y = fft(x);
  mu = linspace(0,22050, floor(numel(x)/2));
  
  nu = mu(mu<maxF);
  y = abs(y(1:numel(nu)));
  
