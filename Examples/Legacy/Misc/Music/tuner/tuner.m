function tuner(T, dt)
%  
%
% TODO improve notes readability
%      smooth noise ?
%      detect closest ?   

  define_guitar_Frs;
  
  r1 = audiorecorder(44100, 16, 1);
  r2 = audiorecorder(44100, 16, 1);

  r = {r1 r2};
  figure;
  Ts = 1/44100;
  %Ndt = dt/Ts;
  %Nt = t/Ts;

  ch = get(gca, 'Children');
  
  turn = 1;
  record(r{turn});
  pause(dt);
  
  pause(r{turn});
  xn =  getaudiodata(r{turn}, 'double');
 % nu = f0-10:.1:f0+10;
  
  nu = 0:1:600;
  Hx  = wfft44(xn,dt, .01, nu);
  barh(nu, Hx);
  hold on;
  set(gca,'Xlim', [0 .01], 'XlimMode', 'Manual');
  ch = get(gca, 'Children');
  barh(fE3, 1,.1,'r')
  barh(fA3, 1,.1,'r')
  barh(fD3, 1,.1,'r')
  barh(fG3, 1,.1,'r')
  barh(fB3, 1,.1,'r')
  barh(fE3, 1,.1,'r')
  barh(fE4, 1,.1,'r')
  barh(fA4, 1,.1,'r')
  barh(fD4, 1,.1,'r')
  barh(fG4, 1,.1,'r')
  barh(fB4, 1,.1,'r')
  barh(fE4, 1,.1,'r')

  
  set(gca, 'YTick',Frs, 'YTickLabel', Frs_ticks, 'YLim', [100 510]);

  
  resume(r{turn});
  pause(dt);
  t_rec = dt;
  ct =0;
  while (ct<T)
   
    stop(r{turn});
    r{mod(turn,2)+1} = audiorecorder(44100, 16, 1);  
    record(r{mod(turn,2)+1}); 
    tic;  
    xn =  getaudiodata(r{turn}, 'double');
   
    Hx  = wfft44(xn,t_rec, 0, nu);    
    set(ch, 'YData', Hx);
    turn=mod(turn,2)+1;
    
    pause(dt); % wait some time
    t_rec = toc;   
    ct = ct+t_rec;
  end
  
  stop(r{turn});
  stop(r{mod(turn,2)+1});
  
  
  
