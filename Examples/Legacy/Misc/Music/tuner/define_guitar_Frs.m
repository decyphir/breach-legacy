%
% define frequences for notes
%

fA = 440.00; % Master Tuned to A 440 This is A4
fGS = fA*2^(-1/12);
fG = fGS*2^(-1/12);
fFS = fG*2^(-1/12);
fF = fFS*2^(-1/12);
fE = fF*2^(-1/12);
fDS = fE*2^(-1/12);
fD = fDS*2^(-1/12);
fCS = fD*2^(-1/12);
fC = fCS*2^(-1/12);
fAS = fA*2^(1/12);
fB = fAS*2^(1/12);


fA1 = 55.00; % Master Tuned to A1 55
fGS1 = fA1*2^(-1/12);
fG1  = fGS1*2^(-1/12);
fFS1 = fG1*2^(-1/12);
fF1  = fFS1*2^(-1/12);
fE1  = fF1*2^(-1/12);
fDS1 = fE1*2^(-1/12);
fD1  = fDS1*2^(-1/12);
fCS1 = fD1*2^(-1/12);
fC1 = fCS1*2^(-1/12);
fAS1 = fA1*2^(1/12);
fB1 = fAS1*2^(1/12);


fA2 = 110.00; % Master Tuned to A2 110
fGS2 = fA2*2^(-1/12);
fG2 = fGS2*2^(-1/12);
fFS2 = fG2*2^(-1/12);
fF2 = fFS2*2^(-1/12);
fE2 = fF2*2^(-1/12);
fDS2 = fE2*2^(-1/12);
fD2 = fDS2*2^(-1/12);
fCS2 = fD2*2^(-1/12);
fC2 = fCS2*2^(-1/12);
fAS2 = fA2*2^(1/12);
fB2 = fAS2*2^(1/12);

fA3 = 220.00; % Master Tuned to A3 220
fGS3 = fA3*2^(-1/12);
fG3 = fGS3*2^(-1/12);
fFS3 = fG3*2^(-1/12);
fF3 = fFS3*2^(-1/12);
fE3 = fF3*2^(-1/12);
fDS3 = fE3*2^(-1/12);
fD3 = fDS3*2^(-1/12);
fCS3 = fD3*2^(-1/12);
fC3 = fCS3*2^(-1/12);
fAS3 = fA3*2^(1/12);
fB3 = fAS3*2^(1/12);

fA4 = 440.00; % Master Tuned to A4 440
fGS4 = fA4*2^(-1/12);
fG4 = fGS4*2^(-1/12);
fFS4 = fG4*2^(-1/12);
fF4 = fFS4*2^(-1/12);
fE4 = fF4*2^(-1/12);
fDS4 = fE4*2^(-1/12);
fD4 = fDS4*2^(-1/12);
fCS4 = fD4*2^(-1/12);
fC4 = fCS4*2^(-1/12);
fAS4 = fA4*2^(1/12);
fB4 = fAS4*2^(1/12);


%Frs1 = [fC1 fCS1 fD1 fDS1 fE1 fF1 fFS1 fG1 fGS1 fA1 fAS1 fB1 ];

Frs1 = [fD1 fE1 fG1 fA1 fB1 ];
Frs_ticks = {};
Frs = [];

for i = 3:5
  Frs = [Frs 2^(i-1)*Frs1];
  Frs_ticks = { Frs_ticks{:} ['D' num2str(i)] ['E' num2str(i)] ['G' num2str(i)] ['A' num2str(i)]  ['B' num2str(i)]};
end
