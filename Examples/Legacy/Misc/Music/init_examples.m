% 
% Initializes all necessary variable for exploring the examples
%  

clear all;
% system 
InitBreach;


define_breach_system;

% properties
define_violin_org_prop;
define_blues_formula;

% load signals 

%% single note
y = wavread('single_F4.wav');
single_F4 = y(1:44100)';

traj.time = 0:Ts:Ts*(numel(single_F4)-1);
traj.X = single_F4;
traj.param = Sys.p;

PF4 = CreateParamSet(Sys,1);
PF4.pts = traj.param;
PF4.traj = traj; 

%% simple melody, violin and church organ 

%%% load files
violin_melody = wavread('violin.wav');
organ_melody = wavread('churchorgan.wav');

violin_melody = violin_melody(1:ceil(7*44100));
organ_melody = organ_melody(1:ceil(7*44100));

time = 0:Ts:Ts*(numel(violin_melody)-1);
traj_violin.time = time;          
traj_org.time = time;

traj_violin.X = violin_melody';
traj_org.X = organ_melody';
traj_violin.param = Sys.p;
traj_org.param = Sys.p;

%%% tune parameters
ind_thA = FindParam(Sys, 'thA'); 
ind_thB = FindParam(Sys, 'thB'); 
ind_thC = FindParam(Sys, 'thC'); 
ind_thD = FindParam(Sys, 'thD'); 
ind_thE = FindParam(Sys, 'thF'); 
ind_thF = FindParam(Sys, 'thE'); 
ind_thG = FindParam(Sys, 'thG'); 

ind_p2 = FindParam(Sys, 'p2');
ind_p3 = FindParam(Sys, 'p3');

ind_th = [ ind_thA   
           ind_thB 
           ind_thC 
           ind_thD 
           ind_thE 
           ind_thF  
           ind_thG
         ];

traj_violin.param(ind_th) = 0.003;     
traj_org.param(ind_th) = 0.003;
traj_org.param(ind_thD) = 0.008;


traj_violin.param(ind_p2) = 0.01;     
traj_org.param(ind_p2) = 0.01;
traj_violin.param(ind_p3) = 0.2;     
traj_org.param(ind_p3) = 0.2;

Pviol = CreateParamSet(Sys,1);
Pviol.pts = traj_violin.param;
Pviol.traj = traj_violin; 

Porg = CreateParamSet(Sys,1);
Porg.pts = traj_org.param;
Porg.traj = traj_org; 

%% blues

%%% load files
y = wavread('fast_blues.wav');
blues = y';
blues = blues .* 50;

%%% tune paramters
bpm = 120.9586;
traj.time = 0:Ts:Ts*(numel(blues)-1);
traj.X = blues;
traj.param = Sys.p;

ind_bpm = FindParam(Sys,'bpm');  % beats for minutes - time
ind_thA = FindParam(Sys, 'thA'); % threshold for A
ind_thB = FindParam(Sys, 'thB'); % threshold for B
ind_thE = FindParam(Sys, 'thE'); % threshold for E

ind_fA = FindParam(Sys, 'fA'); % threshold for A
ind_fB = FindParam(Sys, 'fB'); % threshold for B
ind_fE = FindParam(Sys, 'fE'); % threshold for E

ind_p0 = FindParam(Sys, 'p0');
ind_p1 = FindParam(Sys, 'p1'); 
ind_p2 = FindParam(Sys, 'p2');
ind_p3 = FindParam(Sys, 'p3');

traj.param(ind_thA) = .002;     
traj.param(ind_thB) = .002; 
traj.param(ind_thE) = .002; 

traj.param(ind_fA) = fA3;     
traj.param(ind_fB) = fB3; 
traj.param(ind_fE) = fE3; 

traj.param(ind_p0) = (4*60)/bpm;                                  

traj.param(ind_bpm) = 121; %120.9586;

Pblues = CreateParamSet(Sys,1);
Pblues.pts = traj.param;
Pblues.traj = traj; 

% save files for Breach GUI

dr = pwd; 
indst = strfind(dr, filesep);
dirname = dr(indst(end)+1:end);    

save([ dirname '_properties'], 'phi*' )
save([ dirname '_properties'], '-append', 'A' )
save([ dirname '_properties'], '-append', 'B' )
save([ dirname '_properties'], '-append', 'C' )
save([ dirname '_properties'], '-append', 'D' )
save([ dirname '_properties'], '-append', 'E' )
save([ dirname '_properties'], '-append', 'F' )
save([ dirname '_properties'], '-append', 'G' )
save([ dirname '_properties'], '-append', 'mute' )

save([ dirname '_param_sets'], 'P*');
