% DEFINE_BREACH_SYSTEM  defines a (dynamicless) system compatible with Breach to use robust monitoring
%
% The system has parameters for recognizing notes on a chromatic scale and
% one variable,'s', for one signal. Eventually we can add other variables to
% emulate a music with multiple channels
%

% Init notes model
define_notes;

f0 = 44100;
dt0 = 1/f0;

vars = {'s'};

params = {'p0', 'p1','p2', 'p3' ...                                                             % generic parameters  
          'Dt', 'skt', 'Df','bpm', ...                                                             % params for PSFT
          'fC','fCS','fD','fDS','fE','fF','fFS','fG','fGS','fA','fAS','fB' ...             % frequencies
          'thC','thCS','thD','thDS','thE','thF','thFS','thG','thGS','thA','thAS','thB' ... % thresholds for proper discrimination
         };  

th = .002;     

Dt = .08; % window size in seconds 
skt = .002; % gap between two ffts
Df = 5;

% Init param values
p0 = [ 0, ...
       0 0 0 0, ...
       Dt skt Df 120 ... 
       fC    fCS   fD   fDS   fE   fF   fFS   fG   fGS   fA   fAS   fB ...
       th    th    th   th    th   th   th    th   th    th   th    th   ...
     ];  

% Init system (named Records) 

Sys = CreateExternSystem('Records',vars,params, p0);

