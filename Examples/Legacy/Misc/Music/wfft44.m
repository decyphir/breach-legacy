function [Hx TT NNU]  = wfft44(x,Dt, T0, nu0,MF)

% WFFT44  computes the Windowed Fourier Transform of a signal at time(s) T0, frequency(ies) nu0   
%  
%  [Hx TT NNU]  = wfft44(x,Dt, T0, nu0)
%  
%  Assumes a signal sampled at 44100 Hz. TT and NNU are used for surface plotting of the resulting spectrogram.
%
%  Ex: To plot a spectrogram   
%
%  x = chrom_scale; 
%  Dt = .2 ; T0 = 0:.02:numel(x)*Ts; nu0 = 0:.2:1000;
%  [Hx TT NNU]  = wfft44(x,Dt, T0, nu0);
%  figure;
%  surf(TT,NNU, Hx', 'EdgeColor', 'None');
% 
  
  Ts = 1/44100;

  if (~exist('MF'))
    MF = floor((Dt)/Ts);
  end
  
  xtmp = zeros(1, max(ceil(max(T0+Dt)/Ts), numel(x)));
  xtmp(1:numel(x))=x;
  x= xtmp;
  
  Nt = Dt/Ts+4;
  win_t = MakeWindow('Hanning', (Nt-1)/2);  
  
  i = 1;
  Hx = zeros(numel(T0), numel(nu0));   
  for t0 = T0
    
    it0 = max(floor((t0-0.5*Dt)/Ts),1);
    it1 = max(floor((t0+0.5*Dt)/Ts),1);
       
    if (mod(it1-it0+1,2)==0)
      it1=it1+1;
    end
    it1 = min(it1,size(x,2));
    
    Nt = it1-it0+1;
    s = zeros(1, max(MF, Nt));
       
    mx = x(it0:it1)-mean(x(it0:it1));
    s(1:Nt) = mx.*win_t(end-Nt+1:end);
   
    [om hs] = fft44(s, 1.5*max(nu0));
    hx = interp1(om, hs, nu0);
  
    Hx(i,:) = hx;
    i = i+1;
  end
  
  Hx = Hx*Ts;
  [TT NNU] = meshgrid(T0, nu0);
  
