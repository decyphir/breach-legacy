%
%  Defines a simple notes model and several test melodies
% 


Fs = 44100; %Set Sampling Frequency
%Fr = Fs1;    %Set Reconstruction Frequency
%Fs = Fs1;    %The multiple of the sampling frequency to be used
Ts = 1/Fs;    %Sampling Period

N = 0.2*Fs;    % Number of samples to achieve desired duration
L = 1;          % default value for L (L=1 => no duration change)
n = @(L) 1:L*N; %the array, n, takes an integer multiplier, L, that can reduce or increase the duration of a note, i.e. 1/2 note, 1/4 note, etc
m = 0; %default input for oct, the octave shift function (using downsampling)


%  Generate General Sinusoid
%  m is the desired octave, which is transformed into the appropriate multiplier by the oct function
%  L is the desired length of the note (in quarter notes)
%  fN is the frequncy of the note 
note = @(m, L, fN) sin(2*pi*oct(m)*fN*Ts*n(L)); %standard note at fund. freq.

fA = 440.00; % Master Tuned to A 440 This is A4
fGS = fA*2^(-1/12);
fG = fGS*2^(-1/12);
fFS = fG*2^(-1/12);
fF = fFS*2^(-1/12);
fE = fF*2^(-1/12);
fDS = fE*2^(-1/12);
fD = fDS*2^(-1/12);
fCS = fD*2^(-1/12);
fC = fCS*2^(-1/12);
fAS = fA*2^(1/12);
fB = fAS*2^(1/12);


fA1 = 55.00; % Master Tuned to A1 55
fGS1 = fA*2^(-1/12);
fG1  = fGS*2^(-1/12);
fFS1 = fG*2^(-1/12);
fF1  = fFS*2^(-1/12);
fE1  = fF*2^(-1/12);
fDS1 = fE*2^(-1/12);
fD1  = fDS*2^(-1/12);
fCS1 = fD*2^(-1/12);
fC1 = fCS*2^(-1/12);
fAS1 = fA*2^(1/12);
fB1 = fAS*2^(1/12);


fA2 = 110.00; % Master Tuned to A2 110
fGS2 = fA2*2^(-1/12);
fG2 = fGS2*2^(-1/12);
fFS2 = fG2*2^(-1/12);
fF2 = fFS2*2^(-1/12);
fE2 = fF2*2^(-1/12);
fDS2 = fE2*2^(-1/12);
fD2 = fDS2*2^(-1/12);
fCS2 = fD2*2^(-1/12);
fC2 = fCS2*2^(-1/12);
fAS2 = fA2*2^(1/12);
fB2 = fAS2*2^(1/12);


fA3 = 220.00; % Master Tuned to A3 220
fGS3 = fA3*2^(-1/12);
fG3 = fGS3*2^(-1/12);
fFS3 = fG3*2^(-1/12);
fF3 = fFS3*2^(-1/12);
fE3 = fF3*2^(-1/12);
fDS3 = fE3*2^(-1/12);
fD3 = fDS3*2^(-1/12);
fCS3 = fD3*2^(-1/12);
fC3 = fCS3*2^(-1/12);
fAS3 = fA3*2^(1/12);
fB3 = fAS3*2^(1/12);


freqs = [fC fCS fD fDS fE fF fFS fG fGS fA fAS fB fC*2];

namp = 1; % set the amplitude for the natural freq
hamp = 0.8; % set the amplitude for the overtones

%each note passes m and L to the note function above
%two overtones are added to each note

note_w_ot =  @(m, L, f) namp*note(m, L, f) + hamp*note(m, L, 0.5*f) + hamp*note(m, L, 2*f); 

C = @(m, L) note_w_ot(m, L, fC);
CS = @(m, L) note_w_ot(m, L, fCS);
D = @(m, L) note_w_ot(m, L, fD);
DS = @(m, L) note_w_ot(m, L, fDS);
E = @(m, L) note_w_ot(m, L, fE);
F = @(m, L) note_w_ot(m, L, fF);
FS = @(m, L) note_w_ot(m, L, fFS);
G = @(m, L) note_w_ot(m, L, fG);
GS = @(m, L) note_w_ot(m, L, fGS);
A = @(m, L) note_w_ot(m, L, fA);
AS = @(m, L) note_w_ot(m, L, fAS);
B = @(m, L) note_w_ot(m, L, fB);

%Define Rests
er = zeros(1, floor(.125*N)); % eigth rest
qr = zeros(1, floor(.25*N)); % quarter rest
hr = zeros(1, floor(.5*N)); % half rest
tr = zeros(1, floor(.75*N)); % three-quarter rest
wr = zeros(1, N); % whole rest

% test note 

test_note = [wr note(0, 1, fA) wr];

% Scales 
major_scale = [note_w_ot(0,2,172) wr C(0,2) wr D(0,2) wr E(0,2) wr F(0,2) wr G(0,2) wr A(0,2) wr B(0,2) wr C(1,2) wr D(1,2) wr E(1,2) wr F(1,2) wr G(1,2) wr A(1,2) wr B(1,2) wr C(2,2)]; 

chrom_scale = [C(0,2) er CS(0,2) er D(0,2) er DS(0,2) er E(0,2) er ...
                   F(0,2) er FS(0,2) er G(0,2) er GS(0,2) er A(0,2) er ...
                   AS(0,2) er B(0,2) er C(1,2)];

chrom_scale_bis = [C(0,1) er C(0,1) er CS(0,1) er CS(0,1) er D(0,1) er D(0,1) er DS(0,1) er DS(0,1) er E(0,1) er E(0,1) er ...
                   F(0,1) er F(0,1) er FS(0,1) er FS(0,1) er G(0,1) er G(0,1) er GS(0,1) er GS(0,1) er A(0,1) er A(0,1) er ...
                   AS(0,1) er AS(0,1) er B(0,1) er B(0,1) er C(1,1) er C(1,1)];

%Jingle Bells
%note(octave, duration in 1/4 note) example: A=C(1,1) = 1/4 note middle C               
               
jbseq1 = [A(0,1) qr A(0,1) qr A(0,2) qr]; 
jbseq2 = [A(0,1) qr C(1,1) qr F(0,1) qr G(0,1) qr A(0,4) qr]; 
jbseq3 = [AS(0,1) qr AS(0,1) qr AS(0,1) qr AS(0,0.5) qr AS(0,1) qr A(0,1) qr A(0,1) qr A(0,0.5) er A(0,0.5) qr A(0,1) qr G(0,1) qr G(0,1) qr A(0,1) qr G(0,2) qr C(1,2) qr]; 
jbseq4 = [AS(0,1) qr AS(0,1) qr AS(0,1) qr AS(0,1) qr AS(0,1) qr A(0,1) qr A(0,1) qr A(0,0.5) er A(0,0.5) qr C(1,1) qr C(1,1) qr AS(0,1) qr G(0,1) qr F(0,4)]; 
jbseq5 = [C(0,1) qr A(0,1) qr G(0,1) qr F(0,1) qr C(0,3) qr C(0,0.5) er C(0,0.5) qr C(0,1) qr A(0,1) qr G(0,1) qr F(0,1) qr D(0,4) qr];
jbseq6 = [D(0,1) qr AS(0,1) qr A(0,1) qr G(0,1) qr E(0,4) qr C(1,1) qr C(1,1) qr AS(0,1) qr G(0,1) qr A(0,4) qr];
jbseq7 = [C(0,1) qr A(0,1) qr G(0,1) qr F(0,1) qr C(0,4) qr C(0,1) qr A(0,1) qr G(0,1) qr F(0,1) qr D(0,4) qr];
jbseq8 = [D(0,1) qr AS(0,1) qr A(0,1) qr G(0,1) qr C(1,1) qr C(1,1) qr C(1,1) qr C(1,0.5) er C(1,0.5) qr D(1,1) qr C(1,1) qr AS(0,1) qr G(0,1) qr F(0,1) wr C(1,2) qr];
jbseq9 = [AS(0,1) qr AS(0,1) qr AS(0,1) qr AS(0,1) qr AS(0,1) qr A(0,1) qr A(0,1) qr A(0,0.5) er A(0,0.5) qr C(1,1) qr C(1,1) qr AS(0,1) qr G(0,1) qr F(0,4) qr ];

%Song constructed from several, sometimes repeating, jbsequences
jbells = [jbseq1 jbseq1 jbseq2 jbseq3 jbseq1 jbseq1 jbseq2 jbseq4 jbseq5 jbseq6 jbseq7 jbseq8 jbseq1 jbseq1 jbseq2 jbseq3 jbseq1 jbseq1 jbseq2 jbseq3 jbseq9];

octm2 = [C(-2, 0.5) qr CS(-2, 0.5) qr D(-2, 0.5) qr DS(-2, 0.5) qr E(-2, 0.5) qr F(-2, 0.5) qr FS(-2, 0.5) qr G(-2, 0.5) qr GS(-2, 0.5) qr A(-2, 0.5) qr AS(-2, 0.5) qr B(-2, 0.5) qr];
octm1 = [C(-1, 0.5) qr CS(-1, 0.5) qr D(-1, 0.5) qr DS(-1, 0.5) qr E(-1, 0.5) qr F(-1, 0.5) qr FS(-1, 0.5) qr G(-1, 0.5) qr GS(-1, 0.5) qr A(-1, 0.5) qr AS(-1, 0.5) qr B(-1, 0.5) qr];
oct0 = [C(0, 0.5) qr CS(0, 0.5) qr D(0, 0.5) qr DS(0, 0.5) qr E(0, 0.5) qr F(0, 0.5) qr FS(0, 0.5) qr G(0, 0.5) qr GS(0, 0.5) qr A(0, 0.5) qr AS(0, 0.5) qr B(0, 0.5) qr];
oct1 = [C(1, 0.5) qr CS(1, 0.5) qr D(1, 0.5) qr DS(1, 0.5) qr E(1, 0.5) qr F(1, 0.5) qr FS(1, 0.5) qr G(1, 0.5) qr GS(1, 0.5) qr A(1, 0.5) qr AS(1, 0.5) qr B(1, 0.5) qr];
oct2 = [C(2, 0.5) qr CS(2, 0.5) qr D(2, 0.5) qr DS(2, 0.5) qr E(2, 0.5) qr F(2, 0.5) qr FS(2, 0.5) qr G(2, 0.5) qr GS(2, 0.5) qr A(2, 0.5) qr AS(2, 0.5) qr B(2, 0.5) qr];

superoct = [octm2 octm1 oct0 oct1 oct2];

polytest1 = [C(0,4)+ E(1,4) C(0, 2) C(0,4)+ E(1,4) C(0, 2) C(0,4)+ F(1,4) C(0, 2) C(0,4)+ D(1,4)];
polytest = [polytest1 polytest1 polytest1 C(0,8) + C(-1,8)];

