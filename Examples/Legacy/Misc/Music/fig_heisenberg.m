f0 = 50; 
t0 = 0:Ts:2;
Df = 10;

sil = zeros(size(0:Ts:1.5)); % silence
x = [ sil sin(2*pi*f0*t0) sil ];

t = 0:Ts:(numel(x)-1)*Ts;

T0   = 0:.02:numel(x)*Ts; 
nu0  = f0-Df:.01:f0+Df;

Tf = T0(end);
nui =  nu0(1);
nuf =  nu0(end);

%subplot(3,4, [2,3] ); 
subplot(3,1, 1 ); 
plot(t,x);
set(gca,'XLim', [t(1)+1 t(end)-1]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% first spectrogram 

Dt1 = .5;
[Hx1 TT NNU]  = wfft44(x,Dt1, T0, nu0, 2^15);

% define Heisenberg grid 
%sig_t1 = sqrt(Dt1^3/12);
sig_t1 = compute_sig('Hanning', Dt1,Ts);
sig_om1 = 1/(2*sig_t1);

HeisenX1 = sig_t1/2: sig_t1:Tf;
HeisenY1 = f0+sig_om1/2 - (ceil(Df/sig_om1) * sig_om1) : sig_om1 : f0+sig_om1/2 + (ceil(Df/sig_om1) * sig_om1);
HeisenY1 = unique(sort( [HeisenY1 nu0(1) nu0(end) ]));

%subplot(3,4,[5 6 9 10]);
subplot(3,1,2);
contour(TT,NNU, (Hx1.^2)',30);
title('Dt=.5');
set(gca, 'XTick', HeisenX1, 'YTick',HeisenY1, 'XLim', [1+-Dt1/2 -Dt1/2+t(end)-1]','XTickLabel', []);
grid on;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% second spectrogram

Dt2 = 2;
[Hx2 TT NNU]  = wfft44(x,Dt2, T0, nu0, 2^15);

sig_t2 = compute_sig('Hanning', Dt2,Ts);
sig_om2 = 1/(2*sig_t2);

HeisenX2 = unique(sort([sig_t2/2-sig_t2: sig_t2:Tf]));
HeisenY2 =   f0+sig_om2/2 - ((ceil(Df/sig_om2)) * sig_om2) : sig_om2 : f0+sig_om2/2 + (ceil(Df/sig_om2) * sig_om2);

%subplot(3,4,[7 8 11 12]);
subplot(3,1,3);
%contour(TT,NNU, Hx2', 0.01:.01:1);
contour(TT,NNU, (Hx2.^2)',20);
title('Dt=2');

set(gca, 'XTick', HeisenX2, 'YTick',HeisenY2, 'XLim', [-Dt2/2+1 -Dt2/2+t(end)-1],'XTickLabel', [], 'YTickLabel',[]);
grid on;

%pos = get(gcf, 'PaperPosition');
%pos(4) = 2*pos(4);
%set(gcf,'PaperPosition', pos);
print(gcf, '-depsc', 'fig_heisenberg.eps');


