InitBreach;

% Initialize a system with 10 parameters
n = 3;
Sys = Init_SignalGenerator(n);

% creates nominal parameter set and trajectory
P = CreateParamSet(Sys);
Pf = ComputeTraj(Sys,P, 0:.01:10);

% plots nominal trajectory
figure;
SplotVar(Pf);
return;
% loads and eval property phi on nominal trajectory 

formulas = STL_ReadFile('stl_formula.stl');
val = STL_Eval(Sys, phi, P, Pf.traj, 0)

% bonus: optimize parameters so as to satisfy the formula

Psearch = CreateParamSet(Sys, [Sys.DimX+1:Sys.DimP], repmat([-2 2],n,1));
 
opt.MaxIter = 100;

[val_opt, Popt] = SOptimProp(Sys, Psearch, phi0, opt); 

figure;
SplotVar(Popt);
