function [tspan, X] = sim_SignalGenerator(Sys, tspan, p)
%
% This function implements (a wrapper to) an external simulator for Breach,
% i.e., if attached to System structure, it will be called by the
% ComputeTraj function. In this example, parameters are control points and
% we generate three signals based on three different interpolation
% techniques, piecewise constant, piecewise linear and cubic splines
% 

%% 1. Convert Breach input into the external simulator input 

% We assume that our simulator takes input as a structure with field x0 for
% initial conditions, and parameters for other parameters 

% In Breach, p(1:Sys.DimX) is dedicated to initial conditions  

sim_param.x0 = p(1:Sys.DimX);

%
% The rest are parameters minus initial conditions
%  

sim_param.parameters = p(Sys.DimX+1:end); 

%% 2. Call to external simulator

sim_output = call_external_simulator(tspan, sim_param);

%% 3. Convert external simulator output to Breach output

X = [sim_output.xc;...
     sim_output.xl;...
     sim_output.xs];

end

function sim_output = call_external_simulator(tspan, sim_param)

p = sim_param.parameters;

nb_cp = numel(p);

tp = linspace(tspan(1), tspan(end), nb_cp);

xc = interp1(tp', p, tspan', 'nearest')';
xl = interp1(tp', p, tspan', 'linear')';
xs = interp1(tp', p, tspan', 'spline')';

% adjust initial condition
sim_output.xc = xc-xc(1)+sim_param.x0(1);
sim_output.xl = xl-xl(1)+sim_param.x0(2);
sim_output.xs = xs-xs(1)+sim_param.x0(3);

end
