function Sys = Init_SignalGenerator(n)
%INIT_SIGNALGENERATOR creates a simple Breach system generating signals
% from n parameter values
% 
% Synopsis: Sys = Init_SignalGenerator(n)
%

name ='SignalGenerator';
signals = {'xc', 'xl', 'xs'};

params = cell(1,n);
for ii = 1:n
    params{ii} = sprintf('u%d',ii);
end

p0 = 2*rand(n,1)-1;

Sys = CreateExternSystem(name, signals, params, p0, @sim_SignalGenerator);
Sys.tspan = 0:.01:10;

end
