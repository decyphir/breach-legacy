%%
% This script shows how to set up an interface between Breach and a
% Simulink model with inputs, using simple uniformly distributed
% control points to generate input signals
%

clear;
%% 
% First we need to initialize Breach, of not done at Matlab startup. 
InitBreach;

%% 
% Define the Breach structure from the Simulink model.
% Breach provides a generic wrapper function to create a system structure
% which it can work with. 

% name of the Simulink model 
mdl = 'Autotrans_shift'; % note that breach will use its own copy of this model

% signals to monitor using Breach
signals = 'logged'; % breach will look for logged signals in the model, 
                    % in addition to inputs and outputs
              
params = {}; % we don't declare parameters for this system
p0 = [];     % hence no nominal values. 

%% 
% Create system structure with piecewise constant input with fixed time step
% Note that inputs are piecewise constant because 'interpolate data' is
% unchecked in the input blocks properties of the original Simulink model

inputs.type = 'UniStep';  % fixed time step inputs (duration = simulation time/4)
inputs.cp   =  [4 3]; % number of control points for each input (4 for throttle,
                      % 3 for brake)
inputs.method = {'linear', 'previous'};% this accepts interpolation methods 
                                      % accepted by interp1
                            
Sys = CreateSimulinkSystem(mdl, signals, params, p0, inputs);
Sys.tspan = 0:.01:50; % default time for the simulation

% the following will list inputs parameters generated by breach
[ ~, idx_inputs] = FindParamsInput(Sys);

% Set nominal input values (0 by default otherwise)
Sys.p( idx_inputs ) = ...
      [ 50 ;...       %   throttle_u0     
        100; ...      %   throttle_u1            
        0 ; ...      %   throttle_u2 
        0 ; ...      %   throttle_u3 
        0  ;...       %   brake_u0  
        325  ; ...      %   brake_u1    
        325   ...       %   brake_u2    
      ];

%% Create nominal parameter set and compute trajectory  
P0 = CreateParamSet(Sys);

% runs simulation - this will create a field P0.traj 
P0 = ComputeTraj(Sys, P0, Sys.tspan);

% plots signals
figure;
SplotVar(P0);
