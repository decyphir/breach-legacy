#include "dynamics.h"

int f(realtype t, N_Vector x, N_Vector xdot, void *f_data) {

  Fdata* data = (Fdata*) f_data;

  realtype VD1 = Ith(x,0);
  realtype VD2 = Ith(x,1);
  realtype IL1 = Ith(x,2);

  realtype V_tp = data->p[3];
  realtype K_p = data->p[4];
  realtype WdL = data->p[5];
  realtype Omega_P = data->p[6];
  realtype V_DD = data->p[7];
  realtype I_b = data->p[8];
  realtype C = data->p[9];
  realtype V_ctrl = data->p[10];
  realtype L = data->p[11];
  realtype R = data->p[12];

  /* Compute I_DS1 */
  realtype I_DS1;
  realtype V_GS=VD2-V_DD;
  realtype V_DS=VD1-V_DD;
  
  if (V_GS > V_tp)
    I_DS1=0.;
  else 
    if (V_DS-V_GS > -V_tp)
      I_DS1=-K_p*WdL*((V_GS-V_tp)*(V_DS)-1/2*V_DS*V_DS)*(1-Omega_P*V_DS);
    else
    I_DS1=-K_p/2*WdL*(V_GS-V_tp)*(V_GS-V_tp)*(1-Omega_P*V_DS);
  
    /* Compute I_DS2 */

  realtype I_DS2;
  V_GS=VD1-V_DD;
  V_DS=VD2-V_DD;
  
  if (V_GS > V_tp)
    I_DS2=0;
  else 
    if (V_DS-V_GS > -V_tp)
      I_DS2=-K_p*WdL*((V_GS-V_tp)*(V_DS)-1/2*V_DS*V_DS)*(1-Omega_P*V_DS);
    else
      I_DS2=-K_p/2*WdL*(V_GS-V_tp)*(V_GS-V_tp)*(1-Omega_P*V_DS);
 
      Ith(xdot,0) = -1/C*(I_DS1 + IL1);
      Ith(xdot,1) = -1/C*(I_DS2 +I_b-IL1);
      Ith(xdot,2) =  1/(2*L)*(VD1-VD2-R*(2*IL1-I_b));

      return(0);

 }


void InitFdata(void * &f_data, const mxArray * mxData){

  Fdata* data = (Fdata*) f_data;

  data->dimx = 3;
  data->dimu = 0;
  data->dimp = 13;
  data->p = mxGetPr(mxGetField(mxData,0,"p"));

}

int UpdateFdata(realtype t, N_Vector x, void * &f_data, int Ns, N_Vector* xS)
{ return 0;}

int g(realtype t, N_Vector x, realtype *gout, void *f_data){ 
  return 0;}

int Jac(long int N, DenseMat J, realtype t, 
              N_Vector x, N_Vector fx, void *jac_data,
        N_Vector tmp1, N_Vector tmp2, N_Vector tmp3)
 { 
  return 0; 
}
