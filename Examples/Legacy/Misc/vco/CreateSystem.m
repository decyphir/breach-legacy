InitBreach;
Sys.DimX = 3;
Sys.DimU =0; 
Sys.DimP = 13; 
Sys.ParamList = {'VD1','VD2','IL1', ....
         'V_tp','K_p','WdL','Omega_P','V_DD','I_b','C','V_ctrl','L',....
         'R'};
Sys.x0=[ 0, 0, 0]';
Sys.p=[ 0, 0, 0,-.69,86e-6,960,-0.07,1.8,18e-3,3.43e-2,0.,28.57,....
          3.7]';
Sys.Dir = '/home/alex/workspace/PROBLEMS/vco';
options=[];
options = CVodeSetOptions(options,'RelTol',1.e-6,'AbsTol',1e-8,'MaxNumSteps',100000);
Sys.CVodesOptions = options;

pscales = ones(Sys.DimP,1);
      FSAoptions = [];
      FSAoptions = CVodeSetFSAOptions('SensErrControl','on',...
                               'ParamField', 'p',...
                               'ParamScales',pscales );
Sys.CVodesSensiOptions.method = 'Simultaneous';
Sys.CVodesSensiOptions.FSAoptions = FSAoptions;
