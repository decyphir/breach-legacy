#include "dynamics.h"

int f(realtype t, N_Vector x, N_Vector xdot, void *f_data) {
    
    Fdata* data = (Fdata*) f_data;
    
    realtype x0 = Ith(x,0);
    realtype x1 = Ith(x,1);
    realtype x2 = Ith(x,2);
    
    // System parameters
    realtype a = data->p[3];
    
     // Equations
    switch (data->q) {
   // switch (x0<0) {
    case 0 :
        Ith(xdot,0) = -x0+10*x1- x2*x2*x2 -1;
        Ith(xdot,1) = -10*x0 - x1 - 10;
        Ith(xdot,2) = sin(a*x0) - x2;
        break;
    case 1 :
        Ith(xdot,0) = -x0+10*x1- x2*x2*x2 + 1;
        Ith(xdot,1) = -10*x0 - x1 + 10;
        Ith(xdot,2) = sin(a*x0) - x2;
        break;
    } // end switch
    
    return(0);
}


void InitFdata(void * &f_data, const mxArray * mxData){
    
    Fdata* data = (Fdata*) f_data;
    
    data->dimx = 3;     // number of variables
    data->dimu = 0;
    data->dimg = DIMG;  // number of modes
    data->dimp = 4;     //number of system parameters
    data->p = mxGetPr(mxGetField(mxData,0,"p"));
    
    // initialize q
    double x0 = data->p[0]; // initial x0
    
    if (x0<0) {
        data->q = 0;
    } else {
        data->q = 1;
    }
}

int UpdateFdata(realtype t, N_Vector x, void * &f_data, int Ns, N_Vector* xS) {
    
    Fdata* data = (Fdata*) f_data;
    int rootsfound=-1;
    CVodeGetRootInfo(cvm_Cdata->cvode_mem, &rootsfound);
    
    // compute new q depending on the sign of g
    int prev_q = data->q;
    realtype x0 = Ith(x,0); // current x0
    
    switch (data->q) {
    
    // Location 0
    case 0 :
        switch (rootsfound) {
        // hit guard 0
        case 0:
            data->q = 1;
            break;
        }
        break;
    // Location 1
    case 1 :
        switch (rootsfound) {
        // hit guard 0
        case 0:
            data->q = 0;
            break;
        }
        break;
    } // end switch
    
    //  if (x0<0)
    //    data->q = 0;
    //  else
    //    data->q = 1;
    
    return (prev_q != data->q);
}

int g(realtype t, N_Vector x, realtype *gout, void *f_data) {
    realtype x0 = Ith(x,0); // current x0
    
    gout[0] = x0;
    
    return 0;
}

int Jac(long int N, DenseMat J, realtype t,
        N_Vector x, N_Vector fx, void *jac_data,
        N_Vector tmp1, N_Vector tmp2, N_Vector tmp3) {
    return 0;
}
