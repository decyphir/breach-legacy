InitBreach;

% System Definition

Sys.DimX = 3;
Sys.DimU =0; 
Sys.DimP = 4; 
Sys.ParamList = {'x0','x1','x2', ....
         'a'};
Sys.x0 = [-1. 1. 0. ]';

Sys.p = [0. 0. 0.  ....
         10.]';
Sys.Dir = [ BreachGlobOpt.breach_dir filesep 'Examples' filesep  'hybrid_nonlin'];
options=[];
options = CVodeSetOptions(options,'RelTol',1.e-6,'AbsTol',1e-8,'MaxNumSteps',100000);
Sys.CVodesOptions = options;

% options for sensitivity computation

pscales = ones(Sys.DimP,1);
      FSAoptions = [];
      FSAoptions = CVodeSetFSAOptions('SensErrControl','on',...
                               'ParamField', 'p',...
                               'ParamScales',pscales );
Sys.CVodesSensiOptions.method = 'Simultaneous';
Sys.CVodesSensiOptions.FSAoptions = FSAoptions;
