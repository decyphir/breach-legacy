#ifndef DYNAMICS_H
#define DYNAMICS_H

#include "breach.h"

#define DIMG 1

class Fdata: public  FdataCommon {
 public:
  double *p;
  double lambda[DIMG];
  int q;
  
};

int  f(realtype t, N_Vector y, N_Vector ydot, void *f_data);
void InitFdata(void * &f_data, const mxArray * mxData);
int  UpdateFdata(realtype t, N_Vector y, void * &f_data, int Ns, N_Vector* yS);

int g(realtype t, N_Vector y, realtype *gout, void *g_data);

#endif
