

% basic pitch and roll measurement

PITCH = 'atan2(x0[t], -x1[t])';
ROLL =  'atan2(x1[t], -x2[t])';

% define atemporal positions left_rotate and screen_up 

LROTATE =  [PITCH '> theta_min | theta_min = pi/2'];

left_rotate = STL_Formula('left_rotate', LROTATE );

SCREEN_UP = [ 'abs(' ROLL ') < k | k = .2' ]; 

screen_up = STL_Formula('screen_up', SCREEN_UP);


% define the temporal sequence  LROTATE followed before 3s 
% by SCREEN_UP during at least 2s

gesture = STL_Formula('gesture', ...
['(' LROTATE ') and (eventually_[0,3] (always_[0,2] (' SCREEN_UP ')))']);

% save the defined formulas
save('gesture_properties.mat', 'left_rotate',...
 'screen_up', 'gesture');
