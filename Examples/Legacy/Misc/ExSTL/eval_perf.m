%
% Script which evaluates the performances of Breach robustness algo.
%

%clear all;
load eval_data.mat;

p1 = STL_Formula('p1','x0[t]>1.5');
np1 = STL_Formula('np1','x0[t]<1.5');
ev = STL_Formula('ev','ev_[0,1] (x0[t]<1.5)');
imp = STL_Formula('imp','(x0[t]>1.5) => (ev_[0,1] (x0[t]<1.5))');
phi1 = STL_Formula('phi1','always ((x0[t]>1.5) => (ev_[0,1] (x0[t]<1.5)))');
phi2 = STL_Formula('phi2','always ((x0[t]>1.5) => (ev_[0,5] (alw_[0,10] (x0[t]<1.5))))');

traj = P0.traj;

time0 = traj.time;
X0 = traj.X;

fprintf('Robustness of')
display(phi1);
for ii = 1:5
    N = size(traj.time,2);
    tic;
    STL_Eval(Sys,phi1,P0,traj,0);
    %SEvalProp(Sys,P0,phi1,0); % SEvalProp is faster thanks to formula optimisation
    fprintf('Execution time for %i samples: %g s\n', N, toc);
    
    for jj=1:10
        traj.time = [traj.time time0(2:end)+traj.time(end)];
        traj.X = [traj.X X0(:,2:end)];
        %P0.traj = traj; % Needed if use SEvalProp
    end
end


phist = 'x0[t]<1.5'
phifix =  'until_[0, 10] (x0[t]<1.5)'

fprintf('Execution time for:\n');

for k=1:8
    phist = [ '(' phist ') ' phifix];
    phi = STL_Formula('phi',phist);
    tic;
    STL_Eval(Sys,phi,P0,traj,0);
    %SEvalProp(Sys,P0,phi,0); % SEvalProp is faster thanks to formula optimisation
    te = toc;
    st = short_disp(phi,30);
    fprintf([st ' i.e. %i untils: %g s\n'],k, te);
end


save('test_properties', 'p1');
save('test_properties', '-append','np1');
save('test_properties', '-append','ev');
save('test_properties', '-append','imp');
save('test_properties', '-append','phi*');

