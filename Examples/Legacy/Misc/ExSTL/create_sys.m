name = 'system';
signals = {'x','y','z'};
params = {'p1', 'p2', 'p3'};
p0 = [0 0 0];

Sys = CreateExternSystem(name, signals, params, p0, @get_sim_data);
