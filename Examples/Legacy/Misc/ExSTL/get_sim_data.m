function [t, X] = get_sim_data(Sys, tspan, p)

load traces
t = traces(:, 1)';
X = traces(:, 2:4)';

end
