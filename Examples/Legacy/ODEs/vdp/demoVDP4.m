fprintf(['\n\n %%%% This demo demonstrates reachable set computations ' ...
         'and plots\n\n' ])
fprintf('\n -- Press any key to continue  -- \n\n')
pause;

fprintf('\n%% Reset and recreate system\n');
fprintf('>> clear all;\n');
fprintf('>> close all;\n');
fprintf('>> CreateSystem;\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;

clear all;
close all;
CreateSystem;

% Reachable sets and Expansion in the state space 
fprintf('\n%% Reachable set computation at time tf \n\n');

mu = input('Give a value for mu ( default .5): ');
tf = input('Give a value for tf (default 1): ');

if isempty(mu)
  mu = .5;
end
if isempty(tf)
  tf = 1;
end

fprintf('\n%% First create a new sampling \n');
fprintf('>> S1 = CreateParamSet(Sys, {''x1'',''x2''},[1.5 2.5 ; -.5 .5]);\n');
fprintf('>> SplotBoxPts(S1)\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;
% First create a new sampling 
S1 = CreateParamSet(Sys, {'x1','x2'},[1.5 2.5 ; -.5 .5]);
S1=SetParam(S1,'mu',mu);
SplotBoxPts(S1)

fprintf('\n%% Then compute *a lot* of trajectories to get an idea of the reachable\n');
fprintf('%% set at time tf\n');
fprintf('>> RS1 = Refine(S1,100);\n');
fprintf('>> RSf1 = ComputeTraj(Sys,RS1, {0 tf});\n');
fprintf('>> SplotXf(RSf1,[],[],{''.r'',''MarkerSize'',6});\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;
% Then compute *a lot* of trajectories to get an idea of the reachable
% set at time tf;
RS1 = Refine(S1,20);
RSf1 = ComputeTraj(Sys,RS1, {0 tf});
SplotXf(RSf1,[],[],{'.r','MarkerSize',4});

fprintf('\n%% What do we get with one trajectory ?\n');
fprintf('>> SplotReach(Sys, S1, {0 tf});\n')
fprintf('\n -- Press any key to execute  -- \n'); pause;

SplotReach(Sys, S1, {0 tf});

% What do we get with one trajectory ?

fprintf('\n%% What about 4 trajectories ?\n\n');
fprintf('>> Sclear; SplotBoxPts(S1); SplotXf(RSf1); %% Reset figure\n');
fprintf('>> rS1 = Refine(S1,2);\n');
fprintf('>> Sf1 = ComputeTrajSensi(Sys, rS1, [0 tf]);\n');
fprintf('>> SplotReach(Sys, S1, {0 tf});\n')
fprintf('\n -- Press any key to execute  -- \n'); pause;


Sclear; SplotBoxPts(S1); SplotXf(RSf1,[],[],{'.r','MarkerSize',4}); % Reset figure
rS1 = Refine(S1,2);
Sf1 = ComputeTrajSensi(Sys, rS1, {0 tf});
SplotReach(Sys, rS1, {0 tf});


fprintf('\n\n %% Now let''s try the sreach routine, with error control \n');
fprintf(' %% Coarse tolerance\n');
fprintf('>> options.tol=.05;\n');
fprintf('>> [Sf ErrStat] = sreach(Sys,S1, {0 tf}, options)\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;


% Now let's try the sreach routine, with error control 
% Coarse tolerance
options.tol=.05;
[Sf, ErrStat] = sreach(Sys,S1, {0 tf}, options);

fprintf('\n\n%% plot the result\n');
fprintf('>> Sclear; SplotBoxPts(S1); SplotXf(RSf1); %% Reset figure\n');
fprintf('>> rS1 = Refine(S1,10);\n');
fprintf('>> Sf1 = ComputeTrajSensi(Sys, Sf, {0 tf});\n');
fprintf('>> SplotXf(Sf1,[],[], {''kx'',''MarkerSize'',14});\n');
fprintf('>> fv = SPolygon2d(Sys,Sf1,{0 tf})\n');
fprintf('>> p = patch(fv)\n');
fprintf('>> set(p,''facecolor'',''none'');\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;
% plot the result

Sclear; SplotBoxPts(S1); SplotXf(RSf1,[],[],{'.r','MarkerSize',4}); % Reset figure
rS1 = Refine(S1,10);
Sf1 = ComputeTrajSensi(Sys, Sf, {0 tf});
fv = SPolygon2d(Sys,Sf,{0 tf});
p = patch(fv);
set(p,'facecolor','none');

% Precise tolerance

fprintf('\n\n%% Precise tolerance\n');
fprintf('>> options.tol=1e-3;\n');
fprintf('>> options.NbIterError = 10; \n');
fprintf('>> [Sf ErrStat] = sreach(Sys,S1, {0 tf}, options);\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;

% Precise tolerance
options.tol=1e-3;
options.NbIterError = 10; 
[Sf, ErrStat] = sreach(Sys,S1, {0 tf}, options);

fprintf('\n\n%% Note the quadratic convergence of error\n');
fprintf('>> Sclear; SplotBoxPts(S1); SplotXf(RSf1); %% Reset figure\n');
fprintf('>> Sf1 = ComputeTrajSensi(Sys, Sf, {0 tf});\n');
fprintf('>> SplotXf(Sf1,[],[], {''kx'',''MarkerSize'',14});\n');
fprintf('>> fv = SPolygon2d(Sys,Sf1,{0 tf})\n');
fprintf('>> p = patch(fv)\n');
fprintf('>> set(p,''facecolor'',''none'');\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;

% plot the result
Sclear; SplotBoxPts(S1); SplotXf(RSf1,[],[],{'.r','MarkerSize',4}); % Reset figure
Sf1 = ComputeTrajSensi(Sys, Sf, {0 tf});
fv = SPolygon2d(Sys,Sf,{0 tf});
p = patch(fv);
set(p,'facecolor','none');

