fprintf(['\n\n %%%% This demo demonstrates basic trajectory computations ' ...
         'and plots\n\n' ])
fprintf('\n -- Press any key to continue  -- \n\n')
pause;

% Create and compile system
fprintf(['\n %% First create and compile system (note that you can skip ' ...
          'compilation if you already did it and didn''t modify the C sources) ' ...
          'of the system \n\n' ]);
fprintf('>> CreateSystem;\n');
fprintf('>> CompileSystem(Sys);\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;

CreateSystem
CompileSystem(Sys);


fprintf('%% Reset and recreate system\n');
fprintf('>> clear all;\n');
fprintf('>> close\n');
fprintf('>> CreateSystem;\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;

clear all;
close;
CreateSystem;

fprintf('%% Create a refined sampling Smur\n');
fprintf('>> Pmu = CreateParamSet(Sys,{''mu''}, [0, 2]);\n');
fprintf('>> Pmu = SetParam(Pmu,{''x1'',''x2'',''mu''},[1;1;0]);\n');
fprintf('>> Pmur = Refine(Pmu,100);\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;
Pmu = CreateParamSet(Sys,{'mu'}, [0, 2]);
Pmu = SetParam(Pmu,{'x1','x2','mu'},[1;1;0]);
Pmur = Refine(Pmu,100);

fprintf('\n%% Compute and plot trajectories\n\n');
fprintf('>> Pfmur = ComputeTraj(Sys,Pmur, [0, 7]);\n');
fprintf('>> h = figure;\n');
fprintf('>> SplotTraj(Pfmur);\n');
fprintf('>> axis([-3 3 -4 4 ]); \n');
fprintf('\n -- Press any key to execute  -- \n'); pause;

Pfmur = ComputeTraj(Sys,Pmur, [0, 7]);
h = figure;
SplotTraj(Pfmur);
axis([-3 3 -4 4 ]); 

fprintf('\n%% Compute only end points \n');
fprintf('>> Pfmur = ComputeTraj(Sys,Pmur, {0 7});\n');
fprintf('>> clf\n');
fprintf('>> SplotTraj(Pfmur,[],[],''--b'');\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;

% Compute only end points 
Pfmur = ComputeTraj(Sys,Pmur, {0 7});
%Sclear; % NM: what is the expected behavior of Sclear ? I guess like clf
clf %NM : I added this line
SplotTraj(Pfmur,[],[],'--b');

% Trajectories from Sgrid and S1r
fprintf('\n%% Create samplings Sgrid and S1r\n\n');
fprintf('>> P0 = CreateParamSet(Sys,[1 2], [-2 2 ; -2 2]); \n');
fprintf('>> P1 = CreateParamSet(Sys, ''x1'', [ 1.5 2.5 ]);\n');
fprintf('>> Pgrid = Refine(P0,10); %% refine P0 into a grid of 10x10 \n');
fprintf('>> P1r = Refine(P1,.1); %% refine P1 with new points distant from less than .1\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;

P0 = CreateParamSet(Sys,[1 2], [-2 2 ; -2 2]); 
P1 = CreateParamSet(Sys, 'x1', [ 1.5 2.5 ]);
Pgrid = Refine(P0,10); %% refine P0 into a grid of 10x10 
P1r = Refine(P1,.1); %% refine P1 with new points distant from less than .1

fprintf('\n%% Trajectories from Pgrid\n\n');
fprintf('>> Pf0 = ComputeTraj(Sys, Pgrid, [0, 7])\n');
fprintf('>> clf\n');
fprintf('>> SplotTraj(Pf0);\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;
Pf0 = ComputeTraj(Sys, Pgrid, [0, 7])
%Sclear;
clf;
SplotTraj(Pf0);

fprintf('\n%% Trajectories from P1r\n\n');
fprintf('>> Pf1r = ComputeTraj(Sys, P1r, [0 7]);\n');
fprintf('>> Pf1r.X0plot_opt = {''*r''};\n');
fprintf('>> SplotTraj(Pf1r, [], [], {''-r''}); %% plots in red\n '); 
fprintf('\n -- Press any key to execute  -- \n'); pause; 
Pf1r = ComputeTraj(Sys, P1r, [0 7]);
Pf1r.X0plot_opt = {'*r'};
SplotTraj(Pf1r, [], [], {'-r'}); %% plots in red

% Trajectories starting from Sallr
fprintf('\n%% Trajectories starting from Sallr\n\n');
fprintf('>> Pall = CreateParamSet(Sys,{''x1'',''x2'',''mu''},[ -2.5 -1.5 ; -.5 .5 ; .8 1.2]);\n');
fprintf('>> Pallr = Refine(Pall, [10 10 5]); %% refine Sall into a 10x10x5 grid\n');
fprintf('>> Pfall = ComputeTraj(Sys, Pallr, [0 7]);\n');
fprintf('>> clf\n');
fprintf('>> SplotTraj(Pfall)\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;

Pall = CreateParamSet(Sys,{'x1','x2','mu'},[ -2.5 -1.5 ; -.5 .5 ; .8 1.2]);
Pallr = Refine(Pall, [10 10 5]); %% refine Sall into a 10x10x5 grid
Pfall = ComputeTraj(Sys, Pallr, [0 7]);
%Sclear;
clf %NM : I added this line
SplotTraj(Pfall)

fprintf('\n%% points at time instants computed by CVodes\n\n');
fprintf('>> Sys.CVodesOptions.RelTol = 1e-3;\n');
fprintf('>> Sys.CVodesOptions.AbsTol = 1e-3;\n');
fprintf('>> Sf2 = ComputeTraj(Sys, S1, [0 6]); \n');
fprintf('>> SplotTraj(Sf2);\n');
fprintf('>> SplotTraj(Sf2, [], [], {''sg'', ''MarkerSize'', 4});\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;

Sys.CVodesOptions.RelTol = 1e-3;
Sys.CVodesOptions.AbsTol = 1e-3;
Sf2 = ComputeTraj(Sys, P1, [0 6]); 
%Sclear;
clf %NM : I added this line
SplotTraj(Sf2);
SplotTraj(Sf2, [], [], {'sg', 'MarkerSize', 4});

fprintf(['\n%% points at time instants computed by CVodes (better precision) ' ...
         '\n\n']);
fprintf('>> Sys.CVodesOptions.RelTol = 1e-8;\n');
fprintf('>> Sys.CVodesOptions.AbsTol = 1e-10;\n');
fprintf('>> Sf2 = ComputeTraj(Sys, S1, [0 6]); \n');
fprintf('>> SplotTraj(Sf2);\n');
fprintf('>> SplotTraj(Sf2, [], [], {''+y'', ''MarkerSize'', 4});\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;
Sys.CVodesOptions.RelTol = 1e-8;
Sys.CVodesOptions.AbsTol = 1e-10;
Sf2 = ComputeTraj(Sys, P1, [0 6]); 
SplotTraj(Sf2);
SplotTraj(Sf2, [], [], {'+y', 'MarkerSize', 4});

% forces regularly spaced time instants
fprintf('\n%% same precision but outputs points with a fixed time step of .25\n\n');
fprintf('>> Sf2time = ComputeTraj(Sys, S1, 0:.25:6);\n');
fprintf('>> SplotTraj(Sf2time);\n');
fprintf('>> SplotTraj(Sf2time, [], [], {''xr'', ''MarkerSize'', 10});\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;
Sf2time = ComputeTraj(Sys, P1, 0:.25:6);
SplotTraj(Sf2time);
SplotTraj(Sf2time, [], [], {'xr', 'MarkerSize', 6});

