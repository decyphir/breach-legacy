fprintf(['\n\n %%%% This demo demonstrates more trajectory plottings ' ...
         'and sensitivity computations\n\n' ])
fprintf('\n -- Press any key to continue  -- \n\n')
pause;


fprintf('%% Reset and recreate system\n');
fprintf('>> clear all;\n');
fprintf('>> close all;\n');
fprintf('>> CreateSystem;\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;

clear all;
close all;
CreateSystem;

fprintf('%% Create a refined sampling Pmur\n');
fprintf('>> Pmu = CreateParamSet(Sys,{''mu''}, [ 0 2 ]);\n');
fprintf('>> Pmu = SetParam(Pmu,{''x1'',''x2'',''mu''},[1;1;0]);\n');
fprintf('>> Pmur = Refine(Pmu,10);\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;

Pmu = CreateParamSet(Sys,{'mu'}, [ 0 2 ]);
Pmu = SetParam(Pmu,{'x1','x2','mu'},[1;1;0]);
Pmur = Refine(Pmu,10);

% Compute and plot trajectories

% Compute trajectories starting from Smur

fprintf('\n%% Compute trajectories starting from Pmur\n');
fprintf('>> Pfmur = ComputeTraj(Sys, Pmur, [0 7]);\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;
Pfmur = ComputeTraj(Sys, Pmur, [0 7]);

fprintf('\n%% Plot all trajectories in Pfmur\n\n');
fprintf('>> SplotVar(Pfmur, {''x1'',''x2''});\n'); 
fprintf('\n -- Press any key to execute  -- \n'); pause;
SplotVar(Pfmur, {'x1','x2'}); % Plot all trajectories in Sfmur

fprintf('\n%% Plot only first trajectory in Pfmur\n');
fprintf('>> clf; SplotVar(Pfmur, {''x1'',''x2''},1); %% Plot first trajectory in Pfmur\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;
clf; SplotVar(Pfmur, {'x1','x2'},1); % Plot first trajectory in Pfmur

fprintf('\n%% Plot trajectories 2 to 10 in Pfmur\n');
fprintf('>> clf; SplotVar(Pfmur, {''x1'',''x2''}, 2:10);\n')
fprintf('\n -- Press any key to execute  -- \n'); pause;
clf; SplotVar(Pfmur, {'x1','x2'}, 2:10); % Plot trajectories 2 to 10 in Pfmur
 
% Compute sensitivity matrices for trajectories starting in Pall

fprintf('\n%% Trajectories starting from Pall\n\n');
fprintf('>> Pall = CreateParamSet(Sys,{''x1'',''x2'',''mu''},[ -2.5 -1.5 ; -.5 .5 ; .8 1.2]);\n');
fprintf('\n%% Compute and plot trajectories\n');
fprintf('>> Pfall = ComputeTrajSensi(Sys, Pall, [0 7])\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;
Pall = CreateParamSet(Sys,{'x1','x2','mu'},[ -2.5 -1.5 ; -.5 .5 ; .8 1.2]);
Pfall = ComputeTrajSensi(Sys, Pall, [0 7]);

fprintf('\n%% plot sensitivities of x1 and x2 w.r.t. mu \n');
fprintf('>> clf; SplotSensi(Pfall,{''x1'',''x2''}, {''mu''})\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;
clf; SplotSensi(Pfall,{'x1','x2'}, {'mu'}); 

fprintf('\n%% plot sensitivities of x1  w.r.t. x1(0) and x2(0)\n');
fprintf('>> clf; SplotSensi(Pfall,{''x1''}, {''x1'',''x2''})\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;

clf; SplotSensi(Pfall,{'x1'}, {'x1','x2'})

%plot everything
fprintf('\n%%plots everything\n\n');
fprintf('>> clf; SplotSensi(Pfall) \n');
fprintf('\n -- Press any key to execute  -- \n'); pause;
clf; SplotSensi(Pfall) % plot everything 

fprintf('\n%% Estimated Expansion due to uncertainties in mu\n');

fprintf('>> Pfmur = ComputeTrajSensi(Sys, Pmu, [0 7]);\n');
fprintf('>> clf; SplotVarExp(Pfmu, [], 1);');
fprintf('\n -- Press any key to execute  -- \n'); pause;

Pfmu = ComputeTrajSensi(Sys, Pmu, [0 7]);
clf; SplotVarExp(Pfmu, [], 1);


fprintf('\n%% Expansion due to uncertainties in mu, refined with more trajectories\n');

fprintf('>> Pfmur = ComputeTrajSensi(Sys, Pmur, [0 7]);\n');
fprintf('>> clf; SplotVarExp(Pfmur);\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;

Pfmur = ComputeTrajSensi(Sys, Pmur, [0 7]);
clf; SplotVarExp(Pfmur);
