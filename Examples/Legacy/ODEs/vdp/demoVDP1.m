fprintf('\n\n %%%% This demo demonstrates basic operations on sampling sets\n\n')
fprintf('\n -- Press any key to continue  -- \n\n')
pause;

fprintf('%% Reset and recreate system\n');
fprintf('>> clear all;\n');
fprintf('>> close\n');
fprintf('>> CreateSystem;\n');
fprintf('\n -- Press any key to execute  -- \n')
pause
clear all;
close;
CreateSystem;


%% Create box samplings and refine them 
fprintf('\n%%%% Create box samplings and refine them \n\n');
fprintf('\n -- Press any key to continue  -- \n\n')
pause;
% This spans a large part of the state space (x1,x2)
fprintf('\n%% The following spans a large part of the state space (x1,x2)\n\n');
fprintf('>> P0 = CreateParamSet(Sys,[1 2], [-2 2 ; -2 2]) \n');
fprintf('\n -- Press any key to execute  -- \n')
pause;

P0 = CreateParamSet(Sys,[1 2], [-2 2 ; -2 2])

% A segment parallel to x1 axis around (.5,1) of size .5
fprintf('\n%% This creates a segment parallel to x1 axis around (.5,1) of size .5\n\n');
fprintf('>> P1 = CreateParamSet(Sys,{''x1''}, [ 1.5 2.5 ])\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;

P1 = CreateParamSet(Sys,{'x1'}, [ 1.5 2.5 ])

% A segment for parameter mu, with initial state (1,1)
fprintf('\n%% A segment for parameter mu, with initial state (1,1)\n\n');
fprintf('>> Pmu = CreateParamSet(Sys, ''mu'', [ -2 2 ]);\n');
fprintf('>> Pmu = SetParam(Pmu,{''x1'',''x2''},[1;1])\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;
Pmu = CreateParamSet(Sys, 'mu', [ -2 2 ]);
Pmu = SetParam(Pmu,{'x1','x2'},[1;1])

fprintf('\n%% A box with all three parameters\n');
fprintf('>> Pall = CreateParamSet(Sys,{''x1'',''x2'',''mu''},[ -2.5 -1.5 ; -.5 .5 ; .8 1.2])\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;
Pall = CreateParamSet(Sys,{'x1','x2','mu'},[ -2.5 -1.5 ; -.5 .5 ; .8 1.2])

fprintf('\n%% Refine and plot box sampling\n\n');
fprintf('>> Pgrid = Refine(P0,10); %% refine P0 into a grid of 10x10 \n');
fprintf('>> P1r = Refine(P1,.1); %% refine P1 with new points distant from less than .1\n');
fprintf('>> Pmur = Refine(Pmu,10);\n');
fprintf('Pallr = Refine(Pall, [10 10 5]); %% refine Pall into a 10x10x5 grid\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;
Pgrid = Refine(P0,10); %% refine P0 into a grid of 10x10 
P1r = Refine(P1,.1); %% refine P1 with new points distant from less than .1
Pmur = Refine(Pmu,10);
Pallr = Refine(Pall, [10 10 5]); %% refine Pall into a 10x10x5 grid

fprintf('\n%% plot box samplings\n\n');
fprintf('>> figure;\n');
fprintf('>> SplotPts(Pgrid);\n');
fprintf('>> SplotPts(P1r,[],[],{''*r''});\n');
fprintf('>> SplotPts(Pmur,[],[],{''ok''});\n');
fprintf('>> SplotPts(Pallr,[],[],{''sb''});\n');
fprintf('>> view(-20,20);\n');
fprintf('\n -- Press any key to execute  -- \n'); pause;
figure;
SplotPts(Pgrid);
SplotPts(P1r,[],[],{'*r'});
SplotPts(Pmur,[],[],{'ok'});
SplotPts(Pallr,[],[],{'sb'});
view(-20,20);

