echo on

% This demo assumes that we start with an empty directory
% It creates every file you need to be able to continue 
% with demoVDPx.m files
pause
ls
pause

%% The following creates the source files for the system\n');
pause

NewSys.x0Values = {'x1(0)=2.', 'x2(0)=0.'};
NewSys.ParamValues={'mu=1.'}; 
NewSys.Derivatives={'x1''=x2','x2''=mu*(1-x1*x1)*x2-x1+cos(__t)'};
NewSystem(NewSys);
pause
ls
pause
echo off
