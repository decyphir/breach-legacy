function val = f_phi(param,Sys)
%F_PHI computes evaluation of
% ((x0[0] > x1[0]) and (x1[0] < 0.2)) and ((15 < F) and (F < 15.5))
% 
% phi = STL_Formula('phi','f_phi(traj.param,params.Sys)>=0');
%

idx_x0 = FindParam(Sys,'x0');
idx_x1 = FindParam(Sys,'x1');
idx_F = FindParam(Sys,'F');

val = [...
    param(idx_x0) - param(idx_x1), ... % x0[0] > x1[0]
    0.2-param(idx_x1), ...             % x1[0] < 0.2
    param(idx_F) - 15 ...              % 15 < F
    15.5 - param(idx_F), ...           % F < 15.5
];

val = min(val);

end
