#include "dynamics.h"

int f(realtype __t, N_Vector __x, N_Vector __xdot, void *__f_data) {

    Fdata* __data = (Fdata*) __f_data;

    // Variables
    realtype x0 = Ith(__x,0);
    realtype x1 = Ith(__x,1);
    realtype x2 = Ith(__x,2);

    // System parameters
    realtype a = __data->p[3];
    realtype b = __data->p[4];
    realtype F = __data->p[5];
    realtype G = __data->p[6];

    // Equations
    Ith(__xdot,0)=-x1*x1-x2*x2-a*x0+a*F;
    Ith(__xdot,1)=x0*x1-b*x0*x2-x1+G;
    Ith(__xdot,2)=b*x0*x1+x0*x2-x2;

    return(0);
}


void InitFdata(void * &__f_data, const mxArray * mxData) {

    Fdata* __data = (Fdata*) __f_data;

    __data->dimx = 3;  // number of variables
    __data->dimu = 0;
    __data->dimg = DIMG;  // number of modes
    __data->dimp = 7;  //number of system parameters
    __data->p = mxGetPr(mxGetField(mxData,0,"p"));
}


int UpdateFdata(realtype __t, N_Vector __x, void * &__f_data, int __Ns, N_Vector* __xS) {
    return 0;
}


int g(realtype __t, N_Vector __x, realtype *__gout, void *__f_data) {
    return 0;
}


int Jac(long int __N, DenseMat __J, realtype __t, 
        N_Vector __x, N_Vector __fx, void *__jac_data,
        N_Vector __tmp1, N_Vector __tmp2, N_Vector __tmp3) {
    return 0;
}
