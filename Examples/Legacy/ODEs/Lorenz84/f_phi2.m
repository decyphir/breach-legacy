function val = f_phi2(param)
%F_PHI computes evaluation of
% ((x0[0] > x1[0]) and (x1[0] < 0.2)) and ((15 < F) and (F < 15.5))
% 
% phi = STL_Formula('phi','f_phi2(traj.param)>=0');
%

val = [...
    param(1) - param(2), ... % x0[0] > x1[0]
    0.2-param(2), ...        % x1[0] < 0.2
    param(6) - 15 ...        % 15 < F
    15.5 - param(6), ...     % F < 15.5
];

val = min(val);

end
