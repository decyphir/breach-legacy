%% This tutorial demonstrates how to find parameter values
%% where simulations exhibit oscillations 

CreateSystem;

% Finding property parameter: oscillation period

P = CreateParamSet(Sys, {'T'}, [1,4]);
P = SetParam(P, {'a'}, .05);

opt.tspan = 0:.01:50;
opt.params = {'T'};

opt.MaxIter= 10000;

[val_opt, Popt] = SOptimProp(Sys, P, phi_oscilT, opt);

Tfound = GetParam(Popt,'T');
disp(['Found period T=' num2str(Tfound)]);

pause

% Finding system parameters, given period T

P = CreateParamSet(Sys, {'a', 'F'}, [0 2; 0 20]);
P = SetParam(P, {'T'}, 2);

% Optimize over 100 initial points
P = QuasiRefine(P, 100);

opt.tspan = 0:.01:50;
opt.MaxIter= 10000;
opt.Ninit = 5;
opt.StopWhenFound =1;
[val_opt, Popt] = SOptimProp(Sys, P, phi_oscilT, opt);


GetParam(Popt,{'a'})
SplotVar(Popt, 'x1');

%disp(['Found period T=' num2str(Tfound)]);

% 



