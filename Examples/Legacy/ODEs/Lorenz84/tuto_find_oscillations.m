%% This tutorial demonstrates how to find a subregion of a parameter set
%% where simulations exhibit oscillations 
close all 
clear;
%% first init Lorenz system (create Sys structure) 
CreateSystem;

%% create a parameter set varying 'F' and 'G' in range [0 2]
echo on;
P = CreateParamSet(Sys, {'F','G'}, [0 2; 0 2]);

%% Sample F and G in a 10x10 grid 
P = Refine(P, [10 10]);

%% Compute and plot trajectories
tspan= 0:.01:50;
P = ComputeTraj(Sys, P, tspan);
figure
SplotVar(P) 
pause

%% load oscillations properties
oscil_prop_names = STL_ReadFile('oscil_prop.stl');

%% add formula parameters to param set P 
P = SetParam(P, {'x1h', 'x1l', 'T'}, [.3, -.3, 5]);

%% eval x1_oscil on trajectories in P 
[P, val] = SEvalProp(Sys, P, x1_oscil );

%% Plot satisfaction function with respect to (F,G)
figure;
SplotProp(P, x1_oscil);
% Above the 0 level contour means trajectories oscillate 
pause

%% Refining the boundary between good and bad parameters
Pr = SFindPropBoundary(Sys, P, x1_oscil, tspan, 0, 3);
figure;
SplotPts(Pr);
pause;
% plotting with colors
Partition = DiscrimPropValues(Pr); % Separates good and bad parameters
Pbad = Partition(1);
Pgood = Partition(2);
figure;
SplotPts(Pbad,[],[],'r+'); 
SplotPts(Pgood,[],[],'g+'); 
pause;

%% Extract the 'best' oscillating trajectory
[bval, i_best] = max(val);
Pbest = Sselect(P, i_best);

%% Plot 'best' traj
figure;
SplotVar(Pbest);
pause

%% Plot satisfaction function of property and sub-properties up to depth 6
depth= 6;
figure;
SplotSat(Sys, Pbest, x1_oscil, depth);


%% Save properties and  parameter sets for examination in the GUI (Breach(Sys))
Propsave(Sys, oscil_prop_names{:})
Psave(Sys,'P', 'Pbest','Pr')
echo off;
