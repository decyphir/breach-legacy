%% This script describes the use of init_fun field in a parameter set to sample initial conditions in a non-rectangular set
%

Init_Lorenz84;

% Create a parameter set varying x0(0) and x1(0)

P = CreateParamSet(Sys, {'x0', 'x1'},  [0 1; 0 1]);

% sum1_fn is a function which sets up x2(0) to be equal to 1-x1(0)-x2(0)
% it will be called on P and refined samplings of P before computing any trajectory
P.init_fun = @sum1_fn;

% Sampling P quasi - randomly
Pr = QuasiRefine(P, 100);
Pf = ComputeTraj(Sys, Pr, 0:.1:10);

% plots initial points in Pf
figure;
SplotPts(Pf, [1 2 3]);

% checks that all X0 are in [0,1] and sum(X0_i) == 1
X0 = Pf.pts(1:Pf.DimX, :);
any(X0>1 & X0<0)
sum(X0)


% checks some property on Pf
phi = STL_Formula('phi', 'ev ( x0[t] > 2.5 ) ');
[Pf, val] = SEvalProp(Sys, Pf, phi); % val gets the satisfaction values, also store in the field Pf.prop_values


Partition = DiscrimPropValues(Pf);
figure;
SplotPts(Partition, [1 2 3]);
