
CreateSystem;
P = CreateParamSet(Sys);
tspan = 0:.01:20;
P = ComputeTraj(Sys, P, tspan);

x0 = P.traj.X(1,:);
x1 = P.traj.X(2,:);
x2 = P.traj.X(3,:);

% loading predicates
STL_ReadFile('predicates.stl')

% simplest predicate x1[t] > 0
tau = tspan(1:10:end);
vmu0 = STL_Eval(Sys, mu0, P, P.traj, tau);
plot(tau, vmu0, tspan, x1);

% dist x1[t]^2+ x2[t]^2 >0
figure
vmu_dist = STL_Eval(Sys, mu_dist, P, P.traj, tau);
plot(tau, vmu_dist, tspan, x1.^2+x2.^2);

%mu_shift := x1[t-1.3]       >0
figure
vmu_shift = STL_Eval(Sys, mu_shift, P, P.traj, tau);
plot(tau, vmu_shift, tspan, x1);

%mu_sin := sin(x1[t])        >0
figure
vmu_sin = STL_Eval(Sys, mu_sin, P, P.traj, tau);
plot(tau, vmu_sin, tspan, sin(x1));

%mu_dt := ddt{x1[t]}         >0 
figure
vmu_dt = STL_Eval(Sys, mu_dt, P, P.traj, tau);
plot(tau, vmu_dt, tspan, x1);

%mu_dp := d{x2}{a}[t]                >0
figure
vmu_dp = STL_Eval(Sys, mu_dp, P, P.traj, tau);
plot(tau, vmu_dp, tspan, x1);

%mu_param := p0*x1[t] + q0_x2*x2[t]  >0
figure
p0 = 1.4; q0_x2 = 5.4;
P = SetParam(P, {'p0', 'q0_x2'}, [p0 q0_x2]);
vmu_param = STL_Eval(Sys, mu_param, P, P.traj, tau);
plot(tau, vmu_param, tspan, p0*x1 + q0_x2*x2);

%mu_param2 := x11*x1[t] + q0_x2*x2[t] >0
figure
x11 = 2.4; p_x2_c = -4;
P = SetParam(P, {'x11', 'p_x2_c'}, [x11 p_x2_c]);
vmu_param2 = STL_Eval(Sys, mu_param2, P, P.traj, tau);
plot(tau, vmu_param2, tspan, x11*x1 + p_x2_c*x2);
  
