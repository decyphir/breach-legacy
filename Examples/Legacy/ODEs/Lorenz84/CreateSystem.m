% System Definition 

InitBreach;
clear Sys;
Sys.DimX = 3;
Sys.DimU = 0;
Sys.DimP = 7;
Sys.ParamList = {'x0','x1','x2',...
         'a', 'b', 'F', 'G'};
Sys.x0 = [0.,0.,0.]';
Sys.p = [0.,0.,0.,...
         .25, 4, 15, 2]';
Sys.Dir = pwd;
options = [];
options = CVodeSetOptions(options,'RelTol',1.e-6,'AbsTol',1.e-6,'MaxNumSteps',100000);
Sys.CVodesOptions = options;

% options for sensitivity computation

pscales = ones(Sys.DimP,1);
FSAoptions = CVodeSetFSAOptions('SensErrControl','on',...
                               'ParamField', 'p',...
                               'ParamScales',pscales );
Sys.CVodesSensiOptions.method = 'Simultaneous';
Sys.CVodesSensiOptions.FSAoptions = FSAoptions;
