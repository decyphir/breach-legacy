function test_Eval()
%TEST_EVAL This file compare the time for evalution of a conjunction of
% inequalities written in different manner. It also test the efficiency of
% the STL_OptimizeFormula function. These tests are also done for a
% formula of type ev (alw (phi))
%

phi(1) = STL_Formula('phi1','f_phi(traj.param,params.Sys)>=0');
phi(2) = STL_Formula('phi2','f_phi2(traj.param)>=0');
phi(3) = STL_Formula('phi3','((x0[0] > x1[0]) and (x1[0] < 0.2)) and ((15 < F) and (F < 15.5))');
phi(4) = phi(3); % to avoid a call to an empty constructor
phi(5) = STL_Formula('phi5','ev (alw_[0,1] (phi1))');
phi(6) = STL_Formula('phi6','ev (alw_[0,1] (phi2))');
phi(7) = STL_Formula('phi7','ev (alw_[0,1] (phi3))');
phi(4) = STL_OptimizeFormula(phi(3));
phi(8) = STL_OptimizeFormula(phi(7));

CreateSystem
P = CreateParamSet(Sys);
P = QuasiRefine(P,100);
P = ComputeTraj(Sys,P,0:0.1:10);

tocTable = zeros(1,8);

for jj = 1:8
    ticId = tic;
    for ii=1:5
        SEvalProp(Sys,P,phi(jj),0);
    end
    tocTable(jj) = toc(ticId);
end

for jj = 1:8
    fprintf('phi(%d) : %g secondes\n',jj,tocTable(jj));
end

end
