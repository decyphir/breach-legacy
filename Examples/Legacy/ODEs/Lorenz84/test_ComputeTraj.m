%% Init system and parameter set
clear
CreateSystem;

% Create nominal parameter set 
P0 = CreateParamSet(Sys); 

%% Compute Nominal trajectories
echo on
Pf = ComputeTraj(Sys, P0, [0 10]);
figure;
SplotTraj(Pf);
pause

P0 = SetParam(P0, {'x0','a','b'}, [ 1; 1.5; 3]) % change values of x0, a and b
Pf1 = ComputeTraj(Sys, P0, 0:.01:10) 
figure;
SplotVar(Pf);  
pause

%% Plotting several trajectories
Pr = QuasiRefine(P0,20);
Pf = ComputeTraj(Sys, Pr, 0:.1:10);
figure;
SplotTraj(Pf);
echo off
