function P = sum1_fn(P)
% rearrange pts in P so that sum(P.pts) = [1 1 ... 1]
% and P.pts(i,j) \in (0,1) for all (i,j)
%
% assumes that P.pts(1:DimX, :) is in (0,1)
%

X0 = P.pts(1:P.DimX,:);
sX0 = sort(X0);
dsX0 = diff(sX0);
P.pts(1:P.DimX,:) = [ dsX0 ; 1 - sum(dsX0) ];  

end
