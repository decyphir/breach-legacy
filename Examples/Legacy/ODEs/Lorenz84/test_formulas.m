Init_Lorenz84;

Tf = 100;
N = 1e6;
tspan = linspace(0, Tf, N);

P0 = CreateParamSet(Sys);
P0 = ComputeTraj(Sys, P0, tspan);

traj = P0.traj;

STL_Formula('phi1','x0[t]> .2')
val = STL_Eval(Sys,phi1,P0, traj,0)

STL_Formula('phi2',' x1[t] < .1 ')
val = STL_Eval(Sys,phi2,P0, traj,0)

STL_Formula('phi_not','not (x1[t]<.2)')
val = STL_Eval(Sys,phi_not,P0, traj,0)

STL_Formula('phi_or','(x1[t]<.2) or (x2[t]<.3)')
val = STL_Eval(Sys,phi_or,P0, traj,0)

STL_Formula('phi_and','(x1[t]<.2) and (x2[t]<.3)')
val = STL_Eval(Sys,phi_or,P0, traj,0)

phi_and_or = STL_Formula('phi_and_or','(x1[t]<.2) and ((x2[t]<.3) or (x0[t]>=-.1))')
val = STL_Eval(Sys,phi_and_or,P0, traj,0)

phi_and_or2 = STL_Formula('phi_and_or2','((x1[t]<.2) and (x2[t]<.3)) or (x0[t]>=-.1)')
val = STL_Eval(Sys,phi_and_or2,P0, traj,0)

phi_ev = STL_Formula('phi_eventually','eventually (x1[t]<.2)')
val = STL_Eval(Sys,phi_eventually,P0, traj,0)

phi_alw = STL_Formula('phi_alw','always (x1[t]<.2)')
val = STL_Eval(Sys,phi_alw,P0, traj,0)

phi_ev_I = STL_Formula('phi_ev_I','ev_[2,3] (x1[t]<.2)')
val = STL_Eval(Sys,phi_ev_I,P0, traj,0)

phi_alw_I = STL_Formula('phi_alw_I','alw_[ 2 , 3] (x1[t]<.2)')
val = STL_Eval(Sys,phi_alw_I,P0, traj,0)

phi_until = STL_Formula('phi_until','(x1[t]<.2) until (x1[t]<.2)')
val = STL_Eval(Sys,phi_until,P0, traj,0)

phi_until_I = STL_Formula('phi_until_I','(x1[t]<.2) until_[0,1] (x1[t]<.2)')
val = STL_Eval(Sys,phi_until_I,P0, traj,0)

phi_adv = STL_Formula('phi_adv','(x0[t]>1.) or (ev_[2, 3] ( x1[t]<=-.1 )) ')
val = STL_Eval(Sys,phi_adv,P0, traj,0)

phi_alw_adv = STL_Formula('phi_alw_adv','always ((x0[t]>1.) => (ev_[2, 3] ( x1[t]<=-.1 )))')
val = STL_Eval(Sys,phi_alw_adv,P0, traj,0)

phi_nimp = STL_Formula('phi_nimp','((x1[t]<.2) until_[1,2] (x1[t]>.5)) until_[.5,1.5] (x2[t]<.2)')
val = STL_Eval(Sys,phi_nimp,P0, traj,0)

