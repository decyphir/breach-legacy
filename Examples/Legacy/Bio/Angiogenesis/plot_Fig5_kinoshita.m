%
% Script reproducing the plot of Kinoshita et al 1998, Figure 5 of 
%
%  "Robustness Analysis and Behavior Discrimination in Enzymatic Reaction Networks"
%  by Donzé, A., Fanchon, E., Gattepaille, L. M., Maler O., Tracqui, P.

function plot_Fig5_kinoshita

% load parameter and properties sets and the model

load paper_param_sets;
CreateSystem;

plot_kino(Sys,Pfig2D0,phi_rm2,.7e-8, -14, -5, 1)

xlabel('Initial concentrations of TIMP2 (M)', 'FontSize', 12,'FontWeight','Bold' );
ylabel('Ratio of activated MMP2', 'FontSize', 12,'FontWeight','Bold' );
set(gca,'LineWidth', 1, 'FontSize',12, 'FontWeight','Bold','Xtick',[1e-13 1e-12 1e-11 1e-10 1e-9 1e-8 1e-7 1e-6 1e-5]);

%axis tight;
axis([1e-13 1e-5 0 2]);
grid on;

% Export figure
%set(gcf,'PaperPosition',2*[0 0 3.5 2 ]);
%print(gcf,'-depsc2','kara_kinoshita.eps');
%system('epstopdf kara_kinoshita.eps');
%system('cp kara_kinoshita.pdf /home/donze/LATEX/AngioPaper/figures');


function plot_kino(Sys,P, phi, mt1t20, log1, log2, tf)
   
% Define log values and compute traces
  
  logt2 = logspace(log1,log2,100);
  indt2= 12;
  indmt1t2 = 10; 

  indmt1 = FindParam(Sys, 'mt1');
  indm2p = FindParam(Sys, 'm2p');    
  
  % set values 
  m2p = 1.39e-9;
  mt1 = 41.7e-9;
   
  P.pts(indmt1t2, 1)= mt1t20;
  P.pts(indm2p, 1)= m2p;
  P.pts(indmt1, 1)= mt1;
     
  % reference    
  t20 = 0;
  P0 = P;
  P0.pts(indt2, 1)= t20;
  
  Pf0 = ComputeTraj(Sys, P0, 0:3600:(tf+1)*3600);
  
  Pf0 = SEvalProp(Sys, Pf0, phi, tf*3600); 
  val0 = Pf0.props_values.val;
  
  P = Refine(P,101);
  
  % rest
  
  logt2 = [0 logt2];
  P.pts(indt2, :) = logt2;
    
  Pf  = ComputeTraj(Sys, P, 0:3600:(tf+1)*3600);
  [Pf val]  = SEvalProp(Sys, Pf, phi, tf*3600); 
%  val = SplotProp(Pf, phi); 
%  close;
  figure;  
  semilogx(logt2,val/val0);
