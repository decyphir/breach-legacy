#include "dynamics.h"

int f(realtype t, N_Vector x, N_Vector xdot, void *f_data) {

  Fdata* data = (Fdata*) f_data;

  double c1 = Ith(x,0);
  double c1dm2 = Ith(x,1);
  double c1dmt1 = Ith(x,2);
  double m2 = Ith(x,3);
  double m2_c1 = Ith(x,4);
  double m2_t2 = Ith(x,5);
  double m2_t2_star = Ith(x,6);
  double m2p = Ith(x,7);
  double mt1 = Ith(x,8);
  double mt1_t2 = Ith(x,9);
  double mt1_t2_m2p = Ith(x,10);
  double t2 = Ith(x,11);

  double d_m2 = data->p[12];
  double d_m2t2star = data->p[13];
  double d_t2 = data->p[14];
  double k_iso_m2t2 = data->p[15];
  double kact_eff_m2 = data->p[16];
  double kcat_m2c1 = data->p[17];
  double kcat_mt1c1 = data->p[18];
  double ki_mt1t2 = data->p[19];
  double kiso_m2t2 = data->p[20];
  double km_mt1c1 = data->p[21];
  double koff_m2c1 = data->p[22];
  double koff_m2t2 = data->p[23];
  double koff_mt1t2m2p = data->p[24];
  double kon_m2c1 = data->p[25];
  double kon_m2t2 = data->p[26];
  double kon_mt1t2 = data->p[27];
  double kon_mt1t2m2p = data->p[28];
  double kshed_eff = data->p[29];
  double p_c1 = data->p[30];
  double p_m2p = data->p[31];
  double p_mt1 = data->p[32];
  double p_t2 = data->p[33];

  Ith(xdot,0) = p_c1-kon_m2c1*m2*c1+koff_m2c1*m2_c1-kcat_mt1c1/km_mt1c1*mt1*c1;
  Ith(xdot,1) = kcat_m2c1*m2_c1;
  Ith(xdot,2) = kcat_mt1c1/km_mt1c1*mt1*c1;
  Ith(xdot,3) = kact_eff_m2*mt1*mt1_t2_m2p-kon_m2t2*m2*t2+koff_m2t2*m2_t2-kon_m2c1*m2*c1+koff_m2c1*m2_c1+kcat_m2c1*m2_c1-d_m2*m2;
  Ith(xdot,4) = kon_m2c1*m2*c1-koff_m2c1*m2_c1-kcat_m2c1*m2_c1;
  Ith(xdot,5) = kon_m2t2*m2*t2-koff_m2t2*m2_t2-kiso_m2t2*m2_t2+k_iso_m2t2*m2_t2_star;
  Ith(xdot,6) = kiso_m2t2*m2_t2-k_iso_m2t2*m2_t2_star-d_m2t2star*m2_t2_star;
  Ith(xdot,7) = p_m2p-kon_mt1t2m2p*mt1_t2*m2p+koff_mt1t2m2p*mt1_t2_m2p;
  Ith(xdot,8) = p_mt1-kshed_eff*mt1*mt1-kon_mt1t2*mt1*t2+kon_mt1t2*ki_mt1t2*mt1_t2;
  Ith(xdot,9) = kon_mt1t2*mt1*t2-kon_mt1t2*ki_mt1t2*mt1_t2-kon_mt1t2m2p*mt1_t2*m2p+koff_mt1t2m2p*mt1_t2_m2p;
  Ith(xdot,10) = kon_mt1t2m2p*mt1_t2*m2p-koff_mt1t2m2p*mt1_t2_m2p-kact_eff_m2*mt1*mt1_t2_m2p;
  Ith(xdot,11) = p_t2-kon_m2t2*m2*t2+koff_m2t2*m2_t2-kon_mt1t2*mt1*t2+kon_mt1t2*ki_mt1t2*mt1_t2-d_t2*t2;

 return(0);

 }


void InitFdata(void * &f_data, const mxArray * mxData){

  Fdata* data = (Fdata*) f_data;

  data->dimx = DIMX;
  data->dimu = 0;
  data->dimp = 34;
  data->p = mxGetPr(mxGetField(mxData,0,"p"));

}

int UpdateFdata(realtype t, N_Vector x, void * &f_data, int Ns, N_Vector* xS)
{ return 0;}

int g(realtype t, N_Vector x, realtype *gout, void *f_data){ 
  return 0;}

int Jac(long int N, DenseMat J, realtype t,
              N_Vector x, N_Vector fx, void *jac_data,
       N_Vector tmp1, N_Vector tmp2, N_Vector tmp3)
{

  Fdata* data = (Fdata*) jac_data;
  
  double c1 = Ith(x,0);
  double c1dm2 = Ith(x,1);
  double c1dmt1 = Ith(x,2);
  double m2 = Ith(x,3);
  double m2_c1 = Ith(x,4);
  double m2_t2 = Ith(x,5);
  double m2_t2_star = Ith(x,6);
  double m2p = Ith(x,7);
  double mt1 = Ith(x,8);
  double mt1_t2 = Ith(x,9);
  double mt1_t2_m2p = Ith(x,10);
  double t2 = Ith(x,11);

  double d_m2 = data->p[12];
  double d_m2t2star = data->p[13];
  double d_t2 = data->p[14];
  double k_iso_m2t2 = data->p[15];
  double kact_eff_m2 = data->p[16];
  double kcat_m2c1 = data->p[17];
  double kcat_mt1c1 = data->p[18];
  double ki_mt1t2 = data->p[19];
  double kiso_m2t2 = data->p[20];
  double km_mt1c1 = data->p[21];
  double koff_m2c1 = data->p[22];
  double koff_m2t2 = data->p[23];
  double koff_mt1t2m2p = data->p[24];
  double kon_m2c1 = data->p[25];
  double kon_m2t2 = data->p[26];
  double kon_mt1t2 = data->p[27];
  double kon_mt1t2m2p = data->p[28];
  double kshed_eff = data->p[29];
  
  DENSE_ELEM(J,0,0)= -kon_m2c1*m2 - kcat_mt1c1*mt1/km_mt1c1;
  DENSE_ELEM(J,0,2)=              kcat_mt1c1*mt1/km_mt1c1;
  DENSE_ELEM(J,0,3)=                          -kon_m2c1*m2;
  DENSE_ELEM(J,0,4)=                           kon_m2c1*m2;

  DENSE_ELEM(J,3,0)=               -c1*kon_m2c1;
  DENSE_ELEM(J,3,3)= -d_m2 -c1*kon_m2c1 - kon_m2t2*t2;
  DENSE_ELEM(J,3,4)=                c1*kon_m2c1;
  DENSE_ELEM(J,3,5)=                kon_m2t2*t2;

  DENSE_ELEM(J,4,0)=              koff_m2c1;
  DENSE_ELEM(J,4,1)=              kcat_m2c1;
  DENSE_ELEM(J,4,3)=  kcat_m2c1 + koff_m2c1;
  DENSE_ELEM(J,4,4)= -kcat_m2c1 - koff_m2c1;
  
  DENSE_ELEM(J,5,3)=              koff_m2t2;
  DENSE_ELEM(J,5,5)= -kiso_m2t2 - koff_m2t2;
  DENSE_ELEM(J,5,6)=              kiso_m2t2;
  DENSE_ELEM(J,5,11)=              koff_m2t2;
  
  DENSE_ELEM(J,6,5)=               k_iso_m2t2;
  DENSE_ELEM(J,6,6)= -d_m2t2star - k_iso_m2t2;

  DENSE_ELEM(J,7,7)= -kon_mt1t2m2p*mt1_t2;
  DENSE_ELEM(J,7,9)= -kon_mt1t2m2p*mt1_t2;
  DENSE_ELEM(J,7,10)=  kon_mt1t2m2p*mt1_t2;

  DENSE_ELEM(J,8,0)=         -c1*kcat_mt1c1/km_mt1c1;
  DENSE_ELEM(J,8,2)=          c1*kcat_mt1c1/km_mt1c1;
  DENSE_ELEM(J,8,3)=          kact_eff_m2*mt1_t2_m2p;
  DENSE_ELEM(J,8,8)= -kon_mt1t2*t2 - 2*kshed_eff*mt1;
  DENSE_ELEM(J,8,9)=                    kon_mt1t2*t2;
  DENSE_ELEM(J,8,10)=         -kact_eff_m2*mt1_t2_m2p;
  DENSE_ELEM(J,8,11)=                   -kon_mt1t2*t2;

  DENSE_ELEM(J,9,7)=                      -kon_mt1t2m2p*m2p;
  DENSE_ELEM(J,9,8)=                     ki_mt1t2*kon_mt1t2;
  DENSE_ELEM(J,9,9)= -ki_mt1t2*kon_mt1t2 - kon_mt1t2m2p*m2p;
  DENSE_ELEM(J,9,10)=                       kon_mt1t2m2p*m2p;
  DENSE_ELEM(J,9,11)=                     ki_mt1t2*kon_mt1t2;

  DENSE_ELEM(J,10,3)=                  kact_eff_m2*mt1;
  DENSE_ELEM(J,10,7)=                    koff_mt1t2m2p;
  DENSE_ELEM(J,10,9)=                    koff_mt1t2m2p;
  DENSE_ELEM(J,10,10)= -kact_eff_m2*mt1 - koff_mt1t2m2p;


  DENSE_ELEM(J,11,3)=                 -kon_m2t2*m2;
  DENSE_ELEM(J,11,5)=                  kon_m2t2*m2;
  DENSE_ELEM(J,11,8)=               -kon_mt1t2*mt1;
  DENSE_ELEM(J,11,9)=                kon_mt1t2*mt1;
  DENSE_ELEM(J,11,11)=  -d_t2-kon_m2t2*m2 - kon_mt1t2*mt1;

 return 0;
}
