%
%  This script produces Figure 10 in
%
%  "Robustness Analysis and Behavior Discrimination in Enzymatic Reaction Networks"
%   by Donzé, A., Fanchon, E., Gattepaille, L. M., Maler O., Tracqui, P.
%

function plot_Fig9_oscill_map

load paper_param_sets;
CreateSystem;

Pf = ComputeTraj(Sys, Pcycle_ex, [0:100:100*3600])
Pf.rescale = 1e9;
SplotTraj(Pf, {'m2','m2p','t2'} )
view(45,32)

xlabel('Concentrations of MMP2 (nM)', 'FontSize', 16);
ylabel('Concentrations of pro-MMP2 (nM)', 'FontSize', 16);
zlabel('Concentrations of TIMP2 (nM)', 'FontSize', 16);
set(gca,'FontWeight','normal', 'FontSize',16);

% Exporting figure
%print(gcf,'-depsc','figures/cycle.eps');
