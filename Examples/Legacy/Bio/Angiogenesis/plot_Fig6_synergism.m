%
%  This script produces the figures illustrating synergism between m2 and
%  mt1 degradation, Figure 6 in
%
%  "Robustness Analysis and Behavior Discrimination in Enzymatic Reaction Networks"
%   by Donzé, A., Fanchon, E., Gattepaille, L. M., Maler O., Tracqui, P.
%

function P= plot_Fig6_synergism

% load parameter sets and the model
load paper_param_sets;
CreateSystem
STL_ReadFile('angio_properties.stl');
global phic phicmp;

T = 12*3600;
tspan = 0:1000:T+1000;
P = ComputeTraj(Sys, Psyn,tspan);
P = SEvalProp(Sys, P, phic, T );
P = SEvalProp(Sys, P, phicmp, T );

S = DiscrimPropValues(P);

opt.rescale= 1e9
figure;
SplotPts(S, [], [] , opt);

dx = 10;
dy = 10;

h1 = text(6*dx , dy, 'A');
h2 = text(6*dx , 13*dy, 'B');
h3 = text(16*dx, 13*dy, 'C');
h4 = text(16*dx, 3*dy, 'D');

set(h1, 'FontSize',20)
set(h2, 'FontSize',20)
set(h3, 'FontSize',20)
set(h4, 'FontSize',20)

ylabel('Initial concentrations of TIMP2 (nM)', 'FontSize', 16);
xlabel('Initial concentrations of MT1-MMP (nM)', 'FontSize', 16);

%set(gca,'FontWeight','normal', 'FontSize',16);
%print(gcf,'-depsc','figures/synergism.eps');

