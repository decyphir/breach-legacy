%
%  This script produces Figures 3 A-C in 
%
%  "Robustness Analysis and Behavior Discrimination in Enzymatic Reaction Networks"
%  by Donzé, A., Fanchon, E., Gattepaille, L. M., Maler O., Tracqui, P.
%

function plot_Fig2
  
cpte_fixed_time=1;
cpte_time = 1;
cpte_steady = 1;

% Initialize, sets and trajectories and properties  

% Compute trajectories from the set of parameters Pfig2Dr on a sufficiently
% long time interval (1000 hours) with time steps of 20 minutes
   
load paper_param_sets;
CreateSystem;
dt = 20*60;
T  = 1000*3600;
Pf = ComputeTraj(Sys, Pfig2Dr, 0:dt:T);
  
ind_t2 = FindParam(Sys,'t2');
t2 = Pf.pts(ind_t2,:); % array containing the values of t2 initial
                         % concentrations   
% 
% Compute the ratio of activated m2 for fixed time 
%
 
if (cpte_fixed_time)

  % at time t = 12 hours (original plot of Popel and Kariaganis)

  tf = 12*3600;
  Pf = SEvalProp(Sys,Pf,phi_rm2,tf);
  val12 = SplotProp(Pf, phi_rm2);
  
  % at time t = 36 hours

  tf = 36*3600;
  Pf = SEvalProp(Sys,Pf,phi_rm2,tf);
  val36 = SplotProp(Pf, phi_rm2);
  
  % at time t = 100 hours

  tf = 100*3600;
  Pf = SEvalProp(Sys,Pf,phi_rm2,tf);
  val100 = SplotProp(Pf, phi_rm2);

  % at time t = 1000 hours
  
  hold on
  tf = 1000*3600;
  Pf = SEvalProp(Sys,Pf,phi_rm2,tf);
  val1000 = SplotProp(Pf, phi_rm2);
  
end % fixed time

% Compute the time when the variation of the activated m2 passes under a
% given threshold. This variation is estimated as the quantity of activated
% m2 in one hour, as a proportion of (i.e., divided by) the initial quantity
% of m2p.
  
if (cpte_time)
  
  phi2dtime = set_params(phi2dtime,'T', 1000);
  phi2dtime = set_params(phi2dtime,'max_true_value', 2000);
  
  %  one percent of m2p per hour
  phi2dtime = set_params(phi2dtime,'threshold', 0.01); 
  Pf = SEvalProp(Sys,Pf,phi2dtime,0);
  val = SplotProp(Pf, phi2dtime);
  valT01 = 1000-val;

  % 0.1 percent of m2p per hour
  
  phi2dtime = set_params(phi2dtime,'threshold', 0.001);
  Pf = SEvalProp(Sys,Pf,phi2dtime,0);
  val = SplotProp(Pf, phi2dtime);
  valT001 = 1000-val;
  
  % 0.01 percent of m2p per hour
  phi2dtime = set_params(phi2dtime,'threshold', 0.0001);
  [Pf val]= SEvalProp(Sys,Pf,phi2dtime,0);
  val = SplotProp(Pf, phi2dtime);
  valT0001 = 1000-val;
      
end

%
% Compute ratio of activated m2 after its variation passed under a given threshold 
%

if (cpte_steady)
  % one percent of m2p per hour
  phi2d_ = set_params(phi2d_,'threshold', 0.01);
  Pf = SEvalProp(Sys,Pf,phi2d_,0);
  val = SplotProp(Pf, phi2d_);
  val_steady01 = 100-val;
  
  % 0.1 percent of m2p per hour
  phi2d_ = set_params(phi2d_,'threshold', 0.001);
  Pf = SEvalProp(Sys,Pf,phi2d_,0);
  val = SplotProp(Pf, phi2d_);
  val_steady001 = 100-val;
  
   % 0.01 percent of m2p per hour
  phi2d_ = set_params(phi2d_,'threshold', 0.0001);
  Pf = SEvalProp(Sys,Pf,phi2d_,0);
  val = SplotProp(Pf, phi2d_);
  val_steady0001 = 100-val;

end

%
%  plot figures
%

close;
plot_figs = 1 ;

if (plot_figs)

  % TIMP2 concentrations expressed in nM

  T2 = t2*1e9;
  l = 1.5;

  % fixed times 
  
  figure;
  hold on;
  plot(T2, val12)
  plot(T2, val36,'-+')
  plot(T2, val100,'-*')
  plot(T2, val1000,'-s')
  legend('12 hours','36 hours','100 hours', '1000 hours');
  xlabel('Initial concentrations of TIMP2 (nM)', 'FontSize', 16);
  ylabel('% of activated MMP2', 'FontSize', 16);
  title('Activated MMP2 after a fixed time', 'FontSize', 16);
  set(gca,'FontWeight','normal', 'FontSize',16);
  set(gca,'Xtick',0:20:200)
  grid on;

  %print(gcf,'-depsc','figures/fig2D_fixed_times.eps');

  figure;
  hold on;

  plot(T2, val_steady01,'-+')
  plot(T2, val_steady001,'-*')
  plot(T2, val_steady0001,'-s')
  
  legend('1% per hour','0.1% per hour','0.01% per hour')
  
  xlabel('Initial concentrations of TIMP2', 'FontSize', 16);
  ylabel('% of activated MMP2', 'FontSize', 16);
  title('Activated MMP2 after the variation of MMP2 has become slow', 'FontSize', 16);
  set(gca, 'FontSize',16);
  set(gca,'Xtick',0:20:200);
  grid on;
%  print(gcf,'-depsc','figures/fig2D_steady.eps');

  figure;
  hold on;
  
  plot(T2, valT01,'-+')
  plot(T2, valT001,'-*')
  plot(T2, valT0001,'-s')
  
  legend('1% per hour','0.1% per hour','0.01% per hour')
  
  xlabel('Initial concentration of TIMP2 (nM)', 'FontSize', 16);
  ylabel('Time (hours)', 'FontSize', 16);
  title('Time after when the variation of MMP2 has become slow', 'FontSize', 16);
  set(gca, 'FontSize',16);
  set(gca,'Xtick',0:20:200);
  axis([0 200 0 500]);
  grid on;
 % print(gcf,'-depsc','figures/fig2D_steady_times.eps');
end
