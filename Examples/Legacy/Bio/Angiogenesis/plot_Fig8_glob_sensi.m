%
%  This script produces Figure 8 in
%
%  "Robustness Analysis and Behavior Discrimination in Enzymatic Reaction Networks"
%   by Donzé, A., Fanchon, E., Gattepaille, L. M., Maler O., Tracqui, P.
%

%function plot_Fig8
  
load paper_param_sets;
CreateSystem;

Nbtraj = 1000;
Tmax = 12 ;
dt = 3600; 
P0 = P0_wo_m2t2;

Pr = HaltonRefine(P0,  Nbtraj );
%Pr = ComputeTraj(Sys, Pr, [0 1:dt:(Tmax+1)*3600]);  

opts.open_dialog =0;
opts.stat_type = 'aver_end';
opts.cutoff=  0;  
  
%%% Classic sensitivity plots

iX_interesting = { 'c1', 'm2', 'mt1', 't2'};
iX0_interesting = { 'c1', 'm2p', 'mt1', 't2'};
   
% kinetic constants
iP_kin_all = { 'k_iso_m2t2'  'kact_eff_m2'    'kcat_m2c1' 'kcat_mt1c1' ...
               'ki_mt1t2'    'kiso_m2t2'      'km_mt1c1'  'koff_m2c1' ...
               'koff_m2t2'   'koff_mt1t2m2p'  'kon_m2c1'  'kon_m2t2'  ... 
               'kon_mt1t2'   'kon_mt1t2m2p'   'kshed_eff' };
  
iP_kin = { 'kact_eff_m2'    'kcat_m2c1' 'kcat_mt1c1' ...
           'ki_mt1t2'    'km_mt1c1'  'koff_m2c1' ...
           'koff_mt1t2m2p'  'kon_m2c1'  ... 
           'kon_mt1t2'   'kon_mt1t2m2p'    };
  
% interesting predicates 
%  phi_m2_act = STL_Formula('phi_m2_act', 'm2[t] >= 0');

phi_c1_deg  = STL_Formula('phi_c1_deg', 'c1[t]/c1 < .1'); 
phi_c1_rat  = STL_Formula('phi_c1_rat', 'c1dm2[t] > c1dmt1[t]'); 
    
Tmax = 12 % in hours

%  opts.args.iX = iX_interesting;

opts.args.iX = {}; 
opts.args.iP = [iX0_interesting];
opts.props = { phi_c1_deg, phi_c1_rat};   
opts.args.tspan = [ 0:1000:(Tmax)*3600 ];
opts.taus = opts.args.tspan(end);
  
%[optsk1_100 Mend_k1_100] = SplotSensiBar(Sys, Pr,[], opts);
%set(gca,'FontWeight','normal', 'FontSize',16);    
%view(-60,30)

%print(gcf,'-depsc', 'figures/histo_sensi_var.eps')
%savefig('figures/histo_sensi_var.fig')
  
opts.args.iP = [iP_kin];
opts.props = {phi_c1_deg, phi_c1_rat};   
opts.args.tspan = [ 0:1000:(Tmax)*3600 ];
opts.taus = opts.args.tspan(end);

%[optsk1 Mend_k1] = SplotSensiBar(Sys, Pr, 1, opts);

subplot(2,1,1)
bar(Mend_k1(2,:));
set(gca,'FontWeight','normal', 'FontSize',16);    
set(gca, 'XTickLabel',  xtick_labels );
title('Sensitivity of relative proteolysis by M2 wrt proteolysis by MT1')

grid on;
%set(gcf, 'PaperPosition', [0.25 2.5 8 10]);

subplot(2,1,2)
bar(Mend_k1(1,:));
set(gca,'FontWeight','normal', 'FontSize',16);    

xtick_labels= {'C1(0)', 'M2P(0)', 'MT1(0)', 'T2(0)'};
title('Sensitivity of total proteolysis by both M2 and MT1')
set(gca, 'XTickLabel',  xtick_labels );
grid on;

print(gcf,'-depsc2', 'histo_flat_glob.eps'   );


%set(gca,'FontWeight','normal', 'FontSize',16);    
%savefig('figures/histo_sensi_kin.fig');   

