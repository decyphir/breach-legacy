%
%  This script produces Figure 11 in
%
%  Note: script doesn't work anymore  
%
%  "Robustness Analysis and Behavior Discrimination in Enzymatic Reaction Networks"
%   by Donz�, A., Fanchon, E., Gattepaille, L. M., Maler O., Tracqui, P.
%

function plot_Fig11_global_cyc

load paper_param_sets;
CreateSystem;
rescale = 1e9;

tspan = 0:500:100*3600;

%Pbnd_m2p_glob0.pts =Pbnd_m2p_glob0.pts*rescale; 
%Pbnd_m2p_glob0.epsi =Pbnd_m2p_glob0.epsi*rescale; 
  
% figure A

Pbnd_m2p_glob0 = ComputeTraj(Sys, Pcyc_m2p, tspan)


figure
S = DiscrimPropValues(Pbnd_m2p_glob0);
SplotPts(S);
hold on;  
SplotBoxPts(Pbnd_m2p_glob0,{'p_mt1', 'p_t2'},1,'+b','r',1);
axis([1 3 1 6]);
    
xlabel('Production rate of  MT1-MMP (nM x s^{-1})', 'FontSize', 16);
ylabel('Production rate of  TIMP2 (nM x s^{-1})', 'FontSize', 16);
set(gca,'FontWeight','normal','FontSize',16);

% 
% print(gcf,'-depsc','figures/oscil_map_glob0.eps');


Pcyc_globrr = ComputeTraj(Sys, Pcyc_globrr, tspan )

% figure B
plot_cyc_qmc_sets(Pcyc_globrr,1);
  


