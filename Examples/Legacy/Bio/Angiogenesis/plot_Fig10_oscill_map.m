%
%  This script produces Figure 10 in
%
%  "Robustness Analysis and Behavior Discrimination in Enzymatic Reaction Networks"
%   by Donz�, A., Fanchon, E., Gattepaille, L. M., Maler O., Tracqui, P.
%

%function plot_Fig10_oscill_map

load paper_param_sets;
CreateSystem;
 
figure
tspan = 0:500:100*3600;

P = ComputeTraj(Sys, Pcyc_bnd, tspan);
P = SEvalProp(Sys,P, phi_cyc_t2, 0);
S = DiscrimPropValues(P);

opt.rescale = 1e9;
SplotPts(S,[], [], opt);
  
dx = .2;
dy = .2;

h1 = text(.6 , 3, 'Dv');
h2 = text(2,.5 , 'Dv');
h3 = text(1.55 ,3.3 , 'Osc');
h4 = text(1.55 ,3 , '(T2)');
h5 = text(2.6, 3, 'Cv');

set(h1, 'FontSize',16)
set(h2, 'FontSize',16)
set(h3, 'FontSize',16)
set(h4, 'FontSize',16)
set(h5, 'FontSize',16)
  
xlabel('Production rate of  MT1-MMP (nM x s^{-1})', 'FontSize', 16);
ylabel('Production rate of  TIMP2 (nM x s^{-1})', 'FontSize', 16);
set(gca,'FontWeight','normal', 'FontSize',16);
