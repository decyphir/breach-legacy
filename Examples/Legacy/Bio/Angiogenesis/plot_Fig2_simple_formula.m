%
%  This script produces Figures 2 in 
%
%  "Robustness Analysis and Behavior Discrimination in Enzymatic Reaction Networks"
%  by Donzé, A., Fanchon, E., Gattepaille, L. M., Maler O., Tracqui, P.
%
function plot_Fig2

load paper_param_sets;
CreateSystem
% compute trajectories

Tf = 10; % final time in hours
Pf = ComputeTraj(Sys, Pdamp_oscil, [0:100:Tf*3600]);

% define formulas

mu = STL_Formula('mu', 't2[t] > 1e-7');
ev_mu = STL_Formula('ev_mu', 'ev (t2[t] > 1e-7)' ); 
figure;
plot_prop(Sys,Pf, 't2', {mu, ev_mu});
grid on;

function plot_prop(Sys, P, ix ,prop)

%
%  Plot robustness degree w.r.t. time for properties in prop.
% 

  % Constants
  
  rescale = 1e9;
  time_mult = 1/3600; 
 
  if (~isnumeric(ix))
      ix = FindParam(P,ix);
  end  
  
  traj = P.traj(1);  
  N = numel(prop);   
  phi_tspan = traj.time;
  
  subplot(N+1,1,1);
  plot(traj.time*time_mult, traj.X(ix,:)*rescale);
  grid on;
  
  for k = 2:N+1

    subplot(N+1,1,k);
    grid on;
    propi = prop{k-1};    
    
    if (ischar(propi))
      phi = STL_Formula('phi', propi);
    else
      phi = propi;
    end
    
    phi_val = STL_Eval(Sys,phi,P,traj,phi_tspan)*rescale;
    hold on;
    plot([phi_tspan(1) phi_tspan(end)]*time_mult, [0 0],'-k');
    plot(phi_tspan*time_mult, sign(phi_val)*max(abs(phi_val))/2,'-r');
    plot(phi_tspan*time_mult, phi_val);
    
  end
