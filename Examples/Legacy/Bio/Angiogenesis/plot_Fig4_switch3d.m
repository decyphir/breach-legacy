%
%  This script produces Figures 4 in 
%
%  "Robustness Analysis and Behavior Discrimination in Enzymatic Reaction Networks"
%  by Donzé, A., Fanchon, E., Gattepaille, L. M., Maler O., Tracqui, P.
%

function plot_Fig4_switch3d

load paper_param_sets;
CreateSystem;

h12 = figure;
opt.rescale = 1e9; % units in nano Mols


T12 = 12*3600; 
T100 = 100*3600; 
tspan = 0:3600:T100+1;

Pfig4 = ComputeTraj(Sys, Pfig4, tspan);

Pfig4A = SEvalProp(Sys, Pfig4, phi_rm2, T12);

% eval and plot the ratio for 12 hours 
SplotProp(Pfig4A, phi_rm2,opt);
view(30,52);

xlabel('Initial concentrations of TIMP2 (nM)', 'FontSize', 12);
ylabel('Initial concentrations of MT1-MMP (nM)', 'FontSize', 12);
title('% pro-MMP2 activation after 12 hours', 'FontSize', 12);
set(gca,'FontWeight','normal', 'FontSize',16);

%% Exporting figure
%print(gcf,'-depsc','figures/Fig6new12.eps');

% eval and plot the ratio for 100 hours 

Pfig4B = SEvalProp(Sys, Pfig4, phi_rm2, T100);
hstd = figure;
SplotProp(Pfig4B, phi_rm2,opt);
view(30,52);

xlabel('Initial concentrations of TIMP2 (nM)', 'FontSize', 12);
ylabel('Initial concentrations of MT1-MMP (nM)', 'FontSize', 12);
title('% pro-MMP2 activation at quasi-steady state', 'FontSize', 12);
set(gca,'FontWeight','normal', 'FontSize',16);

%% Exporting figure

%print(gcf,'-depsc','figures/Fig6newstd.eps');


