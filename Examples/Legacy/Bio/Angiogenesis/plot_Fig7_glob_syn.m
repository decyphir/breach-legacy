%
%  This script produces Figure 7 in
%
%  "Robustness Analysis and Behavior Discrimination in Enzymatic Reaction Networks"
%   by Donzé, A., Fanchon, E., Gattepaille, L. M., Maler O., Tracqui, P.
%

function plot_Fig7_global_syn
  
load paper_param_sets;
CreateSystem

Nbtraj = 1000;
Tmax = 12 ;
dt = 3600; 
P0 = P0_wo_m2t2;

Pr = HaltonRefine(P0,  Nbtraj );
Pr = ComputeTraj(Sys, Pr, [0 1:dt:(Tmax+1)*3600]);
  
%[P valcmp] = SEvalProp(Sys,Pr,  phicd,  1000*3600);
%% Competition between M2 and Mt1 without M2T2

phi1 = STL_Formula( 'phi1' , ' ev ((c1dm2[t] > c1dmt1[t]) and (c1[t]/c1 < .1  ))' );
P1 = SEvalProp(Sys, Pr, phi1, dt);  
%Ps1 = DiscrimPropValues(P1);
plot_qmc_sets(P1);

