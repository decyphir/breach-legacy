InitBreach;

% System Definition

Sys.DimX = 12;
Sys.DimU =0; 
Sys.DimP = 34; 
Sys.ParamList = {'c1','c1dm2','c1dmt1','m2','m2_c1','m2_t2','m2_t2_star','m2p','mt1','mt1_t2','mt1_t2_m2p','t2', ....
         'd_m2','d_m2t2star','d_t2','k_iso_m2t2','kact_eff_m2','kcat_m2c1','kcat_mt1c1','ki_mt1t2','kiso_m2t2',....
         'km_mt1c1','koff_m2c1','koff_m2t2','koff_mt1t2m2p','kon_m2c1','kon_m2t2','kon_mt1t2','kon_mt1t2m2p','kshed_eff','p_c1',....
         'p_m2p','p_mt1','p_t2'};
Sys.x0 = [0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. ]';

Sys.p = [0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.  ....
         1e-2 1e-2 1e-2 2e-8 3.62e3 4.5e-3 1.97e-3 4.9e-9 33 ....
         2.9e-6 2.1e-3 6.3 4.7e-3 2.6e3 5.9e6 3.54e6 0.14e6 2.8e3 5e-10 ....
         8e-10 10e-10 16e-10]';

Sys.tspan =  0:500:100*3600;

dir_breach = which('Breach');
dir_breach = dir_breach(1:end-9);

Sys.Dir = [ BreachGlobOpt.breach_dir filesep 'Examples' filesep 'Angiogenesis' ] ;

options=[];
options = CVodeSetOptions(options,'RelTol',1.e-6,'AbsTol',1e-15,'MaxStep',1000,'MaxNumSteps',100000,'InitialStep', ...
                          0.01);
Sys.CVodesOptions = options;

% options for sensitivity computation 

ParamScales = Sys.p;
ParamScales(1:Sys.DimX)= 1e-9;

FSAoptions = [];
FSAoptions = CVodeSetFSAOptions('SensErrControl', 'on',...
                                'ParamField', 'p',...                                
                                'ParamScales',ParamScales );
Sys.CVodesSensiOptions.method = 'Simultaneous';
Sys.CVodesSensiOptions.FSAoptions = FSAoptions;

