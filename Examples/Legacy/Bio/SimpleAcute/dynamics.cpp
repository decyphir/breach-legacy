#include "dynamics.h"

int f(realtype t, N_Vector x, N_Vector xdot, void *f_data) {

  Fdata* data = (Fdata*) f_data;

  double ca = Ith(x,0);
  double d = Ith(x,1);
  double na = Ith(x,2);
  double p = Ith(x,3);

  double cinf = data->p[4];
  double kcn = data->p[5];
  double kcnd = data->p[6];
  double kdn = data->p[7];
  double kmp = data->p[8];
  double knd = data->p[9];
  double knn = data->p[10];
  double knp = data->p[11];
  double kpg = data->p[12];
  double kpm = data->p[13];
  double kpn = data->p[14];
  double muc = data->p[15];
  double mud = data->p[16];
  double mum = data->p[17];
  double mun = data->p[18];
  double munr = data->p[19];
  double pinf = data->p[20];
  double qna = data->p[21];
  double sc = data->p[22];
  double sm = data->p[23];
  double snr = data->p[24];
  double xdn = data->p[25];

  double R = F((knd*d+knp*p+knn*na),ca/cinf);

  Ith(xdot,0) = -muc*ca+sc+kcn*F(na+kcnd*d,ca/cinf)/(1+F(na+kcnd*d,ca/cinf));
  Ith(xdot,1) = kdn*Fs(F(na,ca/cinf),xdn,qna)-mud*d ;
  Ith(xdot,2) = snr*R/(munr+R)-mun*na;
  Ith(xdot,3) = kpg*p*(1-p/pinf)-kpm*sm*p/(mum+kmp*p)-kpn*F(na,ca/cinf)*p;

 return(0);

 }


void InitFdata(void * &f_data, const mxArray * mxData){

  Fdata* data = (Fdata*) f_data;

  data->dimx = DIMX;
  data->dimu = 0;
  data->dimp = 26;
  data->p = mxGetPr(mxGetField(mxData,0,"p"));

}

int UpdateFdata(realtype t, N_Vector x, void * &f_data, int Ns, N_Vector* xS)
{ return 0;}

int g(realtype _t, N_Vector _y, realtype *_gout, void *_g_data) {
  return 0;
}

int Jac(long int N, DenseMat J, realtype t,
               N_Vector x, N_Vector fx, void *jac_data,
	N_Vector tmp1, N_Vector tmp2, N_Vector tmp3){

  return 0;
}

