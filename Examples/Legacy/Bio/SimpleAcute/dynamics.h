#ifndef DYNAMICS_H
#define DYNAMICS_H

#include "breach.h"

class Fdata: public FdataCommon {
 public:
  double *p;
};

int  f(realtype t, N_Vector y, N_Vector ydot, void *f_data);
void InitFdata(void * &f_data, const mxArray * mxData);
int  UpdateFdata(realtype t, N_Vector y, void * &f_data, int Ns, N_Vector* yS);
int g(realtype _t, N_Vector _y, realtype *_gout, void *_g_data);

inline double Fs(double x, double v, double hill) {
  double res = pow(x,hill)/(pow(v,hill)+pow(x,hill));
  return res;
}

inline double F(double x, double y) {
  double res = x/(1+y*y);
  return res;
}

#endif
