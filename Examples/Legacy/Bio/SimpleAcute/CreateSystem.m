InitBreach;

% System Definition

Sys.DimX = 4;
Sys.DimU =0; 
Sys.DimP = 26; 
Sys.x0 = [0.125 0.0 0. 1.]';
Sys.ParamList = {'ca','d','na','p', ....
         'cinf','kcn','kcnd','kdn','kmp','knd','knn','knp','kpg',....
         'kpm','kpn','muc','mud','mum','mun','munr','pinf','qna','sc',....
         'sm','snr','xdn'};

Sys.p = [0.125 0.0 0.0 1.0  ....
         0.28 .04 48 .35 .01 0.02 .01 0.1 .3 ....
         0.6 1.8 .1 .02 .002 .05 .12 20 6 0.0125 ....
         .005 .08 .06]';
Sys.Dir = [ BreachGlobOpt.breach_dir filesep 'Examples' filesep 'SimpleAcute' ]  ;

options = [];
options = CVodeSetOptions(options,'RelTol',1.e-8,'AbsTol',1e-6, ...
                          'MaxNumSteps',100000);
Sys.CVodesOptions = options

% options for sensitivity computation 

pscales = ones(26,1);
FSAoptions = [];
FSAoptions = CVodeSetFSAOptions('SensErrControl', 'on',...
                                'ParamField', 'p',...
                                'ParamScales',pscales );
Sys.CVodesSensiOptions.method = 'Simultaneous';
Sys.CVodesSensiOptions.FSAoptions = FSAoptions;
