function [P,val,nb_valid,nb_suppr_CI,nb_suppr_traj,nb_utilise,nb_plantage] = check_param_1(Sys,N)
%CHECK_PARAM_1 function computing the number of parameter sets verifying
% the constraint okbehavior. The parameter sets are sampled according
% Chifman method: the input (ie: hep) is generated using a standart
% uniform distribution with lower endpoint 0 and upper endpoint 1, the
% other parameters are generated using an exponential distribution with
% mean 1000. The function prints the number of valid parameters sets.
%
% Synopsis: [P,val,nb_valid,nb_suppr_CI,nb_suppr_traj,nb_utilise] = check_param_1(Sys,N)
%
% Inputs:
%  - Sys the system
%  - N   the number of parameter to generate
%
% Outputs:
%  - P   the parameter set containing the trajectories and the evaluation
%        of okbehavior
%  - val the truth value of okbehavior
%%

[~,okbehavior] = define_constraints(); % define property

%% tune Sys
ParamScales = Sys.p;
ParamScales(1:Sys.DimX) = 1;
FSAoptions = CVodeSetFSAOptions('SensErrControl', 'off',...
                                'ParamField', 'p',...                                
                                'ParamScales',ParamScales );
Sys.CVodesSensiOptions.method = 'Simultaneous';
Sys.CVodesSensiOptions.FSAoptions = FSAoptions;


%% Create param set with N set of values
P = CreateParamSet(Sys,...
        {'a1','a2','a3','a4','a5','a6','k15','k52','k53','k54','g2','g3','g4','g5','gh','hep'});
P = Refine(P,[N,ones(1,numel(P.dim)-1)]);


%% define random value for param sets
valeursParam = exprnd(1000,15,N);
%TODO : rand uses a "standart uniform distribution", Chifman uses a
%"continuous uniform distribution". Are they the same ?
valeursEntree = rand(1,N);

idxEntree = FindParam(P,'hep');
P.pts(setdiff(P.DimX+1:P.DimP,idxEntree),:) = valeursParam; % we define all the params except the input and IC
P.pts(idxEntree,:) = valeursEntree;


%% compute initial conditions and remove all negative one
P = find_init_conditions_6(P);
isGood = P.pts(1,:)>=0; % check if the IC is negative or imaginary (if one is negative, then they are all equal to -10)
P.pts = P.pts(:,isGood);
P.epsi = P.epsi(:,isGood);
fprintf(' We keep %d over %d param sets (others have negative or complexe IC).\n',size(P.pts,2),N);
nb_suppr_CI = N - size(P.pts,2);


%% we compute the trajectories
try 
    P = ComputeTraj(Sys,P,0:600:48*3600);
catch err
    fprintf([err.message,'\n'])
    val = NaN;
    nb_valid = 0;
    nb_plantage = size(P.pts,2);
    nb_suppr_traj = 0;
    nb_utilise = 0;
    return ;
end


%% we remove trajectories blocked by the security
phi_bloque = STL_Formula('phi_bloque',...
        'ev (alw (abs(ddt{x3}[t])+abs(ddt{x5}[t]) < 1.0e-16 ))');
[~,val_bloque] = SEvalProp(Sys,P,phi_bloque,0);
Pgood = Sselect(P, find(val_bloque<=0));
sprintf(' We keep %d among %d param sets (others are blocked by the security).\n',size(Pgood.pts,2),size(P.pts,2))
nb_suppr_traj = size(P.pts,2)-size(Pgood.pts,2);
nb_utilise = size(Pgood.pts,2);
P = Pgood;


%% we compute the truth value of okbehavior
[P,val] = SEvalProp(Sys,P,okbehavior,0);
nb_valid = numel(find(val>0));

fprintf('There are %d valid parameter sets.\n',nb_valid);

nb_plantage = 0;


end

