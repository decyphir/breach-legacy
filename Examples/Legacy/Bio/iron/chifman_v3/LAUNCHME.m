%
% This script need 11 GB of memory. Using a Core2 Duo, 3CHz, a run takes
% around 36 hours. (15 fev 2013: now, it will be even longer as we added a
% fourth sampling).
%
%
% The aim of this script (which is in directory v3) is to search for
% parameter sets verifying the expected behavior. The modeled system is
% the one described in Chifman paper. We try here four kind of sampling:
%  - the first one pick randomly parameter with an exponential distribution
% centered in 1000 (as in Chifman article) ;
%  - the second sampling consider a uniform sampling between 0 and 10000
% (as in Chifman article) ;
%  - the third chose the parameter according a uniform distribution in the
% interval obtained using data from the litterature ;
%  - the fourth one follow a uniform distribution on a logarithmic scale in
% the interval obtained using data from the litterature.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script assumes that definitionSystem is already ran.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

N1 = 10000;
N2 = 100;
% N total = N1*N2 = 1'000'000
CreateSystem;
fid = fopen('output.txt','w');

tic
val_min = inf;
val_max = -inf;
nb_valid = 0;
nb_suppr_CI = 0;
nb_suppr_traj = 0;
nb_utilise = 0;
nb_plantage = 0;
for ii=1:N2
    fprintf(' -- step 1. %d/%d\n',ii,N2);
    [~,val,valid,suppr_CI,suppr_traj,utilise,plantage] = check_param_1(Sys,N1);
    val = sort(val);
    if val(1)<val_min
        val_min = val(1);
    end
    if val(end)>val_max
        val_max = val(end);
    end
    nb_valid = nb_valid+valid;
    nb_suppr_CI = nb_suppr_CI+suppr_CI;
    nb_suppr_traj = nb_suppr_traj+suppr_traj;
    nb_utilise = nb_utilise+utilise;
    nb_plantage = nb_plantage + plantage;
end
toc
sortie = sprintf(['\n\nSampling of parameter sets according to an exponential law centered in 1000:\n'...
        'number of parameter set not used because of the IC: %d\n'...
        'number of parameter set not used because of a unreliable value during the resolution: %d\n'...
        'number of parameter set not used because of a failure of the solver: %d\n',...
        'total number of parameter set used: %d\n',...
        'number of valid parameter set: %d\n',...
        'truth value from %e to %e\n\n'],...
        nb_suppr_CI,nb_suppr_traj,nb_plantage,nb_utilise,nb_valid,val_min,val_max);
fprintf(sortie);
fprintf(fid, sortie);
fprintf(fid,'\n');

% When using a test for the IC which keep only parameter sets such that the
% real part of the IC are positive and a complex part lower than 2.2e-16,
% we obtain:
%
% - we keep 545 870 initial conditions (the 454 130 others are not valid)
% - we keep 137 375 trajectories (the 408 495 others become negative)
% - there are no trajectory versifying okbehavior.
%
% The computation takes 8390 seconds.
%
% %%%%%%%%%%%%%%%
%
% When using a test that keep IC if the real part is positive, even if the
% imaginary part is not negligeable, we got the following results (with
% 1'000'002 inital parameter sets):
% - 0 parameter sets are deleted because of the IC
% - 749'638 parameter sets are removed because the trajectory become
% negative
% - among the 250'364 parameter sets kept, no one verify okbehavior
% - computation time : 17178 seconds
%
% %%%%%%%%%%%%%%%
%
% 11 fev 2013 :
% When using a test that keep IC if the real part is positive, even if the
% imaginary part is not negligeable, we got the following results (with
% 1'000'002 inital parameter sets):
% - 0 parameter sets are deleted because of the IC (around 886'000 had an
% imaginary part not null)
% - 748'962 parameter sets are not kept because the trajectory becomes
% negative
% - O parameter sets are not used because they make the solver failed
% - among the 251'038 parameter set considered, no one verify okbehavior
% - computation time : 17993 seconds
%

tic
val_min = inf;
val_max = -inf;
nb_valid = 0;
nb_suppr_CI = 0;
nb_suppr_traj = 0;
nb_utilise = 0;
nb_plantage = 0;
for ii=1:N2
    fprintf(' -- step 2. %d/%d\n',ii,N2);
    [~,val,valid,suppr_CI,suppr_traj,utilise,plantage] = check_param_2(Sys,N1);
    val = sort(val);
    if val(1)<val_min
        val_min = val(1);
    end
    if val(end)>val_max
        val_max = val(end);
    end
    nb_valid = nb_valid+valid;
    nb_suppr_CI = nb_suppr_CI+suppr_CI;
    nb_suppr_traj = nb_suppr_traj+suppr_traj;
    nb_utilise = nb_utilise+utilise;
    nb_plantage = nb_plantage + plantage;
end
toc
sortie = sprintf(['\n\n Parameter sets sampled using an uniform sampling between 0 and 10''000:\n'...
        'number of parameter sets not used because of the IC: %d\n'...
        'number of parameter sets not used because they run the security: %d\n'...
        'number of parameter set not used because of a solver failure: %d\n',...
        'total number of parameter sets used: %d\n',...
        'number of valid parameter sets: %d\n',...
        'truth values from %e to %e\n\n'],...
        nb_suppr_CI,nb_suppr_traj,nb_plantage,nb_utilise,nb_valid,val_min,val_max);
fprintf(sortie);
fprintf(fid, sortie);
fprintf(fid,'\n');


% En utilisant un test pour les CI ??liminant des jeux de param??tres
% seulement si ceux-ci ont une partie r??elle n??gative, et en g??n??rant
% 1'000'002 jeux de param??tres, nous avons obtenus une erreur. Le programme
% a correctement trait?? 500'001 jeux de param??tres. Parmi ceux-la:
% - aucun n'a ??t?? ??limin?? ?? cause de ses conditions initiales
% - 377'197 ont ??t?? ??limin??s car leur trajectoire devenait n??gative.
% - parmi les 122'804 restant, aucun respecte okbehavior.
%
% Lors du traitement des 166'667 jeux de param??tres suivants, un jeu de
% param??tres a ??t?? ??limin?? car il menait ?? des conditions initiales
% non-valides. Ensuite, le solveur a plant?? lors du calcul des
% trajectoires.
%
% %%%%%%%%%%%%%%%
% 11 fev 2013 :
% En utilisant un test gardant les CI si la partie r??elle est positive,
% m??me s'il existe une partie complexe, nous obtenons les r??sultats
% suivants (1'000'000 jeux de param??tres initiaux, tir??s de mani??re
% uniforme entre 0 et 10'000):
% - 0 jeux de param??tres ont ??t?? supprim??s ?? cause des conditions initiales
%     (environ 939'000 d'entres eux avaient une partie imaginaire non
%     nulle)
% - 755'042 jeux de param??tres n'ont pas ??t?? conserv??s car la trajectoire
%     correspondante devient n??gative
% - O jeux de param??tres n'ont pas ??t?? utilis??s car plantant le solveur
% - parmi les 244'958 jeux de param??tres utilis??s, aucune ne respecte
%     okbehavior
% - temps de calcul : 15084 secondes
%
% %%%%%%%%%%%%%%%
% 18 fev 2013 :
% nombre de jeu de param??tre non utilis?? ?? cause des CI : 0 (mais environ
% 93% des CI avaient une partie imaginaire non nulle)
% nombre de jeu de param??tre non utilis?? car d??clanchant la s??curit?? : 754784
% nombre de jeu de param??tre non utilis?? car plantant le solveur : 0
% nombre total de jeu de param??tre utilis??s : 245216
% nombre de jeu de param??tres valides : 0
% valeurs de v??rit?? comprises entre -6.765554e+06 et -1.769428e-05
% Elapsed time is 14977.106904 seconds.
%

tic
val_min = inf;
val_max = -inf;
val_min2 = inf;
val_max2 = -inf;
nb_valid = 0;
nb_suppr_CI = 0;
nb_suppr_traj = 0;
nb_utilise = 0;
nb_valid2 = 0;
nb_plantage = 0;
for ii=1:N2
    fprintf(' -- step 3. %d/%d\n',ii,N2);
    [~,val,valid,suppr_CI,suppr_traj,utilise,valid2,val2,plantage] = check_param_3(Sys,N1);
    val = sort(val);
    if val(1)<val_min
        val_min = val(1);
    end
    if val(end)>val_max
        val_max = val(end);
    end
    val2 = sort(val2);
    if val2(1)<val_min2
        val_min2 = val2(1);
    end
    if val2(end)>val_max2
        val_max2 = val2(end);
    end
    nb_valid = nb_valid+valid;
    nb_suppr_CI = nb_suppr_CI+suppr_CI;
    nb_suppr_traj = nb_suppr_traj+suppr_traj;
    nb_utilise = nb_utilise+utilise;
    nb_valid2 = nb_valid2+valid2;
    nb_plantage = nb_plantage + plantage;
end
toc
sortie = sprintf(['\n\n Parameter sets generated using a uniform distribution with endpoints obtained from the litterature:\n'...
        'number of parameter sets not used because of the IC: %d\n'...
        'number of parameter sets not used because they run the security: %d\n'...
        'number of parameter sets not used because of a failure of the solver: %d\n',...
        'total number of used parameter sets: %d\n',...
        'number of parameter sets verifying okbehavior: %d\n',...
        'truth values from %e to %e\n',...
        'number of parameter set verifying okglobal3: %d\n',...
        'truth values from %e to %e\n\n'],...
        nb_suppr_CI,nb_suppr_traj,nb_plantage,nb_utilise,nb_valid,val_min,val_max,nb_valid2,val_min2,val_max2);
fprintf(sortie);
fprintf(fid, sortie);
fprintf(fid,'\n');


% 11 fev 2013 :
% En utilisant un test gardant les CI si la partie r??elle est positive,
% m??me s'il existe une partie complexe, nous obtenons les r??sultats
% suivants (1'000'000 jeux de param??tres initiaux, tir??s de mani??re
% uniforme dans les intervalles):
% - 0 jeux de param??tres ont ??t?? supprim??s ?? cause des conditions initiales
%     (environ 972'000 d'entres eux avaient une partie imaginaire non
%     nulle)
% - 0 jeux de param??tres n'ont pas ??t?? conserv??s car la trajectoire
%     correspondante devient n??gative
% - O jeux de param??tres n'ont pas ??t?? utilis??s car plantant le solveur
% - parmi les 1'000'000 jeux de param??tres utilis??s, aucun ne respecte
%     okbehavior ni okglobal3
% - temps de calcul : 96105 secondes (soit 26h 41min 45s)
%
% %%%%%%%%%%%%%%%
% 18 fev 2013 :
% Tirage des jeu de param??tres selon une loi uniforme comprise dans les intervalles de la litt??rature :
% nombre de jeu de param??tre non utilis?? ?? cause des CI : 0 (mais environ
% 97% des CI avaient une partie complexe non nulle. Globalement: abs(imag(x1))<1.0e-19)
% nombre de jeu de param??tre non utilis?? car d??clanchant la s??curit?? : 0
% nombre de jeu de param??tre non utilis?? car plantant le solveur : 0
% nombre total de jeu de param??tre utilis??s : 1000000
% nombre de jeu de param??tres valides pour okbehavior: 0
% valeurs de v??rit?? comprises entre -2.570695e+02 et -4.455689e-10
% nombre de jeu de param??tres valides pour okglobal3: 0
% valeuds de v??rit??s comprises entre -8.230182e+02 et -1.536324e-05
% Elapsed time is 94694.399435 seconds.
%



tic
val_min = inf;
val_max = -inf;
val_min2 = inf;
val_max2 = -inf;
nb_valid = 0;
nb_suppr_CI = 0;
nb_suppr_traj = 0;
nb_utilise = 0;
nb_valid2 = 0;
nb_plantage = 0;
for ii=1:N2
    fprintf(' -- step 4. %d/%d\n',ii,N2);
    [~,val,valid,suppr_CI,suppr_traj,utilise,valid2,val2,plantage] = check_param_4(Sys,N1);
    val = sort(val);
    if val(1)<val_min
        val_min = val(1);
    end
    if val(end)>val_max
        val_max = val(end);
    end
    val2 = sort(val2);
    if val2(1)<val_min2
        val_min2 = val2(1);
    end
    if val2(end)>val_max2
        val_max2 = val2(end);
    end
    nb_valid = nb_valid+valid;
    nb_suppr_CI = nb_suppr_CI+suppr_CI;
    nb_suppr_traj = nb_suppr_traj+suppr_traj;
    nb_utilise = nb_utilise+utilise;
    nb_valid2 = nb_valid2+valid2;
    nb_plantage = nb_plantage + plantage;
end
toc
sortie = sprintf(['\n\n Parameter sets generated using a uniform logarithmic distribution with endpoint obtained from the litterature:\n'...
        'number of parameter sets not used because of the IC: %d\n'...
        'number of parameter sets not used because they run the security: %d\n'...
        'number of parameter sets not used because of a failure of the solver: %d\n',...
        'total number of used parameter sets: %d\n',...
        'number of parameter sets verifying okbehavior: %d\n',...
        'truth values from %e to %e\n',...
        'number of parameter set verifying okglobal3: %d\n',...
        'truth values from %e to %e\n\n'],...
        nb_suppr_CI,nb_suppr_traj,nb_plantage,nb_utilise,nb_valid,val_min,val_max,nb_valid2,val_min2,val_max2);
fprintf(sortie)
fprintf(fid,sortie);

fclose(fid);

% 12 fev 2013:
% nombre de jeu de param??tre non utilis?? ?? cause des CI : 502732
% nombre de jeu de param??tre non utilis?? car d??clanchant la s??curit?? : 20
% nombre de jeu de param??tre non utilis?? car plantant le solveur : 447939
% nombre total de jeu de param??tre utilis??s : 49309
% nombre de jeu de param??tres valides pour okbehavior: 0
% valeurs de v??rit?? comprises entre -1.314928e+04 et -9.683455e-17
% nombre de jeu de param??tres valides pour okglobal3: 0
% valeuds de v??rit??s comprises entre -3.426375e+12 et -1.711229e-06
% Elapsed time is 4669.945764 seconds.
%
% %%%%%%%%
% 12 fev 2013 (2??me run)
% nombre de jeu de param??tre non utilis?? ?? cause des CI : 503114
% nombre de jeu de param??tre non utilis?? car d??clanchant la s??curit?? : 150
% nombre de jeu de param??tre non utilis?? car plantant le solveur : 168600
% nombre total de jeu de param??tre utilis??s : 328136
% nombre de jeu de param??tres valides pour okbehavior: 0
% valeurs de v??rit?? comprises entre -2.986883e+04 et -2.574449e-17
% nombre de jeu de param??tres valides pour okglobal3: 0
% valeuds de v??rit??s comprises entre -3.896192e+12 et -5.377550e-08
% Elapsed time is 32906.511187 seconds.
%
% %%%%%%%%
% 18 fev 2013 (pas sur de l'initialisation)
% nombre de jeu de param??tre non utilis?? ?? cause des CI : 501093
% nombre de jeu de param??tre non utilis?? car d??clanchant la s??curit?? : 102
% nombre de jeu de param??tre non utilis?? car plantant le solveur : 219613
% nombre total de jeu de param??tre utilis??s : 279192
% nombre de jeu de param??tres valides pour okbehavior: 0
% valeurs de v??rit?? comprises entre -7.873438e+04 et -6.930554e-17
% nombre de jeu de param??tres valides pour okglobal3: 0
% valeuds de v??rit??s comprises entre -4.182607e+12 et -8.026059e-07
% Elapsed time is 26433.663088 seconds.
%
% %%%%%%%%
% 19 fev 2013 (initialisation pas terrible)
% nombre de jeu de param??tre non utilis?? ?? cause des CI : 501803
% nombre de jeu de param??tre non utilis?? car d??clanchant la s??curit?? : 138
% nombre de jeu de param??tre non utilis?? car plantant le solveur : 193999
% nombre total de jeu de param??tre utilis??s : 304060
% nombre de jeu de param??tres valides pour okbehavior: 0
% valeurs de v??rit?? comprises entre -9.812631e+04 et -2.574449e-17
% nombre de jeu de param??tres valides pour okglobal3: 0
% valeuds de v??rit??s comprises entre -3.896192e+12 et -5.377550e-08
% Elapsed time is 28544.925059 seconds



