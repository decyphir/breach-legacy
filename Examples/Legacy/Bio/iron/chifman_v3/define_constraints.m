function [oksteady, okbehavior, okglobal, okbehavior3, okglobal3] = define_constraints()
%DEFINE_CONSTRAINTS defines the constraints.
%
% Synopsis: [oksteady, okbehavior, okglobal,okbehavior3, okglobal3] = define_constraints()
% 
% Outputs:
%  - oksteady   :  enforce stationarity before iron cutoff + intervals for
%                  initial conditions
%  - okbehavior :  enforce the plateau
%  - okglobal   :  enforce oksteady + okbehavior
%  - okbehavior3:  enforce the plateau + highest value of iron (2.0e-6)
%  - okglobal3  :  enforce oksteady + okbehavior3
%

CreateSystem;

Steadiness = '1.0e-6'; % 1.0e-5 or higher is not flat enough

% %%%%%%%%%%%%
% steady state
% %%%%%%%%%%%%

     % stationnarity
STL_Formula('stable', [...
		'abs(ddt{x1}[t]/x1[t])+',...
		'abs(ddt{x2}[t]/x2[t])+',...
		'abs(ddt{x3}[t]/x3[t])+',...
		'abs(ddt{x4}[t]/x4[t])+',...
		'abs(ddt{x5}[t]/x5[t]) <',Steadiness]);

STL_Formula('phi_x1', ' 6.0e-11 < x1[t]');
STL_Formula('phi_x2', '(1.0e-8  < x2[t]) and (x2[t] < 4.31e-8)');
STL_Formula('phi_x3', '(3.0e-10 < x3[t]) and (x3[t] < 1.0e-5 )');
STL_Formula('phi_x4', '(1.0e-13 < x4[t]) and (x4[t] < 1.0e-5 )');
STL_Formula('phi_x5', '(4.5e-9  < x5[t]) and (x5[t] < 1.55e-8)');
STL_Formula('phi_xall',' (phi_x1) and ((phi_x2) and ((phi_x3) and ((phi_x4) and (phi_x5))))');

oksteady = STL_Formula('oksteady', 'ev_[0,6*3600] (alw_[0,3600] ((stable) and (phi_xall)))');

% %%%%%%%%%%%%%%%%%%%%%%%
% iron-depleted situation
% %%%%%%%%%%%%%%%%%%%%%%%


okbehavior = STL_Formula('okbehavior',...
    ['ev_[6*3600, inf] ( alw_[0, 10*3600] ('...
     '((abs(ddt{x1}[t]/x1[t]) < ',Steadiness,') and '... % stationnarity of iron
     '(x1[t] > 0.01*x1[4*3600])) and '...                % enforce we not have a plateau at 0
     '(x1[t] > 2*x1[47*3600]) '...                       % ensure that the plateau is not too long
     ') )']);

okbehavior3 = STL_Formula('okbehavior3','(okbehavior) and (alw (x1[t]<2.0e-6))');

% %%%
% All
% %%%


okglobal = STL_Formula('okglobal', '(okbehavior) and (oksteady)');
okglobal3 = STL_Formula('okglobal3','(oksteady) and (okbehavior3)');


save('constraints.mat', 'stable');
save('constraints.mat', '-append','okbehavior');
save('constraints.mat', '-append','oksteady');
save('constraints.mat', '-append','okglobal');
save('constraints.mat', '-append','okbehavior3');
save('constraints.mat', '-append','okglobal3');

end

