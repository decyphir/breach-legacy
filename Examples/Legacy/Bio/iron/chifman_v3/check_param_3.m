function [P,val,nb_valid,nb_suppr_CI,nb_suppr_traj,nb_utilise,nb_valid2,val2,nb_plantage] = check_param_3(Sys,N)%,AA
%CHECK_PARAM_3 computes the number of parameter sets verifying okbehavior.
% Parameter sets are generated as using a continuous uniform distribution.
% The lower and upper endpoints are extracted from the litterature. The
% function prints the number of valid parameter sets.
%
% Synopsis: [P,val,nb_valid,nb_suppr_CI,nb_suppr_traj,nb_utilise,nb_valid2,val2,nb_plantage] = check_param_3(Sys,N)
%
% Inputs:
%  - Sys the system
%  - N   number of parameter set to generate
%
% Outputs:
%  - P   parameter set containing the trajectorise and the evaluation of
%        okbehavior
%  - val truth value of okbehavior
%%

[~,okbehavior,~,~,okglobal3] = define_constraints(); % define properties

%% tune Sys
ParamScales = Sys.p;
ParamScales(1:Sys.DimX-1) = 1.0e-9;
ParamScales(Sys.DimX) = 1; % Tf_sat
FSAoptions = CVodeSetFSAOptions('SensErrControl', 'off',...
                                'ParamField', 'p',...                                
                                'ParamScales',ParamScales );
Sys.CVodesSensiOptions.method = 'Simultaneous';
Sys.CVodesSensiOptions.FSAoptions = FSAoptions;


%% Create param set with N set of values
params = {... % will be also used later
    'a1',...
    'a2',...
    'g2',...
    'g3',...
    'g5',...
    'k52',...
    'a3',...
    'a4',...
    'a5',...
    'a6',...
    'gh',...
    'g4',...
    'k15',...
    'k53',...
    'k54',...
    'hep',...
};
P = CreateParamSet(Sys,params);
P = Refine(P,[N,ones(1,numel(P.dim)-1)]);


%% define random value for param sets
ranges =[ ... % in the same order than params
    2.0e-2   3.9e-2  ; ...  % a1
    1.0e-12  2.0e-12 ; ...  % a2
    2.0e-5   3.0e-5  ; ...  % g2
    0        9.6e-6  ; ...  % g3
    1.28e-5  1.84e-5 ; ...  % g5
    1.0e-8   1.0e-7  ; ...  % k52
    1.0e-15  1.0e-9  ; ...  % a3
    1.0e-9   1.0e4   ; ...  % a4
    1.0e-14  1.0e-12 ; ...  % a5
    3.0      1.0e5   ; ...  % a6
    1.0e-9   1.0e-2  ; ...  % gh
    1.0e-6   1.0e-2  ; ...  % g4
    3.67e-12 1.0e-5  ; ...  % k15
    1.0e-11  1.0e-4  ; ...  % k53
    1.0e-11  1.0e-4  ; ...  % k54
    1.0e-13  1.0e-5  ; ...  % hep
];

valeursParam = zeros(16,N); % 15 parameters + 1 input
for i=1:16
    valeursParam(i,:) = unifrnd(ranges(i,1),ranges(i,2),1,N);
end

%valeursParam = AA;

P.pts(P.DimX+1:P.DimP,:) = valeursParam; % we define all the params + input
P.ParamList(P.DimX+1:P.DimP) = params; % We specify the parameter order
                                       % (allowed because we don't use the
                                       % epsi)


%% compute initial conditions and remove all negative one
P = find_init_conditions_6(P);
isGood = P.pts(1,:)>=0; % check if the IC is negative or imaginary (if one is negative, then they are all equal to -10)
P.pts = P.pts(:,isGood);
P.epsi = P.epsi(:,isGood);
fprintf(' We keep %d over %d param sets (others have negative or complexe IC).\n',size(P.pts,2),N);
nb_suppr_CI = N - size(P.pts,2);


%% we compute the trajectories
try 
    P = ComputeTraj(Sys,P,0:600:48*3600);
catch err
    fprintf([err.message,'\n'])
    val = NaN;
    val2 = NaN;
    nb_valid = 0;
    nb_valid2 = 0;
    nb_plantage = size(P.pts,2);
    nb_suppr_traj = 0;
    nb_utilise = 0;
    return ;
end


%% we remove trajectories blocked by the security
phi_bloque = STL_Formula('phi_bloque',...
        'ev (alw (abs(ddt{x3}[t])+abs(ddt{x5}[t]) < 1.0e-17 ))');
[~,val_bloque] = SEvalProp(Sys,P,phi_bloque,0);
Pgood = Sselect(P, find(val_bloque<=0));
sprintf(' We keep %d among %d param sets (others are blocked by the security).\n',size(Pgood.pts,2),size(P.pts,2))
nb_suppr_traj = size(P.pts,2)-size(Pgood.pts,2);
nb_utilise = size(Pgood.pts,2);
P = Pgood;


%% we compute the truth value of okbehavior
[~,val] = SEvalProp(Sys,P,okbehavior,0);
nb_valid = numel(find(val>0));

fprintf('There are %d parameter sets verifying okbehavior.\n',nb_valid);


%% we compute okglobal3
[~,val2] = SEvalProp(Sys,P,okglobal3,0);
nb_valid2 = numel(find(val2>0));

fprintf('There are %d parameter sets verifying okglobal3.\n',nb_valid2);

nb_plantage = 0;


end
