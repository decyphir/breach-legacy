function P = find_init_conditions_6(P)
% FIND_INIT_CONDITIONS_6 defines the initial conditions of the Chifman/v3
% model. Chifman demonstrates that it exists one unique positive solution
% for each parameter set.
%
% We try to manage very small imaginary part due to small errors during the
% computations of the solution. The algorithm is the following:
% 1- we compute the first solution
% 2- if it is real and positive, we are done (it is "good" solution)
% 3- if the real part is negative , it is a "bad" solution
% 4- if the real part is positive and the absolute value of the imaginary
%     part is not null, we call this solution a "maybe" solution
% 5- if there are "maybe" or "bad" solution, we compute the second
%     solution.
% 6- we replace all the "bad" solution
% 7- we replace the old "maybe" solution by the new "good" solution
% 8- we replace the old "maybe" solution by the new "maybe" solution if the
%     they are better (TO DEFINE)
% 9- we proceed in a similar way with the third solution
%
% The equations used there come from the resolution of the system in its
% cannonical form (aka a*x1^3 + b*x1^2 + c*x1 + d = 0)
%
% Specificity of this file : if there are bad solutions, they are replaced
% by -10 and a warning (instead of an error is thrown)



for ii=1:numel(P.ParamList)
    eval([P.ParamList{ii} '= GetParam(P,''' P.ParamList{ii} ''');']);
end

i = sqrt(-1);

% when computing the analytical solution, we consider a1 as a1*feex and g3
% as g3+gh*hep.

a1 = a1.*Feex; 
g3 = g3+gh.*hep;

% %%%%%
% first solution
% %%%%%
x1 = (1./6.*a5.*k15.*sqrt((27.*a1.^2.*a2.^2.*a3.*a5.^2.*a6.*g2.*g3.^2.*g5.*k15.*k52.^2 - (a3.^3.*a5.^2.*a6.^3.*g2.^3.*g5.*k15.^3 + 2.*a1.*a2.*a3.^2.*a5.^2.*a6.^2.*g2.^2.*g3.*g5.*k15.^2 + a1.^2.*a2.^2.*a3.*a5.^2.*a6.*g2.*g3.^2.*g5.*k15 + (a3.^3.*a6.^3.*g2.^3.*g5.^3.*k15.^3 + 2.*a1.*a2.*a3.^2.*a6.^2.*g2.^2.*g3.*g5.^3.*k15.^2 + a1.^2.*a2.^2.*a3.*a6.*g2.*g3.^2.*g5.^3.*k15).*k52.^2 + 2.*(a3.^3.*a5.*a6.^3.*g2.^3.*g5.^2.*k15.^3 + 4.*a1.*a2.*a3.^2.*a5.*a6.^2.*g2.^2.*g3.*g5.^2.*k15.^2 + 5.*a1.^2.*a2.^2.*a3.*a5.*a6.*g2.*g3.^2.*g5.^2.*k15 + 2.*a1.^3.*a2.^3.*a5.*g3.^3.*g5.^2).*k52).*k53.^2 + 2.*(2.*a1.*a2.*a3.^2.*a6.^2.*g2.^2.*g3.*g5.^3.*k15.^2.*k52.^3 - 2.*a1.*a2.*a3.^2.*a5.^3.*a6.^2.*g2.^2.*g3.*k15.^2 + 3.*(a1.*a2.*a3.^2.*a5.*a6.^2.*g2.^2.*g3.*g5.^2.*k15.^2 + 3.*a1.^2.*a2.^2.*a3.*a5.*a6.*g2.*g3.^2.*g5.^2.*k15).*k52.^2 - 3.*(a1.*a2.*a3.^2.*a5.^2.*a6.^2.*g2.^2.*g3.*g5.*k15.^2 + 3.*a1.^2.*a2.^2.*a3.*a5.^2.*a6.*g2.*g3.^2.*g5.*k15).*k52).*k53).*k15./(a3.*a6.*g2.*g5)).*sqrt(1./3)./(a3.*a6.*g2.*g5.^2.*k52.^2.*k53) + 1./54.*(9.*(a5.*g3.*g5.^2.*k15.^2.*k52.^2.*k53 + (3.*k52.^2 - k52.*k53).*a5.^2.*g3.*g5.*k15.^2).*a1.*a2 + (2.*a6.*g2.*g5.^3.*k15.^3.*k52.^3.*k53 + 3.*a5.*a6.*g2.*g5.^2.*k15.^3.*k52.^2.*k53 - 3.*a5.^2.*a6.*g2.*g5.*k15.^3.*k52.*k53 - 2.*a5.^3.*a6.*g2.*k15.^3.*k53).*a3)./(a3.*a6.*g2.*g5.^3.*k52.^3.*k53)).^(1./3) - 1./3.*(2.*g5.*k15.*k52 + a5.*k15)./(g5.*k52) + 1./9.*(3.*a1.*a2.*a5.*g3.*g5.*k15.*k52 + (a6.*g2.*g5.^2.*k15.^2.*k52.^2 + a5.*a6.*g2.*g5.*k15.^2.*k52 + a5.^2.*a6.*g2.*k15.^2).*a3)./((1./6.*a5.*k15.*sqrt((27.*a1.^2.*a2.^2.*a3.*a5.^2.*a6.*g2.*g3.^2.*g5.*k15.*k52.^2 - (a3.^3.*a5.^2.*a6.^3.*g2.^3.*g5.*k15.^3 + 2.*a1.*a2.*a3.^2.*a5.^2.*a6.^2.*g2.^2.*g3.*g5.*k15.^2 + a1.^2.*a2.^2.*a3.*a5.^2.*a6.*g2.*g3.^2.*g5.*k15 + (a3.^3.*a6.^3.*g2.^3.*g5.^3.*k15.^3 + 2.*a1.*a2.*a3.^2.*a6.^2.*g2.^2.*g3.*g5.^3.*k15.^2 + a1.^2.*a2.^2.*a3.*a6.*g2.*g3.^2.*g5.^3.*k15).*k52.^2 + 2.*(a3.^3.*a5.*a6.^3.*g2.^3.*g5.^2.*k15.^3 + 4.*a1.*a2.*a3.^2.*a5.*a6.^2.*g2.^2.*g3.*g5.^2.*k15.^2 + 5.*a1.^2.*a2.^2.*a3.*a5.*a6.*g2.*g3.^2.*g5.^2.*k15 + 2.*a1.^3.*a2.^3.*a5.*g3.^3.*g5.^2).*k52).*k53.^2 + 2.*(2.*a1.*a2.*a3.^2.*a6.^2.*g2.^2.*g3.*g5.^3.*k15.^2.*k52.^3 - 2.*a1.*a2.*a3.^2.*a5.^3.*a6.^2.*g2.^2.*g3.*k15.^2 + 3.*(a1.*a2.*a3.^2.*a5.*a6.^2.*g2.^2.*g3.*g5.^2.*k15.^2 + 3.*a1.^2.*a2.^2.*a3.*a5.*a6.*g2.*g3.^2.*g5.^2.*k15).*k52.^2 - 3.*(a1.*a2.*a3.^2.*a5.^2.*a6.^2.*g2.^2.*g3.*g5.*k15.^2 + 3.*a1.^2.*a2.^2.*a3.*a5.^2.*a6.*g2.*g3.^2.*g5.*k15).*k52).*k53).*k15./(a3.*a6.*g2.*g5)).*sqrt(1./3)./(a3.*a6.*g2.*g5.^2.*k52.^2.*k53) + 1./54.*(9.*(a5.*g3.*g5.^2.*k15.^2.*k52.^2.*k53 + (3.*k52.^2 - k52.*k53).*a5.^2.*g3.*g5.*k15.^2).*a1.*a2 + (2.*a6.*g2.*g5.^3.*k15.^3.*k52.^3.*k53 + 3.*a5.*a6.*g2.*g5.^2.*k15.^3.*k52.^2.*k53 - 3.*a5.^2.*a6.*g2.*g5.*k15.^3.*k52.*k53 - 2.*a5.^3.*a6.*g2.*k15.^3.*k53).*a3)./(a3.*a6.*g2.*g5.^3.*k52.^3.*k53)).^(1./3).*a3.*a6.*g2.*g5.^2.*k52.^2);

[~,is_maybe,is_bad] = good_maybe_bad(x1);
f_x1 = f(x1,P);

if any(is_bad) || any(is_maybe) % if there is at least one variable having a negative value, we compute the second solution.
    % %%%%%
    % second solution
    % %%%%%
    x1_tmp = -1./2.*(-i.*sqrt(3) + 1).*(1./6.*a5.*k15.*sqrt((27.*a1.^2.*a2.^2.*a3.*a5.^2.*a6.*g2.*g3.^2.*g5.*k15.*k52.^2 - (a3.^3.*a5.^2.*a6.^3.*g2.^3.*g5.*k15.^3 + 2.*a1.*a2.*a3.^2.*a5.^2.*a6.^2.*g2.^2.*g3.*g5.*k15.^2 + a1.^2.*a2.^2.*a3.*a5.^2.*a6.*g2.*g3.^2.*g5.*k15 + (a3.^3.*a6.^3.*g2.^3.*g5.^3.*k15.^3 + 2.*a1.*a2.*a3.^2.*a6.^2.*g2.^2.*g3.*g5.^3.*k15.^2 + a1.^2.*a2.^2.*a3.*a6.*g2.*g3.^2.*g5.^3.*k15).*k52.^2 + 2.*(a3.^3.*a5.*a6.^3.*g2.^3.*g5.^2.*k15.^3 + 4.*a1.*a2.*a3.^2.*a5.*a6.^2.*g2.^2.*g3.*g5.^2.*k15.^2 + 5.*a1.^2.*a2.^2.*a3.*a5.*a6.*g2.*g3.^2.*g5.^2.*k15 + 2.*a1.^3.*a2.^3.*a5.*g3.^3.*g5.^2).*k52).*k53.^2 + 2.*(2.*a1.*a2.*a3.^2.*a6.^2.*g2.^2.*g3.*g5.^3.*k15.^2.*k52.^3 - 2.*a1.*a2.*a3.^2.*a5.^3.*a6.^2.*g2.^2.*g3.*k15.^2 + 3.*(a1.*a2.*a3.^2.*a5.*a6.^2.*g2.^2.*g3.*g5.^2.*k15.^2 + 3.*a1.^2.*a2.^2.*a3.*a5.*a6.*g2.*g3.^2.*g5.^2.*k15).*k52.^2 - 3.*(a1.*a2.*a3.^2.*a5.^2.*a6.^2.*g2.^2.*g3.*g5.*k15.^2 + 3.*a1.^2.*a2.^2.*a3.*a5.^2.*a6.*g2.*g3.^2.*g5.*k15).*k52).*k53).*k15./(a3.*a6.*g2.*g5)).*sqrt(1./3)./(a3.*a6.*g2.*g5.^2.*k52.^2.*k53) + 1./54.*(9.*(a5.*g3.*g5.^2.*k15.^2.*k52.^2.*k53 + (3.*k52.^2 - k52.*k53).*a5.^2.*g3.*g5.*k15.^2).*a1.*a2 + (2.*a6.*g2.*g5.^3.*k15.^3.*k52.^3.*k53 + 3.*a5.*a6.*g2.*g5.^2.*k15.^3.*k52.^2.*k53 - 3.*a5.^2.*a6.*g2.*g5.*k15.^3.*k52.*k53 - 2.*a5.^3.*a6.*g2.*k15.^3.*k53).*a3)./(a3.*a6.*g2.*g5.^3.*k52.^3.*k53)).^(1./3) - 1./3.*(2.*g5.*k15.*k52 + a5.*k15)./(g5.*k52) - 1./18.*(i.*sqrt(3) + 1).*(3.*a1.*a2.*a5.*g3.*g5.*k15.*k52 + (a6.*g2.*g5.^2.*k15.^2.*k52.^2 + a5.*a6.*g2.*g5.*k15.^2.*k52 + a5.^2.*a6.*g2.*k15.^2).*a3)./((1./6.*a5.*k15.*sqrt((27.*a1.^2.*a2.^2.*a3.*a5.^2.*a6.*g2.*g3.^2.*g5.*k15.*k52.^2 - (a3.^3.*a5.^2.*a6.^3.*g2.^3.*g5.*k15.^3 + 2.*a1.*a2.*a3.^2.*a5.^2.*a6.^2.*g2.^2.*g3.*g5.*k15.^2 + a1.^2.*a2.^2.*a3.*a5.^2.*a6.*g2.*g3.^2.*g5.*k15 + (a3.^3.*a6.^3.*g2.^3.*g5.^3.*k15.^3 + 2.*a1.*a2.*a3.^2.*a6.^2.*g2.^2.*g3.*g5.^3.*k15.^2 + a1.^2.*a2.^2.*a3.*a6.*g2.*g3.^2.*g5.^3.*k15).*k52.^2 + 2.*(a3.^3.*a5.*a6.^3.*g2.^3.*g5.^2.*k15.^3 + 4.*a1.*a2.*a3.^2.*a5.*a6.^2.*g2.^2.*g3.*g5.^2.*k15.^2 + 5.*a1.^2.*a2.^2.*a3.*a5.*a6.*g2.*g3.^2.*g5.^2.*k15 + 2.*a1.^3.*a2.^3.*a5.*g3.^3.*g5.^2).*k52).*k53.^2 + 2.*(2.*a1.*a2.*a3.^2.*a6.^2.*g2.^2.*g3.*g5.^3.*k15.^2.*k52.^3 - 2.*a1.*a2.*a3.^2.*a5.^3.*a6.^2.*g2.^2.*g3.*k15.^2 + 3.*(a1.*a2.*a3.^2.*a5.*a6.^2.*g2.^2.*g3.*g5.^2.*k15.^2 + 3.*a1.^2.*a2.^2.*a3.*a5.*a6.*g2.*g3.^2.*g5.^2.*k15).*k52.^2 - 3.*(a1.*a2.*a3.^2.*a5.^2.*a6.^2.*g2.^2.*g3.*g5.*k15.^2 + 3.*a1.^2.*a2.^2.*a3.*a5.^2.*a6.*g2.*g3.^2.*g5.*k15).*k52).*k53).*k15./(a3.*a6.*g2.*g5)).*sqrt(1./3)./(a3.*a6.*g2.*g5.^2.*k52.^2.*k53) + 1./54.*(9.*(a5.*g3.*g5.^2.*k15.^2.*k52.^2.*k53 + (3.*k52.^2 - k52.*k53).*a5.^2.*g3.*g5.*k15.^2).*a1.*a2 + (2.*a6.*g2.*g5.^3.*k15.^3.*k52.^3.*k53 + 3.*a5.*a6.*g2.*g5.^2.*k15.^3.*k52.^2.*k53 - 3.*a5.^2.*a6.*g2.*g5.*k15.^3.*k52.*k53 - 2.*a5.^3.*a6.*g2.*k15.^3.*k53).*a3)./(a3.*a6.*g2.*g5.^3.*k52.^3.*k53)).^(1./3).*a3.*a6.*g2.*g5.^2.*k52.^2);

    % 1/ we change all the bad solutions
    x1(is_bad) = x1_tmp(is_bad);
    
    % 2/ then we look for the "maybe" first solution which can be replaced
    % by "good" second solution
    [is_tmp_good,is_tmp_maybe,~] = good_maybe_bad(x1_tmp);

    x1(is_maybe & is_tmp_good) = x1_tmp(is_maybe & is_tmp_good);
    
    % 3/ finally, we compare first "maybe" solutions and second "maybe"
    % solutions
    f_x1_tmp = f(x1_tmp,P);
    is_f_lower = abs(f_x1_tmp)<abs(f_x1);
    
    x1(is_maybe & is_f_lower & is_tmp_maybe) = x1_tmp(is_maybe & is_f_lower & is_tmp_maybe); % we keep the closest polynome value to 0
    
    
    % preparation for comparison with the next solution
    f_x1 = f(x1,P);
    [~,is_maybe,is_bad] = good_maybe_bad(x1);

    if any(is_bad) || any(is_maybe)
        % %%%%%
        % third solution
        % %%%%%
        x1_tmp = -1./2.*(i.*sqrt(3) + 1).*(1./6.*a5.*k15.*sqrt((27.*a1.^2.*a2.^2.*a3.*a5.^2.*a6.*g2.*g3.^2.*g5.*k15.*k52.^2 - (a3.^3.*a5.^2.*a6.^3.*g2.^3.*g5.*k15.^3 + 2.*a1.*a2.*a3.^2.*a5.^2.*a6.^2.*g2.^2.*g3.*g5.*k15.^2 + a1.^2.*a2.^2.*a3.*a5.^2.*a6.*g2.*g3.^2.*g5.*k15 + (a3.^3.*a6.^3.*g2.^3.*g5.^3.*k15.^3 + 2.*a1.*a2.*a3.^2.*a6.^2.*g2.^2.*g3.*g5.^3.*k15.^2 + a1.^2.*a2.^2.*a3.*a6.*g2.*g3.^2.*g5.^3.*k15).*k52.^2 + 2.*(a3.^3.*a5.*a6.^3.*g2.^3.*g5.^2.*k15.^3 + 4.*a1.*a2.*a3.^2.*a5.*a6.^2.*g2.^2.*g3.*g5.^2.*k15.^2 + 5.*a1.^2.*a2.^2.*a3.*a5.*a6.*g2.*g3.^2.*g5.^2.*k15 + 2.*a1.^3.*a2.^3.*a5.*g3.^3.*g5.^2).*k52).*k53.^2 + 2.*(2.*a1.*a2.*a3.^2.*a6.^2.*g2.^2.*g3.*g5.^3.*k15.^2.*k52.^3 - 2.*a1.*a2.*a3.^2.*a5.^3.*a6.^2.*g2.^2.*g3.*k15.^2 + 3.*(a1.*a2.*a3.^2.*a5.*a6.^2.*g2.^2.*g3.*g5.^2.*k15.^2 + 3.*a1.^2.*a2.^2.*a3.*a5.*a6.*g2.*g3.^2.*g5.^2.*k15).*k52.^2 - 3.*(a1.*a2.*a3.^2.*a5.^2.*a6.^2.*g2.^2.*g3.*g5.*k15.^2 + 3.*a1.^2.*a2.^2.*a3.*a5.^2.*a6.*g2.*g3.^2.*g5.*k15).*k52).*k53).*k15./(a3.*a6.*g2.*g5)).*sqrt(1./3)./(a3.*a6.*g2.*g5.^2.*k52.^2.*k53) + 1./54.*(9.*(a5.*g3.*g5.^2.*k15.^2.*k52.^2.*k53 + (3.*k52.^2 - k52.*k53).*a5.^2.*g3.*g5.*k15.^2).*a1.*a2 + (2.*a6.*g2.*g5.^3.*k15.^3.*k52.^3.*k53 + 3.*a5.*a6.*g2.*g5.^2.*k15.^3.*k52.^2.*k53 - 3.*a5.^2.*a6.*g2.*g5.*k15.^3.*k52.*k53 - 2.*a5.^3.*a6.*g2.*k15.^3.*k53).*a3)./(a3.*a6.*g2.*g5.^3.*k52.^3.*k53)).^(1./3) - 1./3.*(2.*g5.*k15.*k52 + a5.*k15)./(g5.*k52) - 1./18.*(-i.*sqrt(3) + 1).*(3.*a1.*a2.*a5.*g3.*g5.*k15.*k52 + (a6.*g2.*g5.^2.*k15.^2.*k52.^2 + a5.*a6.*g2.*g5.*k15.^2.*k52 + a5.^2.*a6.*g2.*k15.^2).*a3)./((1./6.*a5.*k15.*sqrt((27.*a1.^2.*a2.^2.*a3.*a5.^2.*a6.*g2.*g3.^2.*g5.*k15.*k52.^2 - (a3.^3.*a5.^2.*a6.^3.*g2.^3.*g5.*k15.^3 + 2.*a1.*a2.*a3.^2.*a5.^2.*a6.^2.*g2.^2.*g3.*g5.*k15.^2 + a1.^2.*a2.^2.*a3.*a5.^2.*a6.*g2.*g3.^2.*g5.*k15 + (a3.^3.*a6.^3.*g2.^3.*g5.^3.*k15.^3 + 2.*a1.*a2.*a3.^2.*a6.^2.*g2.^2.*g3.*g5.^3.*k15.^2 + a1.^2.*a2.^2.*a3.*a6.*g2.*g3.^2.*g5.^3.*k15).*k52.^2 + 2.*(a3.^3.*a5.*a6.^3.*g2.^3.*g5.^2.*k15.^3 + 4.*a1.*a2.*a3.^2.*a5.*a6.^2.*g2.^2.*g3.*g5.^2.*k15.^2 + 5.*a1.^2.*a2.^2.*a3.*a5.*a6.*g2.*g3.^2.*g5.^2.*k15 + 2.*a1.^3.*a2.^3.*a5.*g3.^3.*g5.^2).*k52).*k53.^2 + 2.*(2.*a1.*a2.*a3.^2.*a6.^2.*g2.^2.*g3.*g5.^3.*k15.^2.*k52.^3 - 2.*a1.*a2.*a3.^2.*a5.^3.*a6.^2.*g2.^2.*g3.*k15.^2 + 3.*(a1.*a2.*a3.^2.*a5.*a6.^2.*g2.^2.*g3.*g5.^2.*k15.^2 + 3.*a1.^2.*a2.^2.*a3.*a5.*a6.*g2.*g3.^2.*g5.^2.*k15).*k52.^2 - 3.*(a1.*a2.*a3.^2.*a5.^2.*a6.^2.*g2.^2.*g3.*g5.*k15.^2 + 3.*a1.^2.*a2.^2.*a3.*a5.^2.*a6.*g2.*g3.^2.*g5.*k15).*k52).*k53).*k15./(a3.*a6.*g2.*g5)).*sqrt(1./3)./(a3.*a6.*g2.*g5.^2.*k52.^2.*k53) + 1./54.*(9.*(a5.*g3.*g5.^2.*k15.^2.*k52.^2.*k53 + (3.*k52.^2 - k52.*k53).*a5.^2.*g3.*g5.*k15.^2).*a1.*a2 + (2.*a6.*g2.*g5.^3.*k15.^3.*k52.^3.*k53 + 3.*a5.*a6.*g2.*g5.^2.*k15.^3.*k52.^2.*k53 - 3.*a5.^2.*a6.*g2.*g5.*k15.^3.*k52.*k53 - 2.*a5.^3.*a6.*g2.*k15.^3.*k53).*a3)./(a3.*a6.*g2.*g5.^3.*k52.^3.*k53)).^(1./3).*a3.*a6.*g2.*g5.^2.*k52.^2);

        % 1/ we change all the bad solutions
        x1(is_bad) = x1_tmp(is_bad);

        % 2/ then we look for the "maybe" solution which can be replaced
        % by "good" third solution

        [is_tmp_good,is_tmp_maybe,~] = good_maybe_bad(x1_tmp);

        x1(is_maybe & is_tmp_good) = x1_tmp(is_maybe & is_tmp_good);
        
        % 3/ finally, we compare "maybe" solutions and third "maybe"
        % solutions
        f_x1_tmp = f(x1_tmp,P);
        is_f_lower = abs(f_x1_tmp)<abs(f_x1);

        x1(is_maybe & is_f_lower & is_tmp_maybe) = x1_tmp(is_maybe & is_f_lower & is_tmp_maybe); % we keep the closest polynome value to 0
        
        % testing the final solution
        [~,is_maybe,is_bad] = good_maybe_bad(x1);
    end
end

if any(is_maybe)
    warning('find_init_conditions_6:bad_IC',...
            ['There are %d initial conditions with non-zero imaginary part (between %g and %g) and positive real part.'...
            ' The imaginary part has been skipped.'],numel(find(is_maybe==true)),min(imag(x1)),max(imag(x1)) );
    x1 = real(x1);
end

x5 = a5.*k15./(g5.*(k15+x1));
x4 = a4.*k54.*x1./(g4.*(k54+x5));
x3 = a3.*k53./(g3.*(k53+x5));
x2 = a2.*x5./(g2.*(k52+x5));

if any(is_bad)
    warning('find_init_conditions_6:bad_IC',...
            'There are %d initial conditions with negative real part.',numel(find(is_bad==true)));
    x1(is_bad) = -10;
    x2(is_bad) = -10;
    x3(is_bad) = -10;
    x4(is_bad) = -10;
    x5(is_bad) = -10;
end


% fprintf('dx1 : %g, dx2 : %g, dx3 : %g, dx4 : %g, dx5 : %g\n',...
%     a1.*x2-a6.*x1.*x3,...
%     a2.*x5./(k52+x5)-g2.*x2,...
%     a3.*k53./(k53+x5)-g3.*x3,...
%     a4.*x1.*k54./(k54+x5)-g4.*x4,...
%     a5.*k15./(k15+x1)-g5.*x5)

P = SetParam(P,'x1',x1);
P = SetParam(P,'x2',x2);
P = SetParam(P,'x3',x3);
P = SetParam(P,'x4',x4);
P = SetParam(P,'x5',x5);

end

function y = f(x1,P)
%F computes the value of the polynome

for ii=1:numel(P.ParamList)
    eval([P.ParamList{ii} '= GetParam(P,''' P.ParamList{ii} ''');']);
end

% when computing the analytical solution, we consider a1 as a1*feex and g3
% as g3+gh*hep.

a1 = a1.*Feex;
g3 = g3+gh.*hep;

x1 = real(x1);
y = a1.* (  a2./(g2.*( 1+k52.*g5.*(k15+x1)./(a5.*k15) ))  ) -a6.*x1.* (  a3.*k53./(g3.*( k53+a5.*k15./(g5.*(k15+x1)) ))  );
end



function [is_good,is_maybe,is_bad] = good_maybe_bad(x1)
% good if x1 is real positive
% bad if real part of x1 is negative
% maybe otherwise


% the "list" of correct solutions
is_good = real(x1)>=0 & imag(x1)==0;

% the "list" of indexes such that the first solution computed may be the
% solution (if not better solution is found)
is_maybe = imag(x1)~=0 & real(x1)>=0;

is_bad = ~(is_good | is_maybe); % NaN are bad

end
