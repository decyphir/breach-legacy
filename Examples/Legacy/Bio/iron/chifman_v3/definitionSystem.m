function definitionSystem()
%DEFINITIONSYSTEM Create the system (v3)

% This directory is specially created to test if there are parameters
% values choosen in the chiman way that behaves as expected.
% the chiman way consists to randomly generate parameter value with an
% exponential distribution centered in 1000.
% As these values definitely differs from the usual one, we desactivate the
% security freezing the system if the concentration becomes too high.
% Nevertheless, we keep the one freezing if the concentration becomes (too)
% negative.


initial.Derivatives={...
	['ddt{x1} ='... %iron
			'a1*Feex*x2 +'... %iron input
			'g4*x4 - a4*x1*k54/(k54+x5) -'... %load/unload of ft
			'a6*x1*x3'],... %export by fpn
	['ddt{x2} ='... %tfr1
			'a2*x5/(k52+x5) -'... %regulation by IRP
			'g2*x2'],... %spontaneous degradation
	['ddt{x3} ='... %fpn
			'a3*k53/(k53+x5) -'...
			'(g3+gh*hep)*x3'],... %hepc-mediated + spontaneous degradation
	['ddt{x4} ='... %ft
			'a4*x1*k54/(k54+x5) -'... %regulation by IRP
			'g4*x4'],... %spontaneous degradation
	['ddt{x5} ='... %IRP
			'a5*k15/(k15+x1) -'... %regulation by IRP
			'g5*x5'],... %spontaneous degradation
	['ddt{Feex} =3000*(1-pow(t,40)/(pow(t,40)+pow(21600,40))) -'...
				'10000*Feex'],...
};

initial.x0Values={...
    'x1(0)=7.966e-7',...
    'x2(0)=5.31e-8',...
    'x3(0)=5.0e-7',...
    'x4(0)=1.4e-8',...
    'x5(0)=7.9e-9',...
	'Feex(0)=0.3',...
};

initial.ParamValues={...
	'a1 = 3.0e-2',... %iron input
	'a2 = 1.5e-12',... %prod tfr1
	'a3 = 2.5e-12',... %prod fpn
	'a4 = 1e-9',... %prod ft
	'a5 = 1.0e-12',... %prod irp
	'a6 = 300',... %iron export by fpn
	'k15 = 1.0e-5',... % iron->irp
	'k52 = 1.0e-7',... % irp->tfr1
	'k53 = 5.0e-11',... % irp->fpn
	'k54 = 7e-11',... % irp->ft
	'g2 = 2.4e-5',... %tfr1 degradation
	'g3 = 5.0e-6',... %fpn degradation
	'g4 = 5.0e-3',... %ft degradation
	'g5 = 1.4e-5',... %irp degradation
	'gh = 0.5e-5',... %hepcidine-mediated fpn degradation
    'hep = 1.0e-9',... %hepcidine concentration
...	%parameter specific to the iron input change -> hard written in eqns
};

% We set the tolerances
initial.RelTol = 1.0e-6;
initial.AbsTol = 1.0e-16;

% The command NewSystem creates the files 
% CreateSystem.m, dynamics.cpp, dynamics.h:
NewSystem(initial);

% add at the end of CreateSystem commands to set the time-scale
fid = fopen('CreateSystem.m','a');
fprintf(fid,'Sys.time_mult = 1/3600;\n');
fclose(fid);

% we define the initialization function (and the time step)
%fid = fopen('CreateSystem.m','a');
%fprintf(fid,'Sys.init_fun = @find_init_conditions_5;\n'); %updated to v6
%fprintf(fid,'Sys.tspan = 0:600:48*3600');
%fclose(fid);

% call to CreateSystem to create the variable Sys 
run(['.',filesep,'CreateSystem']);

% we define a security guard
fixLimit(['x1<-1.0e-4 ||'...
    'x2<-1.0e-4 ||'...
    'x3<-1.0e-4 ||'...
    'x4<-1.0e-4 ||'...
    'x5<-1.0e-4']);

% We compile the system (creation of cvm.mexglx)
CompileSystem(Sys)

end
