function [oksteady,okparam,okbehavior,okglobal,okbehavior_v2,okglobal_v2] = define_constraints()
%DEFINE_CONSTRAINTS define constraints for simplif3 system
%
% [oksteady,okparam,okbehavior,okglobal,okbehavior_v2,okglobal_v2] = define_constraints()
%
% the _v2 version includes experimental data from JMM
%

Steadiness = '1.0e-6'; % 1.0e-5 or higher is not flat enough
Epsi = '1e-15';



% %%%%%
% Formulas to characterize iron-repleted situation
% %%%%%

% steady state
     % stationnarity - v1
%STL_Formula('phi_s01', ['abs(ddt{Fe}[t]   / Fe[t])   <',Steadiness]);
%STL_Formula('phi_s02', ['abs(ddt{Ft}[t]   / Ft[t])   <',Steadiness]);
%STL_Formula('phi_s03', ['abs(ddt{FPN1a}[t]/ FPN1a[t])<',Steadiness]);
%STL_Formula('phi_s04', ['abs(ddt{IRP}[t]  / IRP[t])  <',Steadiness]);
%STL_Formula('phi_s05', ['abs(ddt{TfR1}[t] / TfR1[t]) <',Steadiness]);

%STL_Formula('stable', '(((phi_s01) and (phi_s02)) and ((phi_s03) and (phi_s04))) and (phi_s05)');

     % stationnarity - v2
STL_Formula('stable', [...
		'abs(ddt{Fe}[t]/Fe[t])+',...
		'abs(ddt{Ft}[t]/Ft[t])+',...
		'abs(ddt{FPN1a}[t]/FPN1a[t])+',...
		'abs(ddt{IRP}[t]/IRP[t])+',...
		'abs(ddt{TfR1}[t]/TfR1[t]) <',Steadiness]);

%STL_Formula('phi_test_stable', 'ev (alw_[0,3600] ((stable) and (t<6*3600)))');

     % biological data (or deduced by the interval solver)
STL_Formula('phi_s6', '3.5e-8 < Fe[t]'); %Fe always lower than 2e-6
STL_Formula('phi_s7', '(3.0e-9 < IRP[t]) and (IRP[t] < 1.07e-8)');
STL_Formula('phi_s8', '(1.0e-8 < TfR1[t]) and (TfR1[t] < 8.7e-8)');
STL_Formula('phi_s9', '(1.04e-13 < FPN1a[t]) and (FPN1a[t] < 1.0e-5)');
STL_Formula('phi_s10', '(1.0e-13 < Ft[t]) and (Ft[t] < 1.0e-5)');

     % formula deduced from the expected behavior in iron-depleted situation
STL_Formula('phi_s11', 'IRP[t] < theta_IRP_Ft');
STL_Formula('phi_s12', 'Fe[t] > theta_Fe_IRP');
STL_Formula('phi_s13', 'IRP[t] < theta_IRP_FPN1a');

     % formula from the way we model / biological data
STL_Formula('phi_s14', ['k_Fe_cons + ',Epsi,' > k_Fe_export*FPN1a[t]']);

     % grouping alltogether
STL_Formula('phi_sall', ['(stable) and ((phi_s6) and ((phi_s7) and ' ...
						'((phi_s8) and ((phi_s9) and ((phi_s10) and ' ...
						'((phi_s11) and ((phi_s12) and ((phi_s13) and ' ...
						'(phi_s14)))))))))']);

oksteady = STL_Formula('oksteady', 'ev_[0,6*3600] (alw_[0,3600] (phi_sall))');



% %%%%%
% Parameter relations
% %%%%%

     % formula from the way we model
STL_Formula('phi_p1', ['k_FPN1a_prod + ',Epsi,' > k_IRP_FPN1a']);
STL_Formula('phi_p2', ['k_Ft_prod + ',Epsi,' > k_IRP_Ft']);

     % formula deduced from the expected behavior in iron-depleted situation
STL_Formula('phi_p3', 'k_IRP_Ft > 0.95*k_Ft_prod');

     % grouping alltogether
okparam = STL_Formula('okparam', '(phi_p1) and ((phi_p2) and (phi_p3))');



% %%%%%
% Formulas to characterize iron-depleted situation
% %%%%%

STL_Formula('phi_Fe_stable',['abs(ddt{Fe}[t]/Fe[t]) <',Steadiness]);

     % existence of a plateau
%old version : STL_Formula('phi_b1', 'ev_[6*3600, inf] ( alw_[0, 10*3600] ( (phi_Fe_stable) and (Fe[t] > 0.01*Fe[4*3600]) ) )');
STL_Formula('phi_b1', ['ev_[6*3600, inf] ( alw_[0, 10*3600] ('...
    '((phi_Fe_stable) and '...
    '(Fe[t] > 0.01*Fe[4*3600])) and '... % enforce we not have a plateau at 0
    '(Fe[t] > 2*Fe[47*3600]) '... % ensure that the plateau is not too long
    ') )']);

     % biological data
STL_Formula('phi_b2', 'alw (Fe[t] < 2e-6)'); % To be satisfied all the time

     % NEW : data from JMM/EP experiments
STL_Formula('phi_b3', 'Ft[30*3600]<Ft[4*3600]/3');

     % grouping alltogether
okbehavior = STL_Formula('okbehavior', '(phi_b1) and (phi_b2)');
% new version including JMM/EP results
okbehavior_v2 = STL_Formula('okbehavior_v2','(phi_b1) and ((phi_b2) and (phi_b3))');


% %%%%%
% All
% %%%%%

okglobal = STL_Formula('okglobal', '(okbehavior) and ((oksteady) and (okparam))');
% new version including JMM results
okglobal_v2 = STL_Formula('okglobal_v2', '(okbehavior_v2) and ((oksteady) and (okparam))');


save('constraints.mat', 'phi_*');

save('constraints.mat', '-append','oksteady');
save('constraints.mat', '-append','okparam');
save('constraints.mat', '-append','okbehavior');
save('constraints.mat', '-append','okglobal');
save('constraints.mat', '-append','okbehavior_v2');
save('constraints.mat', '-append','okglobal_v2');

end

