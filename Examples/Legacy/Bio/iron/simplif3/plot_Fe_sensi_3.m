
% compute ans plot roughtly global sensitivity (average of 20 trajectories)
% of the system to the parameters in Pall

define_param_sets; % define Pall

Prlog = RandomLogRefine(Pall,20, 1.0e-20);
Prlog = init_to_steady(Prlog);

iX = FindParam(Prlog, {'Fe','Ft','FPN1a','IRP','TfR1'});
iP = Prlog.dim;

opts.args.iX = iX;
opts.args.iP = iP;
opts.props = {};   
opts.args.tspan = 3*3600:600:20*3600;
opts.open_dialog = 0;
opts.plot_histo = 0; 

ParamScales = Sys.p;
ParamScales(1:Sys.DimX-1) = 1e-8;
ParamScales(1:Sys.DimX) = 1; % TFsat


FSAoptions = CVodeSetFSAOptions('SensErrControl', 'off',...
                                'ParamField', 'p',...                                
                                'ParamScales',ParamScales );
Sys.CVodesSensiOptions.method = 'Simultaneous';
Sys.CVodesSensiOptions.FSAoptions = FSAoptions;

[M, opts] = SplotSensiBar(Sys, Prlog,[], opts);

plot_histo(M, Prlog, iX,[], iP);
%fig_resize(gcf, 1.5, 1);
