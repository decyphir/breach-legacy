% plot behaviors of the model

define_param_sets;
P0 = init_to_steady(P0);
Pall = init_to_steady(Pall);

tspan = 0:600:48*3600;
CreateSystem;

%opt = {'-b', 'LineWidth',2, 'FontSize', 14};

opt = {'-b', 'LineWidth',2};

Pf = ComputeTraj(Sys,P0,tspan);
h = figure;
SplotVar(Pf, [6,2,4,1,3,5], [], opt);
d = get(h, 'Children'); % get subplots  
set(d, 'FontSize', 12) %
xlabel('time (hours)')

fig_resize(h, 1, 2); % height x 2 
save2pdf('fig_nominal.pdf',h);
