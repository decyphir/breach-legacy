
% compute global sensitivity of okglobal to the parameters in Pall. We use
% the personnal version of SPropSensi. This script computes two times the
% sensibility. The first one, it shows it on a unity scale, the second
% time, on a log scale.

clear opts;

CreateSystem;

define_param_sets; % define P0, params and ranges
[~,~,~,~,~,okglobal_v2] = define_constraints(); % define okglobal

opts.tspan = 0:600:48*3600;
opts.params = params;
opts.tprop = 0;
opts.lbound = ranges(:,1)';
opts.ubound = ranges(:,2)';
opts.r = 1000; % 200
opts.p = 16; % 8
opts.logPlot = 1;
%opts.muGraphOpt = {'XScale','log'}; % aussi possible 'XLim',[0 1]
%opts.muGraphOpt = {'XLim',[-1 2]};
%opts.mustarGraphOpt = {'XScale','log'};
%opts.sigmGraphOpt = {'XScale','log'};

ParamScales = Sys.p;
ParamScales(1:Sys.DimX-1) = 1e-8;
ParamScales(1:Sys.DimX) = 1; % TFsat


FSAoptions = CVodeSetFSAOptions('SensErrControl', 'off',...
                                'ParamField', 'p',...                                
                                'ParamScales',ParamScales );
Sys.CVodesSensiOptions.method = 'Simultaneous';
Sys.CVodesSensiOptions.FSAoptions = FSAoptions;

tic
SPropSensiPerso(Sys,P0,okglobal_v2,opts);
toc



% %%%%%%%
% 8 mars 2013
% r=1000, p=16
% temps de calcul = 2073.697835 seconds.
%
% %%%%%%%
% 8 mars 2013
% r=1000, p=16
% temps de calcul = 2101.918279 seconds.
%
% %%%%%%%


