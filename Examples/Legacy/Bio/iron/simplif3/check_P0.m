define_param_sets;
define_constraints;


 for ii=1:numel(P0.ParamList)
    eval( [P0.ParamList{ii} '= GetParam(P0,''' P0.ParamList{ii} ''');']);
 end

N = 100;
def = 1;

% %still not good
% coef_cat1 = 1.5;
% coef_cat2 = 3;
% coef_cat3 = 1.5;
% coef_cat4 = 1.5;
% coef_cat5 = 3;
% 
% 
% scal = [...    
% def*coef_cat5;...    % 'k_FPN1a_deg'    
% def*1.7647;...%*coef_cat4; ...   % 'k_TfR1_prod' %% max = *1.7647;...%
% 0.857; ...   % 'k_IRP_deg'      
% def*coef_cat3; ...   % 'k_TfR1_deg' %% max = *1.666;...%
% 0.2857; ...   % 'k_IRP_TfR1'     
% def*coef_cat3; ...   % 'k_Fe_input'     
% def*coef_cat2; ...   % 'n_Ft'           
% def*coef_cat5; ...   % 'theta_IRP_FPN1a'
% def*coef_cat2; ...   % 'theta_Fe_IRP'   
% def*coef_cat2; ...   % 'theta_IRP_Ft'   
% def*coef_cat1; ...   % 'k_Ft_prod'      
% def*coef_cat4; ...   % 'k_IRP_prod'     
% def*1.3;...%coef_cat3; ...   % 'k_Fe_cons' %% max = 1.3
% def*coef_cat4; ...   % 'k_Fe_export'    
% def*coef_cat5; ...   % 'k_FPN1a_prod'   
% def*coef_cat1; ...   % 'k_Ft_deg'       
% def*coef_cat4; ...   % 'k_Fe_IRP'       
% def*coef_cat5  ...   % 'k_IRP_FPN1a'       
% ];
    

coef_autre=3; %fe_input (max=3)
coef_inf=3.5;
coef_ft=0.9; % Ft, n_Ft
coef_tfr1=2; %tfr1, k_IRP_TfR1 (sert à rien)
coef_irp=1.1; %irp_prod k_Fe_IRP
coef_fe_cons=1; %fe_cons (max=1.3)
coef_fe_export=1; %fpn et fe_export

scal = [...    
def*coef_fe_export;... % 'k_FPN1a_deg'    
def*1.7647;...%*coef_tfr1; ...   % 'k_TfR1_prod' %% max = *1.7647;...%
0.857; ...   % 'k_IRP_deg'      
def*1.666;...%*coef_tfr1; ...   % 'k_TfR1_deg' %% max = *1.666;...%
0.2857; ...   % 'k_IRP_TfR1'     
def*coef_autre; ...  % 'k_Fe_input' %% max = *3;...%
def*coef_ft; ...% 'n_Ft'           
def*coef_inf; ...    % 'theta_IRP_FPN1a'
def*coef_inf; ...    % 'theta_Fe_IRP'   
def*coef_inf; ...    % 'theta_IRP_Ft'   
def*coef_ft; ...% 'k_Ft_prod'      
def*coef_irp; ...   % 'k_IRP_prod'     
def*coef_fe_cons;... % 'k_Fe_cons' %% max = 1.3
def*coef_fe_export; ...% 'k_Fe_export'    
def*coef_fe_export; ...% 'k_FPN1a_prod'   
def*coef_ft; ...% 'k_Ft_deg'       
def*coef_irp; ...  % 'k_Fe_IRP'       
def*coef_inf  ...    % 'k_IRP_FPN1a'       
];

P0.epsi = P0.epsi.*scal;

if(check_interval(P0,Pall,1)==0)
	disp '--- intervals too large ---';
	%return;
end

% on génère des points aléatoirement
Pr = QuasiRefine(P0,N);

% on initialise
Pr = init_to_steady(Pr);

% on calcule les traj
tspan = 0:600:48*3600;
Pf = ComputeTraj(Sys, Pr, tspan, 0);

%SplotVar(Pf, 1:5) % a utiliser pour visualiser les trajectoires avec un N faible (max 100)

%on vérifie que les trajectoires respectent la propriété okglobal
[Pf, val] = SEvalProp(Sys, Pf, okglobal);
fprintf([num2str(numel(val(val>0))) '/' num2str(N) ' traj satisfy okglobal\n']);

         
