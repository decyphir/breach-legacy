
% le 09 nov 2012 :
% script permettant de trouver la valeur maximale et minimale de la
% concentration en fer à l'état stationnaire. Les intervalles sont ceux
% décrit comme étant correct. On trouve que la concentration en fer peut
% être supérieure à 2.0e-6, ce qui ne devrait pas arriver. (Il y a une
% contrainte imposant que alw (Fe[t]<2.0e-6) )

nbPoint = 1000000;

var = {
    'k_IRP_deg',...
    'k_Fe_IRP',...
    'k_FPN1a_deg',...
    'k_Fe_input',...
    'k_TfR1_prod',...
    'k_IRP_TfR1',...
    'k_IRP_prod',...
    'k_FPN1a_prod',...
    'k_Fe_export',...
    'k_Fe_cons',...
    'k_TfR1_deg'};

param = [
    1.28e-5  1.52e-5 ;...
    8.9e-4   1.11e-3 ;...
    4.5e-6   5.5e-6  ;...
    2.1e-2   3.9e-2  ;...
    1.4e-13  1.99e-13;...
    1.36e-4  1.44e-4 ;...
    7.12e-12 8.88e-12;...
    2.25e-12 2.75e-12;...
    270      330     ;...
    2.7e-4   3.3e-4  ;...
    2.0e-5   2.8e-5];

for i=1:numel(var)
    eval([var{i}, '= param(i,1) + (param(i,1)-param(i,2))*rand(nbPoint,1);']);
end

Tf_sat0 = 0.3;


Fe = (((k_IRP_deg+k_Fe_IRP).*k_FPN1a_deg.*k_Fe_input.*k_TfR1_prod+ ...
       k_FPN1a_deg.*k_Fe_input.*k_IRP_TfR1.*k_IRP_prod).*Tf_sat0)  ./  (((k_FPN1a_prod.*k_Fe_export+k_FPN1a_deg.*k_Fe_cons).*k_IRP_deg+k_FPN1a_prod.*k_Fe_IRP.*k_Fe_export+k_FPN1a_deg.*k_Fe_IRP.*k_Fe_cons).*k_TfR1_deg);
disp(['min : ', num2str(min(Fe))]);
disp(['max : ', num2str(max(Fe))]);

% résultats :
% min : 1.0288e-07
% max : 2.338e-06
