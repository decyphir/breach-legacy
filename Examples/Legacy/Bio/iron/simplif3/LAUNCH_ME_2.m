%
% suite de commande à lancer pour calculer la sensibilité locale de
% okglobal


CreateSystem; % define Sys
define_param_sets; % define P0 and params
[~,~,~,~,~,okglobal_v2] = define_constraints();

nb_params = numel(params);
p = zeros(1,nb_params);
val = zeros(1,nb_params);
vald = zeros(1,nb_params);
fprintf('Lancement de STL_SEvalDiff\n\n');
for ii=1:nb_params
    [p(ii),val(ii),vald(ii)] = STL_SEvalDiff(Sys,okglobal_v2,P0,0:600:48*3600,params{ii},0);
end

fprintf('Lancement de plot_histo\n\n');
plot_histo(vald, P0, 1,[], FindParam(params));

