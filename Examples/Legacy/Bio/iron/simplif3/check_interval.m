function [inside] = check_interval(P0,Pall,VERBOSE)
%
% [inside] = check_interval(P0,Pall,VERBOSE)
%
% check that P0.pts-P0.epsi > Pall.pts - Pall.epsi
% and P0.pts+P0.epsi < Pall.pts + Pall.epsi
% return 0 if there exist a parameter in P0 such parameter
% space is not is the corresponding parameter space in Pall.
% The list of incertain parameter in P0 and Pall must be the same.
%VERBOSE (optionnal) is 1 or 0 (0 by default)

if(nargin==2)
	VERBOSE = 0;  % 0 or 1
end


inside = 1;

if(numel(Pall.dim) ~= numel(P0.dim))
	if(VERBOSE==1)
		disp 'Uncertain parameters not matching'
	end
	inside = 0;
	return;
end

%we get value and epsilon of incertain parameter for pall
for i=1:numel(Pall.dim)
	id_name = Pall.ParamList{Pall.dim(i)};
    eval( [id_name,'_Pall_pts= GetParam(Pall,''' id_name ''');'] );
	eval( [id_name '_Pall_epsi= GetEpsi(Pall,''' id_name ''');'] );
end

%idem for P0
for i=1:numel(P0.dim)
	id_name = P0.ParamList{P0.dim(i)};
    eval( [id_name,'_P0_pts= GetParam(P0,''' id_name ''');'] );
	eval( [id_name '_P0_epsi= GetEpsi(P0,''' id_name ''');'] );
end

for i=1:numel(Pall.dim)
	id_name = Pall.ParamList{Pall.dim(i)};
	eval( ['bad = find(',id_name,'_P0_pts + ',id_name,'_P0_epsi > ',...
			id_name, '_Pall_pts + ',id_name,'_Pall_epsi);']);
	if ~isempty(bad)
		inside = 0;
		if(VERBOSE==1)
			disp([id_name,' : point(s) of index ',num2str(bad), 'are too high']);
			eval([id_name,'_P0_pts']);
			eval([id_name,'_P0_epsi']);
			eval([id_name,'_Pall_pts']);
			eval([id_name,'_Pall_epsi']);
		end
		if(VERBOSE==0)
			return;
		end
	end
	eval( ['bad = find(',id_name,'_P0_pts - ',id_name,'_P0_epsi < ',...
			id_name, '_Pall_pts - ',id_name,'_Pall_epsi);']);
	if ~isempty(bad)
		inside = 0;
		if(VERBOSE==1)
			disp([id_name,' : point(s) of index ',num2str(bad), ' are too low']);
			eval([id_name,'_P0_pts']);
			eval([id_name,'_P0_epsi']);
			eval([id_name,'_Pall_pts']);
			eval([id_name,'_Pall_epsi']);
		end
		if(VERBOSE==0)
			return;
		end
	end
end

%all parameter of P0 are within Pall, it is OK.

end
