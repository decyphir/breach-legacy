
%file from AD

define_param_sets; % define Punc, uncertain_params and other_ranges
define_constraints;

%phi = STL_Formula( 'phi', 'Fe[t] > 0' );

opt.tspan = 0:600:20*3600;
opt.tprop = 3600;
opt.params = uncertain_params; 
opt.lbound = other_ranges(:,1)';
opt.ubound = other_ranges(:,2)';
opt.p = 4;
opt.r = 10; 
opt.plot = 1; 

Sys.init_fun = @init_to_steady;

%[mu, mustar, sigm] = SPropSensi(Sys, Punc, phi,  opt);

opt.tprop = 0;
[mu, mustar, sigm] = SPropSensi(Sys, Punc, okglobal,  opt);
