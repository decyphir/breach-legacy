% Define outer parameter space for the simplif3 system
%
% Here, only 19 over 20 parameters are defined. The last parameter is Tf_sat and is represented by a variable

known_ranges = ...
[ ...
  1e-13    9.6e-6   ; ... %  k_FPN1a_deg     half life FPN1a
  1e-13    2e-13    ; ... %  k_TfR1_prod     prod rate of TfR1
  1.28e-5  1.6e-5   ; ... %  k_IRP_deg
  2e-5     3e-5     ; ... %  k_TfR1_deg
  4.2e-5   14.4e-5  ; ... %  k_IRP_TfR1
  2e-2     3.9e-2   ; ... %  k_Fe_input
  0        4500     ; ... %  n_Ft
];

known_params = {... 
                'k_FPN1a_deg'...
                'k_TfR1_prod'...
                'k_IRP_deg'...
                'k_TfR1_deg'...
                'k_IRP_TfR1'...
                'k_Fe_input'...
                'n_Ft'...
               };               

other_ranges = ...
[ ...
%
%  Initial concentrations % now ENFORCED BY OTHER PARAMETERS USING
%  init_to_steady function 
%
% 3.5e-8   2e-6       ; ...  %  Fe 
% 3e-9     10.7e-9    ; ...  %  IRP
% 1e-8     8.7e-8     ; ...  %  TfR1
% 1.0e-13  1.0e-5     ; ...  %  Ft            
% 1.0e-13  1.0e-5     ; ...  %  FPN1a       
%
%  Thresholds
% 
 1.0e-13  1.0e-5     ; ...  %  theta_IRP_FPN1a      
 1.0e-13  1.0e-5     ; ...  %  theta_Fe_IRP 
 1.0e-13  1.0e-5     ; ...  %  theta_IRP_Ft     
% 
% others
%
 1.0e-18  1.0e-10    ; ...  %  k_Ft_prod
 3.84e-14 1e-10      ; ...  %  k_IRP_prod
 1.0e-18        3.39e-4    ; ...  %  k_Fe_cons    MODIFIED to avoid 0 
 1.0e-18        1.0e5      ; ...  %  k_Fe_export  MODIFIED to avoid 0
 1.0e-18  9.6e-11    ; ...  %  k_FPN1a_prod
 1.0e-9   3.33e-2    ; ...  %  k_Fe_IRP
 % 1.0e-19  1.0e-10    ; ...  %  k_IRP_Ft    -> ENFORCED to be  0.975*k_Ft_prod 
 1.0e-19  1.0e-10    ; ...  %  k_IRP_FPN1a      
 1.0e-9   1.0e-2     ; ...  %  k_Ft_deg
];

uncertain_params = { ...
 'theta_IRP_FPN1a'...
 'theta_Fe_IRP'...                   
 'theta_IRP_Ft'...                   
 'k_Ft_prod'...                      
 'k_IRP_prod'...                     
 'k_Fe_cons'...                      
 'k_Fe_export'...                    
 'k_FPN1a_prod'...                   
 'k_Fe_IRP'...                       
 'k_IRP_FPN1a'...                   
 'k_Ft_deg'...                       
 };

ranges = [known_ranges ; other_ranges];
params = [known_params, uncertain_params];

CreateSystem;
Pall = CreateParamSet(Sys, params, ranges);
P0 = CreateParamSet(Sys, params);
Pok = CreateParamSet(Sys, uncertain_params);


Psearch = CreateParamSet(Sys, uncertain_params, other_ranges);

Pboth = SConcat(Psearch, Pok);

Psave(Sys, 'Pall',Pall);
Psave(Sys, 'P0',P0);

Psave(Sys, 'Psearch');
Psave(Sys, 'Pok');
Psave(Sys, 'Pboth');
