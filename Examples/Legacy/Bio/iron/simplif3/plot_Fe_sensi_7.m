
% compute global sensitivity of okglobal to the parameters in a subset of
% Pall centered around P0. The aim of this script is to verify the output
% of SPropSensi

% Il y a au plus un ordre de grandeur entre les bornes inf et les bornes
% sup. Nous décrivons ici un espace contenant le point solution trouvé.
% Nous espérons ainsi avoir que très peu de jeu de paramètres déclanchant
% la sécurité lors du calcul des trajectoires.

CreateSystem;

known_ranges = ...
    [ ...
    1e-6     9.6e-6   ; ... %  k_FPN1a_deg     half life FPN1a
    1e-13    2e-13    ; ... %  k_TfR1_prod     prod rate of TfR1
    1.28e-5  1.6e-5   ; ... %  k_IRP_deg
    2e-5     3e-5     ; ... %  k_TfR1_deg
    4.2e-5   14.4e-5  ; ... %  k_IRP_TfR1
    2e-2     3.9e-2   ; ... %  k_Fe_input
    100      1000     ; ... %  n_Ft
    ];

known_params = {...
    'k_FPN1a_deg'...
    'k_TfR1_prod'...
    'k_IRP_deg'...
    'k_TfR1_deg'...
    'k_IRP_TfR1'...
    'k_Fe_input'...
    'n_Ft'...
    };

other_ranges = ...
    [ ...
    1.0e-8   1.0e-7     ; ...  %  theta_IRP_FPN1a
    1.0e-7   1.0e-6     ; ...  %  theta_Fe_IRP
    1.0e-8   1.0e-7     ; ...  %  theta_IRP_Ft
    1.0e-11  1.0e-10    ; ...  %  k_Ft_prod
    1.0e-12  1e-11      ; ...  %  k_IRP_prod
    1.0e-4   3.39e-4    ; ...  %  k_Fe_cons
    100      1000       ; ...  %  k_Fe_export
    1.0e-12  1.0e-11    ; ...  %  k_FPN1a_prod
    5.0e-4   5.0e-3     ; ...  %  k_Fe_IRP
    1.0e-13  1.0e-12    ; ...  %  k_IRP_FPN1a
    1.0e-3   1.0e-2     ; ...  %  k_Ft_deg
    ];

uncertain_params = { ...
 'theta_IRP_FPN1a'...
 'theta_Fe_IRP'...
 'theta_IRP_Ft'...
 'k_Ft_prod'...
 'k_IRP_prod'...
 'k_Fe_cons'...
 'k_Fe_export'...
 'k_FPN1a_prod'...
 'k_Fe_IRP'...
 'k_IRP_FPN1a'...
 'k_Ft_deg'...
 };

ranges = [known_ranges ; other_ranges];
params = [known_params, uncertain_params];

P0 = CreateParamSet(Sys,params);

[~,~,okbehavior,okglobal] = define_constraints(); % define okglobal

opts.tspan = 0:600:48*3600;
opts.params = params;
opts.tprop = 0;
opts.lbound = ranges(:,1)';
opts.ubound = ranges(:,2)';
opts.r = 50; % 200
opts.p = 4; % 8
opts.plot = 1;
%opts.muGraphOpt = {'XScale','log'}; % aussi possible 'XLim',[0 1]
%opts.muGraphOpt = {'XLim',[-1 2]};
%opts.mustarGraphOpt = {'XScale','log'};
%opts.sigmGraphOpt = {'XScale','log'};

ParamScales = Sys.p;
ParamScales(1:Sys.DimX-1) = 1e-8;
ParamScales(1:Sys.DimX) = 1; % TFsat


FSAoptions = CVodeSetFSAOptions('SensErrControl', 'off',...
                                'ParamField', 'p',...                                
                                'ParamScales',ParamScales );
Sys.CVodesSensiOptions.method = 'Simultaneous';
Sys.CVodesSensiOptions.FSAoptions = FSAoptions;

%[mu,mustar,sigm] = SPropSensiPerso(Sys,P0,okglobal,opts); % all
%trajectories are always keept, we don't need the personal version of
%SPropSensi, so we will use the standart version :
[mu,mustar,sigm] = SPropSensi(Sys,P0,okglobal,opts);







[~,imu] = sort(mu,'descend');
%imu = [15,1,14]; %k_FPN1a_prod, k_FPN1a_deg, k_Fe_export

fprintf('generation d''un param set dont les parametres sont %s, %s et %s\n',...
        params{imu(1)},params{imu(2)},params{imu(3)});

NN = 3;
Psensible = CreateParamSet(Sys,params(imu(1:3)),ranges(imu(1:3),:),NN); % generate 27 param sets
Psensible = ComputeTraj(Sys,Psensible,opts.tspan);

[~,val] = SEvalProp(Sys,Psensible,okglobal,0);
val = reshape(val,[NN,NN,NN]);

% les indices des val ont été trouvés empiriquement
fprintf('\nvaleur de vérité quand %s augmente:\n',params{imu(1)});
fprintf('%- 13.3e, %- 13.3e, %- 13.3e\n',val(:,:,1),val(:,:,2),val(:,:,3));
fprintf('\nvaleur de vérité quand %s augmente:\n',params{imu(2)});
fprintf('%- 13.3e, %- 13.3e, %- 13.3e\n',val(1,:,:),val(2,:,:),val(3,:,:));
fprintf('\nvaleur de vérité quand %s augmente:\n',params{imu(3)});
fprintf('%- 13.3e, %- 13.3e, %- 13.3e\n',val(:,1,:),val(:,2,:),val(:,3,:));


