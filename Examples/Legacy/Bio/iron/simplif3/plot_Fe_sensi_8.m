
% This script aims at computing the local sensitivity of okglobal to the
% parameters in P0.

CreateSystem;

define_param_sets; % define P0, params and ranges
[~,~,~,okglobal] = define_constraints(); % define okglobal

P0 = ComputeTraj(Sys,P0,0:600:48*3600);
[P0,val] = SEvalProp(Sys,P0,okglobal,0); % not sure this line is needed

[~,mu] = STL_Culprit(Sys, okglobal, P0, 0);


% dphi = DiffFormula(mu{1},params{1}); % params(1) est k_FPN1a_deg

dphi = cell(1,numel(params));
for ii = 1:numel(params)
    dphi{ii} = DiffFormula(mu{1},params{ii});
end

