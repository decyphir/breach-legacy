
% compute the (roughtly global) sentivity of the system to the parameter in
% Psol_all. For this purpose, we compute 10 random trajectories in Psol_all
% and average the sensitivity

define_solution; % define Psol_10

iX = FindParam(Psol_10, {'Fe','Ft','FPN1a','IRP','TfR1'});
iP = Psol_10.dim;

opts.args.iX = iX;
opts.args.iP = Psol_10.dim;
opts.props = {};   
opts.args.tspan = 3*3600:600:20*3600;
opts.open_dialog = 0;
opts.plot_histo = 0; 

% try tuning sensi param 

ParamScales = Sys.p;
ParamScales(1:Sys.DimX-1) = 1e-8;
ParamScales(1:Sys.DimX) = 1; % TFsat


FSAoptions = CVodeSetFSAOptions('SensErrControl', 'off',...
                                'ParamField', 'p',...                                
                                'ParamScales',ParamScales );
Sys.CVodesSensiOptions.method = 'Simultaneous';
Sys.CVodesSensiOptions.FSAoptions = FSAoptions;

[M, opts] = SplotSensiBar(Sys, Psol_10,[], opts);

plot_histo(M, Psol_10, iX,[], iP);
%fig_resize(gcf, 1.5, 1);
