
%
% script used to find a valid hyper-rectangle for okglobal_v2
% The result is published (at least I hope) in Information and Computation)
%

define_param_sets; % defines Pall, P0 et params
[~,~,~,~,~,okglobal_v2] = define_constraints;


for ii=1:numel(P0.ParamList)
    eval( [P0.ParamList{ii} '= GetParam(P0,''' P0.ParamList{ii} ''');']);
end


% %%%
% Etape 1 :
% Valeur maximale des epsi pour rester dans l'espace de recherche
% %%%


% Manually found using the formula:
%
%scal_max = 10 * min(  (Pall.pts+Pall.epsi)/P0.pts - 1  ,  1 - (Pall.pts-Pall.epsi)/P0.pts  )
%
scal_max = [...     % compris entre 0 et 10
  9.2 ; ...  % 'k_FPN1a_deg'
  1.76; ...  % 'k_TfR1_prod'
  0.85; ...  % 'k_IRP_deg'
  1.66; ...  % 'k_TfR1_deg'
  2.85; ...  % 'k_IRP_TfR1'
  3   ; ...  % 'k_Fe_input'
  9.99; ...  % 'n_Ft'
  9.99; ...  % 'theta_IRP_FPN1a'
  9.99; ...  % 'theta_Fe_IRP'
  9.99; ...  % 'theta_IRP_Ft'
  4.28; ...  % 'k_Ft_prod'
  9.95; ...  % 'k_IRP_prod'
  1.3 ; ...  % 'k_Fe_cons'
  9.99; ...  % 'k_Fe_export'
  9.99; ...  % 'k_FPN1a_prod'
  9.99; ...  % 'k_Fe_IRP'
  9.99; ...  % 'k_IRP_FPN1a'
  9.99; ...  % 'k_Ft_deg'
];
 


% %%%
% Etape 2 :
% Valeur maximale des epsi pour avoir des extremum vérifiant okglobal_v2
% %%%

% Code utilisé pour obtenir scal_max_2

% nbParam = numel(params);
% scal_max_2 = zeros(1,nbParam);
% for ii=1:nbParam
%     Ptmp = SDelUncertainParam(P0,params,params{ii},1);
%     idx_param = FindParam(P0,params{ii});
%     paramValue=P0.pts(idx_param,1);
%     Ptmp = Refine(Ptmp,2);
%     Ptmp.epsi = [0,0];
%     
%     valid = true;
%     step = 0.01;
%     scalTmp = step;
%     while valid
%         Ptmp.pts(idx_param,:) = [paramValue*(1-scalTmp/10),paramValue*(1+scalTmp/10)];
%         Ptmp = ComputeTraj(Sys,Ptmp,0:600:48*3600);
%         [~,val] = SEvalProp(Sys,Ptmp,okglobal_v2,0);
%         if(any(val<0) || scalTmp+step>scal_max(ii))
%             valid = false;
%             fprintf('##########\nmaximum for parameter %s found : %f\n##########\n',params{ii},scalTmp);
%         else
%             fprintf('Param %s (%d/%d): scalTmp = %g valid\n',params{ii},ii,nbParam,scalTmp);
%             scalTmp = scalTmp+step;
%         end
%     end
%     scal_max_2(ii) = scalTmp;
% end

% Résultat obtenu:

scal_max_2 = [...    % compris entre 0 et 10
    5.01; ...  % 'k_FPN1a_deg'
    1.75; ...  % 'k_TfR1_prod'
    0.84; ...  % 'k_IRP_deg'
    1.65; ...  % 'k_TfR1_deg'
    2.85; ...  % 'k_IRP_TfR1'
    3.00; ...  % 'k_Fe_input'
    3.87; ...  % 'n_Ft'
    9.99; ...  % 'theta_IRP_FPN1a'
    2.73; ...  % 'theta_Fe_IRP'
    9.99; ...  % 'theta_IRP_Ft'
    3.87; ...  % 'k_Ft_prod'
    3.57; ...  % 'k_IRP_prod'
    1.29; ...  % 'k_Fe_cons'
    7.79; ...  % 'k_Fe_export'
    7.73; ...  % 'k_FPN1a_prod'
    2.67; ...  % 'k_Fe_IRP'
    9.99; ...  % 'k_IRP_FPN1a'
    2.79; ...  % 'k_Ft_deg'
];



% %%%
% Etape 3.1 :
% On trouve un hyper-rectangle valide proportionnel à max_scal_2/max_scal
% (on se sert de ce ration comme une sensibilité)
% %%%

% tic
% N = 1000;
% sensi = scal_max_2./scal_max;  % inférieur à 1
% Ptmp = P0;
% valid = true;
% step = 0.02;
% scaleTmp = step; % ratio between 0 and 1
% while valid
%      % la ligne suivante fonctionne car params est dans le même ordre que P0.ParamList{P0.dim}
%     Ptmp.epsi = P0.epsi.*(scaleTmp*sensi.*scal_max_2);
%     Pr = QuasiRefine(Ptmp,N);
%     Pf = ComputeTraj(Sys, Pr, 0:600:48*3600, 0); % change name to ensure we recompute traj every times
%     [Pf, val] = SEvalProp(Sys, Pf, okglobal_v2, 0);
%     fprintf('%d/%d traj satisfy okglobal_v2\n',numel(val(val>0)),N);
%     if(any(val<0) || scaleTmp+step>1)
%         fprintf('##########\nmaximum scale found : %g\n##########\n',scaleTmp);
%         valid = false;
%     else
%         fprintf('scaleTmp %g valide\n',scaleTmp);
%         scaleTmp = scaleTmp+step;
%     end
% end
% toc


% %%%
% 11 mars 2013
% Résultat obtenu: scalTmp max en menant qu'à des simu valides = 0.32
% Temps de calcul: 2392.226161 seconds.


% %%%
% Etape 3.2 :
% Alternative : On trouve un hyper-rectangle valide en tirant de chaque
% coté tant que ça veut bien
% %%%

% tic
% N = 500;
% %sensi = scal_max_2./scal_max;
% nbParam = numel(params);
% scale = zeros(nbParam,1); % 0.1 x le pourcentage par lequel on étire les paramètres
% %scale = ones(nbParam,1)*0.6; % j'ai interrompu les calculs alors que tous les scales était à 0.6
% valid = ones(nbParam,1); % vaut 1 si le param d'indice ii peut être étiré
% Ptmp = P0;
% step = 0.2;
% ii = 0;
% while any(valid)
%     ii = 1+mod(ii,nbParam); % on passe au paramètre suivant
%     if ~valid(ii) % s'il n'est pas étirable, on zappe
%         continue ;
%     end
%     if(scale(ii)+step>=scal_max_2(ii)) % s'il est étiré au max, on ne l'étire plus
%        valid(ii) = 0;
%        fprintf('Le paramètre %s ne peut plus augmenter. Valeur : %g ; %d/%d restants\n',...
%                params{ii},scale(ii),numel(valid(valid==1)),nbParam);
%        continue;
%     end
%     scale(ii) = scale(ii) + step;
%     Ptmp.epsi = P0.epsi.*scale;
%     Pr = QuasiRefine(Ptmp,N);
%     Pf = ComputeTraj(Sys, Pr, 0:600:48*3600, 0); % change name to ensure we recompute traj every times
%     [Pf, val] = SEvalProp(Sys, Pf, okglobal_v2, 0);
%     fprintf('%d/%d traj satisfy okglobal_v2\n',numel(val(val>0)),N);
%     if any(val<0)
%         scale(ii) = scale(ii) - step;
%         valid(ii) = 0;
%         fprintf('Le paramètre %s a atteint son maximum : %g ; %d/%d restants\n',...
%                 params{ii},scale(ii),numel(valid(valid==1)),nbParam);
%     else
%         fprintf('Paramètre %s étiré à %g ; %d/%d paramètres étirables\n',...
%                 params{ii},scale(ii),numel(valid(valid==1)),nbParam);
%     end
% end
% toc

% %%%
% 11 mars 2013
% scale :
%  1.0
%  1.6
%  0.8
%  1.6
%  1.0
%  2.8
%  1.0
%  1.0
%  1.0
%  1.8
%  1.0
%  1.6
%  0.8
%  1.0
%  1.0
%  1.6
%  4.4
%  0.8
% résultat obtenu en 6533.734199 seconds. + le temps d'amener tous les
% paramètres à 0.6


% %%%
% Etape 4.1 :
% On vérifie que l'hyper-rectangle trouvé avec 3.1 est bien valide
% %%%

% tic
% N = 10000;
% sensi = scal_max_2./scal_max;
% Ptmp = P0;
% Ptmp.epsi = P0.epsi.*(0.32*sensi.*scal_max_2);
% Pr = QuasiRefine(Ptmp,N);
% Pf = ComputeTraj(Sys, Pr, 0:600:48*3600, 0);
% 
% %SplotVar(Pf, 1:5) % a utiliser pour visualiser les trajectoires avec un N faible (max 100)
% 
% [Pf, val] = SEvalProp(Sys, Pf, okglobal_v2,0);
% fprintf([num2str(numel(val(val>0))) '/' num2str(N) ' traj satisfy okglobal_v2\n']);
% toc
%

% %%%
% 11 mars 2013
% nous n'avons pas trouvé de jeu de paramètre non-valide
% durée de calcul : 1640.282507 seconds.


% %%%
% Etape 4.2 :
% On vérifie que l'hyper-rectangle trouvé avec 3.2 est bien valide
% %%%

tic
N = 100000;
Ptmp = P0;
%scale = [1.0, 1.6, 0.8, 1.6, 1.0, 2.8, 1.0, 1.0, 1.0, 1.8, 1.0, 1.6, 0.8, 1.0, 1.0, 1.6, 4.4, 0.8]';
%scale = [0.9, 1.5, 0.7, 1.5, 0.9, 2.7, 0.9, 0.9, 0.9, 1.7, 0.9, 1.5, 0.7, 0.9, 0.9, 1.5, 4.3, 0.7]';
%scale = [0.8, 1.4, 0.6, 1.4, 0.8, 2.6, 0.8, 0.8, 0.8, 1.6, 0.8, 1.4, 0.6, 0.8, 0.8, 1.4, 4.2, 0.6]';
%scale = [0.8, 1.4, 0.6, 1.4, 0.8, 2.2, 0.8, 0.8, 0.6, 1.6, 0.8, 1.4, 0.6, 0.8, 0.8, 1.4, 4.2, 0.6]';
 scale = [0.8, 1.4, 0.6, 1.2, 0.8, 2.0, 0.8, 0.8, 0.6, 1.6, 0.8, 1.4, 0.6, 0.8, 0.8, 1.4, 4.2, 0.6]';
Ptmp.epsi = P0.epsi.*scale;
Pr = QuasiRefine(Ptmp,N);
Pf = ComputeTraj(Sys, Pr, 0:600:48*3600, 0);

%SplotVar(Pf, 1:5) % a utiliser pour visualiser les trajectoires avec un N faible (max 100)

[Pf, val] = SEvalProp(Sys, Pf, okglobal_v2,0);
fprintf([num2str(numel(val(val>0))) '/' num2str(N) ' traj satisfy okglobal_v2\n']);
toc

% %%%
% 11 mars 2013
% en utilisant les résultats trouvés, nous obtenons qu'il y a seulement
% 9962 sur 10000 jeux de paramètres valides
% durée de calcul : 1420.406534 seconds.
%
% %%%
%
% en utilisant les résultats trouvés moins 0.1 pour chaque paramètres, nous
% trouvons qu'il y a 8 jeux de paramètres non valides
% durée de calcul :  1417.344057 seconds.
%
% %%%
%
% en utilisant les résultats trouvés moins 0.2 pour chaque paramètres, nous
% n'obtenons que des jeux de paramètres valides.
% durée du calcul : 1468.500277 seconds.
% Ce jeu de paramètres est plus large que celui trouvé en 3.1 (la moyenne
% des pourcentages est plus élevée)
%
% %%%
% 12 mars 2013
% en utilisant les résultats trouvés moins 0.2 pour chaque paramètres, nous
% obtenons 12 jeux de paramètres non-valides sur les 100'000 simulations.
% Les contraintes violées sont phi_B2 et phi_B3.
% Elapsed time is 13619.861229 seconds.
%
% %%%
%
% en diminant les intervalles pour k_Fe_input et theta_Fe_IRP (nous avons
% choisi ces paramètre en regardant, dans Breach, quels était les
% paramètres les plus aux bord des intervalles) à 2.2 et 0.6, nous obtenons
% deux simulations ne respectant pas phi_B3.
% Elapsed time is 14362.957064 seconds.
%
% %%%
% 13 mars 2013
% en diminant les intervalles pour k_Fe_input et k_TfR1_deg de 0.2 chacun
% (choix de ces paramètres identique que précédemment), nous n'obtenons que
% des jeux de paramètres valides.
% Elapsed time is 14279.243630 seconds.
%




