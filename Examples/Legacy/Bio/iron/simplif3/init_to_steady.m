function P = init_to_steady(P)
%
% set initial conditions to the analytic value of steady state in P
%
% AD: - added checks to bound the values of initial concentrations
% (probably kills the purpose of starting at steady though... TO BE
% CHECKED)
% NM : -surely kills the steady state. The properties phi_s6 to phi_s10
% check that
% 
%

for i=1:numel(P.ParamList)
    eval([P.ParamList{i} '= GetParam(P,''' P.ParamList{i} ''');']);
end


% %%%
% Swap k_IRP_FPN1a and k_FPN1a_prod if needed
% %%%

i = find(k_FPN1a_prod<k_IRP_FPN1a);
tmp = k_FPN1a_prod(i);
k_FPN1a_prod(i)= k_IRP_FPN1a(i);
k_IRP_FPN1a(i) = tmp;


% %%%
% Define k_IRP_Ft
% %%%

k_IRP_Ft = 0.975*k_Ft_prod;


% %%%
% Define initial conditions
% %%%

Fe = (((k_IRP_deg+k_Fe_IRP).*k_FPN1a_deg.*k_Fe_input.*k_TfR1_prod+ ...
    k_FPN1a_deg.*k_Fe_input.*k_IRP_TfR1.*k_IRP_prod).*Tf_sat0)  ./  (((k_FPN1a_prod.*k_Fe_export+k_FPN1a_deg.*k_Fe_cons).*k_IRP_deg+k_FPN1a_prod.*k_Fe_IRP.*k_Fe_export+k_FPN1a_deg.*k_Fe_IRP.*k_Fe_cons).*k_TfR1_deg);

Ft = k_Ft_prod./k_Ft_deg;

FPN1a = k_FPN1a_prod./k_FPN1a_deg;

IRP = k_IRP_prod./(k_IRP_deg+k_Fe_IRP);

TfR1 = (((k_IRP_deg+k_Fe_IRP).*k_TfR1_prod)+k_IRP_TfR1.*k_IRP_prod)./ ...
    ((k_IRP_deg+k_Fe_IRP).*k_TfR1_deg);


% %%%
% Change threshold if needed
% %%%

% Nota : we approximate the 20-square root of 99 (= 99^(1/20) ) by 1.258

theta_IRP_FPN1a = max(theta_IRP_FPN1a,1.258*IRP); % ensure that sig(IRP,theta_IRP_FPN1a) > 0.99 with a steep factor=20
theta_IRP_Ft = max(theta_IRP_Ft,1.258*IRP); % ensure that sig(IRP,theta_IRP_Ft) > 0.99 with a steep factor=20
theta_Fe_IRP = min(theta_Fe_IRP,Fe/1.258); % ensure that sig(IRP,theta_IRP_Ft) < 0.01 with a steep factor=20



% %%%
% Save the changes
% %%%

P = SetParam(P, 'Fe', Fe);
P = SetParam(P, 'Ft', Ft);
P = SetParam(P, 'FPN1a', FPN1a);
P = SetParam(P, 'IRP', IRP);
P = SetParam(P, 'TfR1', TfR1);
P = SetParam(P, 'k_IRP_Ft', k_IRP_Ft);
P = SetParam(P, 'k_FPN1a_prod',k_FPN1a_prod);
P = SetParam(P, 'k_IRP_FPN1a',k_IRP_FPN1a);
P = SetParam(P, 'theta_IRP_FPN1a',theta_IRP_FPN1a);
P = SetParam(P, 'theta_IRP_Ft',theta_IRP_Ft);
P = SetParam(P, 'theta_Fe_IRP',theta_Fe_IRP);

end
