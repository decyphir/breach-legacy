%

define_param_sets;
iX = FindParam(P0, {'Fe','Ft','FPN1a','IRP','TfR1'});
iP = P0.dim;

opts.args.iX = iX;
opts.args.iP = P0.dim;
opts.props = {};
opts.args.tspan = 3*3600:600:20*3600;
opts.open_dialog = 0;
opts.plot_histo = 0;

% try tuning sensi param 

ParamScales = Sys.p;
ParamScales(1:Sys.DimX-1) = 1e-9;
ParamScales(Sys.DimX) = 1; % TFsat


FSAoptions = CVodeSetFSAOptions('SensErrControl', 'off',...
                                'ParamField', 'p',...
                                'ParamScales',ParamScales);
Sys.CVodesSensiOptions.method = 'Simultaneous';
Sys.CVodesSensiOptions.FSAoptions = FSAoptions;

[M, opts] = SplotSensiLog(Sys, P0, [], opts);

plot_histo(M, P0, iX, [], iP);
%fig_resize(gcf, 1.5, 1);
