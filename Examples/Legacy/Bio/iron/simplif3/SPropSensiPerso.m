function [mu, mustar, sigma, Pr] = SPropSensiPerso(Sys, P, phi, opt)
% SPROPSENSI estimates the sensitivity of a property wrt some parameters
% (Morris method). This function is adapted from the strategy described in
% "Global Sensitivity Analysis, A Primer", Saltelli et al, p113++
%
%
% ----
% Cette fonction supprime du calcul les jeux de paramètres déclanchant le
% mécanisme de gel. Nous faisons ceci afin d'éviter que des trajectoires
% ne correspondant pas à une simulation valide soit utilisées pour le
% calcul de sensibilité.
%
% Cette fonction est spécifique au modèle du fer. Le système doit inclure
% les variables Fe Ft et IRP.
% ----
%
% Synopsis: [mu, mustar, sigma] = SPropSensi(Sys, P, phi, opt)
%
% Input:
%    - P  is a parameter set for Sys. The value of all parameters of P, not
%         in opt.params is defined by the first parameters values in P.
%    - phi is an STL property
%    - opt is an option structure with the following fields :
%
%        - tspan       time domain computation of the trajectories
%        - tprop       time instant (scalar) when to eval prop satisfaction
%                      (default tspan(1))
%        - params      variable parameters
%        - lbound      lower bounds for the search domain
%        - ubound      upper bounds for the search domain
%        - p           number of levels (p-grid). Recommended to be even
%                      (default 4)
%        - r           number of trajectories (default 10)
%        - k           OBSOLETE ; REPLACED BY r
%        - plot        if 1, plots histograms (default 0)
%        - muGraphOpt      if plot=1, define graphical options for mu graph
%                          (optional)
%        - mustarGraphOpt  if plot=1, graphical options for mu* graph
%                          (optional)
%        - sigmaGraphOpt   if plot=1, graphical options for sigma graph
%                          (optional)
%        - logPlot         
%
% Output:
%   - mu      expectation of elementary effects
%   - mustar  expectation of absolute values of elementary effects
%   - sigma   variance of elementary effects
%
% Example (Lorentz84):
%   CreateSystem;
%   P = CreateParamSet(Sys);
%   phi = STL_Formula('phi','ev (x1[t] > 3)');
%   % P = SetParam(P, {'x1h', 'x1l', 'T'}, [.3, -.3, 5]);
%   % oscil_prop_names = STL_ReadFile('oscil_prop.stl');
%   % phi = STL_Formula(oscil_prop_names{end});
%   opt.tspan = 2:0.1:5;
%   opt.params = {'a','F'};
%   opt.lbound = [0.15, 5];
%   opt.ubound = [0.35, 25];
%   opt.plot = 1;
%   opt.muGraphOpt = {'XScale','log'};
%   [mu, mustar, sigma] = SPropSensi(Sys, P, phi, opt);
%
%See also STL_Formula CreateParamSet
%


if isfield(opt, 'tspan')
    tspan = opt.tspan;
elseif isfield(P, 'traj')
    tspan = P.traj(1).time;
elseif isfield(Sys, 'tspan')
    tspan = Sys.tspan;
else
    tspan = 0:.2:10;
end

if isfield(opt, 'tprop')
    tprop = opt.tprop;
else
    tprop = tspan(1);
end

if isfield(opt, 'p')
    p = opt.p;
else
    p = 4;
    opt.p = 4;
end

if ~isfield(opt, 'r')
    if isfield(opt,'k')
        opt.r = opt.k;
    else
        opt.r = 10;
    end
end

Sys.p = P.pts(:,1);
Pr = CreateParamSet(Sys, opt.params, [opt.lbound' opt.ubound']);

Pr = pRefine(Pr, opt.p, opt.r);

%NM : we must compute the truth value of phi at time=tprop
if tprop < tspan(1)
    tspan = [tprop,tspan];
%elseif tprop > tspan(end)
%    tspan = [tspan, tprop];
end

Pr = ComputeTraj(Sys, Pr, tspan);


% ---- AJOUT LOCAL

% nous regardons que Fe, Ft et IRP
phi_bloque = STL_Formula('phi_bloque',...
        'ev (alw (abs(ddt{Fe}[t])+abs(ddt{Ft}[t])+abs(ddt{IRP}[t]) <1.0e-16 ))');
[~,val_bloque] = SEvalProp(Sys,Pr,phi_bloque,0);
idx_bad = find(val_bloque>0); % idx_bad = indexs des param sets déclanchant la sécurité
dim_bloc = numel(Pr.dim)+1; % un bloc = un ensemble de param sets où à chaque fois une seule composante change
idx_bad_bloc_id = floor((idx_bad-1)/dim_bloc); % zero-based
% on garde les trajectoires, telles qu'il n'y en ait aucune dans le bloc qui
% déclanche la sécurité
idx_to_keep = 1:numel(val_bloque);
idx_to_keep = idx_to_keep(~ismember(floor((idx_to_keep-1)/dim_bloc),idx_bad_bloc_id));
Pgood = Sselect(Pr, idx_to_keep);
% on copie D qui est constitué de blocs k x k
dim_bloc = numel(Pr.dim);
idx_to_keep = 1:opt.r*dim_bloc;
idx_to_keep = idx_to_keep(~ismember(floor((idx_to_keep-1)/dim_bloc),idx_bad_bloc_id));
Pgood.D = Pr.D(:,idx_to_keep);
sprintf(' keep %d among %d param sets\n',size(Pgood.pts,2),size(Pr.pts,2))
Pr = Pgood;

opt.r = opt.r-numel(idx_bad_bloc_id); % on a enlevé des blocs de trajectoires

% ---- FIN AJOUT LOCAL


[Pr, Y] = SEvalProp(Sys, Pr, phi, tprop);

[mu, mustar, sigma] = EEffects(Y, Pr.D, p);


if opt.logPlot
    [~,isort] = sort(abs(mu));
    Mu = mu(isort);
    h = figure;
    subplot(2,2,1);
    MuTmp = -Mu;
    MuTmp(Mu>0)=0;
    barh(MuTmp);
    title('Negative Elementary Effects (mu)')
    set(gca, 'YTick', 1:numel(opt.params), 'YTickLabel', opt.params(isort), 'XScale','log');
    
    subplot(2,2,2);
    MuTmp = Mu;
    MuTmp(Mu<0)=0;
    barh(MuTmp);
    title('Positive Elementary Effects (mu)')
    set(gca, 'YTick', 1:numel(opt.params), 'YTickLabel', opt.params(isort), 'XScale','log');

    subplot(2,2,3);
    barh(mustar(isort));
    title('Absolute values of Elementary Effects (mu*)')
    set(gca, 'YTick', 1:numel(opt.params), 'YTickLabel', opt.params(isort), 'XScale','log');
    
    
    subplot(2,2,4);
    barh(sigma(isort));
    title('Standart deviation of Elementary Effects (sigma)')
    set(gca, 'YTick', 1:numel(opt.params), 'YTickLabel', opt.params(isort), 'XScale','log');
      
    fig_resize(h,2,2)
elseif opt.plot
    [~,isort] = sort(abs(mu));
    h = figure;
    subplot(3,1,1);
    barh(mu(isort));
    title('Expectation of elementary effects (mu)')
    set(gca, 'YTick', 1:numel(opt.params), 'YTickLabel', opt.params(isort));
    if isfield(opt,'muGraphOpt')
        set(gca,opt.muGraphOpt{:});
    end
    
    subplot(3,1,2);
    barh(mustar(isort));
    title('Expectation of absolute values of elementary effects (mu*)')
    set(gca, 'YTick', 1:numel(opt.params), 'YTickLabel', opt.params(isort));
    if isfield(opt,'mustarGraphOpt')
        set(gca,opt.mustarGraphOpt{:});
    end
    
    subplot(3,1,3);
    barh(sigma(isort));
    title('Standard deviation of elementary effects (sigma)')
    set(gca, 'YTick', 1:numel(opt.params), 'YTickLabel', opt.params(isort));
    if isfield(opt,'sigmaGraphOpt')
        set(gca,opt.sigmaGraphOpt{:});
    end
    
    fig_resize(h,1,2.5)
end

end
