define_param_sets;

 for i=1:numel(P0.ParamList)
    eval( [P0.ParamList{i} '= GetParam(P0,''' P0.ParamList{i} ''');']);
 end

N = 10;
def = 1;
    

coef_autre=3; %fe_input (max=3)
coef_inf=3.5;
coef_ft=0.9; % Ft, n_Ft
coef_tfr1=2; %tfr1, k_IRP_TfR1 (sert à rien)
coef_irp=1.1; %irp_prod k_Fe_IRP
coef_fe_cons=1; %fe_cons (max=1.3)
coef_fe_export=1; %fpn et fe_export

scal = [...    
def*coef_fe_export;... % 'k_FPN1a_deg'    
def*1.7647;...%*coef_tfr1; ...   % 'k_TfR1_prod' %% max = *1.7647;...%
0.857; ...   % 'k_IRP_deg'      
def*1.666;...%*coef_tfr1; ...   % 'k_TfR1_deg' %% max = *1.666;...%
0.2857; ...   % 'k_IRP_TfR1'     
def*coef_autre; ...  % 'k_Fe_input' %% max = *3;...%
def*coef_ft; ...% 'n_Ft'           
def*coef_inf; ...    % 'theta_IRP_FPN1a'
def*coef_inf; ...    % 'theta_Fe_IRP'   
def*coef_inf; ...    % 'theta_IRP_Ft'   
def*coef_ft; ...% 'k_Ft_prod'      
def*coef_irp; ...   % 'k_IRP_prod'     
def*coef_fe_cons;... % 'k_Fe_cons' %% max = 1.3
def*coef_fe_export; ...% 'k_Fe_export'    
def*coef_fe_export; ...% 'k_FPN1a_prod'   
def*coef_ft; ...% 'k_Ft_deg'       
def*coef_irp; ...  % 'k_Fe_IRP'       
def*coef_inf  ...    % 'k_IRP_FPN1a'       
];

% on sauvegarde le sous-espace obtenu (ie: l'hyper-rectangle solution)
Psol_all = P0;
Psol_all.epsi = P0.epsi.*scal;
Psol_all = init_to_steady(Psol_all);
Psave(Sys,'Psol_all');


% on génère des points aléatoirement
Psol_10 = QuasiRefine(Psol_all,N);

% on initialise
Psol_10 = init_to_steady(Psol_10);


% on calcule les traj
tspan = 0:600:48*3600;
Psol_10 = ComputeTraj(Sys,Psol_10, tspan);
Psave(Sys,'Psol_10');

         
