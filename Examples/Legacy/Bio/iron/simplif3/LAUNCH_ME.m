%LAUNCH_ME suite de commande à lancer (répertoire simplif3)

% %%%%
% suite de commandes utilisées pour vérifier qu'un espace est valide
% tic
% definitionSystem();
% check_P0;
% toc


% %%%%
%suite de commandes lancées pour trouver automatiquement un jeu de
%paramètre valide
CreateSystem;
define_param_sets;  % define params and ranges
[~,~,~,~,ok_behavior_v2,okglobal_v2] = define_constraints();
clear opts;
opts.tspan = 0:600:48*3600;
opts.tau = 0;
opts.lbound = ranges(:,1)';
opts.ubound = ranges(:,2)';
opts.params = params;
opts.MaxIter = 50;
opts.OptimType = 'Max';
opts.OptimizeNInitPoints = 1;
opts.nbSplit = 250;

tic
[Pres1,~] = SOptimPropLog(Sys, oksteady, opts);
[Pres2,~] = SOptimPropLog(Sys, okbehavior_v2, opts);
P = SConcat(Pres1,Pres2);
toc

tic
opts.StopWhenFound = 1;
opts.MaxIter = 100;
[val_res,Pres] = SOptimProp(Sys,P,okglobal_v2,opts);
toc

%
%%%%%
% le 4 mars 2013
% trouvé un jeu de paramètre valide (val_res=1.611956186710388e-13)
% temps première étape = 3540.429886 seconds.
% temps deuxième étape = 237.591836 seconds.
% nombre param set valides pour oksteady = 24
% nombre param set valides pour okbehavior_v2 = 6
%
