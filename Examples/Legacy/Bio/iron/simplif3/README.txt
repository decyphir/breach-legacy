The file definitionSystem.m
 - defines the differential system under Breach.

The file define_constraints.m
 - defines the properties ensuring that all constraints we have on the system are respected.

The file define_param_sets.m
 - defines two parameter sets : Pall which define the space we are looking in, and P0 which contain a point in this space respecting the constraints.

The file define_solution.m
 - defines two parameter sets : Psol_all which define the solution sub-space and Psol_10 which contains 10 trajectoire within this sub-space

The files RecalibrateSampling3_1.m and init_to_steady.m
 - fixe the Initial Condition corresponding to the value at the steady state. The first is writen by NM, the second by AD.

The file plot_nominal.m
 - is a script plotting a curve and saving it in a pdf file.

The file plot_Fe_sensi.m
 - plot the sensibility of the parameter set in P0 to the parameters

The file plot_Fe_sensi_2.m
 - It is a try to plot the average sensitivity of X (usually 10 or 50) trajectories. The point is how to set the options! When trying with the GUI, I obtain a different result than when trying with this script.

The files sensibilité_de_10_courbes_dans_Psol.xxx
 - is an average sensitivity of 10 random trajectories with SensErrControl set to off and a RelTol of 1e-5, computed through the GUI


The file param_set_P0divers contains 3 valid parameter sets. They are not in the sub-space manually found (aka P0)


All the notation used in these files are the notation used in the article, (excepted lower/upper-case for the file simplif3_fer_redondant.rp)
