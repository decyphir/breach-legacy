
% compute global sensitivity of okglobal to the parameters in Pall

CreateSystem;

define_params_sets; % define P0, params and ranges
define_constraints; % define okglobal

opts.tspan = 3*3600:600:20*3600;
opts.params = params;
opts.tprop = 0;
opts.lbound = ranges(:,1)';
opts.ubound = ranges(:,2)';
opts.r = 100;
opts.p = 8;
opts.plot = 1;
%opts.muGraphOpt = {'XScale','log'}; % aussi possible 'XLim',[0 1]
opts.muGraphOpt = {'XLim',[-1 2]};
opts.mustarGraphOpt = {'XScale','log'};
opts.sigmGraphOpt = {'XScale','log'};

ParamScales = Sys.p;
ParamScales(1:Sys.DimX-1) = 1e-8;
ParamScales(1:Sys.DimX) = 1; % TFsat


FSAoptions = CVodeSetFSAOptions('SensErrControl', 'off',...
                                'ParamField', 'p',...                                
                                'ParamScales',ParamScales );
Sys.CVodesSensiOptions.method = 'Simultaneous';
Sys.CVodesSensiOptions.FSAoptions = FSAoptions;

Sys.init_fun = @init_to_steady;

[mu, mustar, sigm]  = SPropSensi(Sys,P0,phi_b1,opts);
