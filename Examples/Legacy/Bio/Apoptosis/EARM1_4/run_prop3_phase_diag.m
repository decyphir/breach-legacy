% Plots phase diagram for property 1 - Fig 2

InitCells;

% load properties

STL_ReadFile('properties.stl');

%% Compute quantitative function for property1
Init_EARM1_4;
tspan = 0:60:6*3600;
cell_line = Phct;
prop= property3;
ngrid = 10;
var1 = 'XIAP';
var2 = 'pC3';
ranges = [0 60000;... % XIAP range
          1e3 1e6]    % pC3  range

P =  compute_phasediagram(EARM1_4, ngrid, prop, tspan, cell_line, ranges, ...
                          var1, var2);

% Plots the corresponding phase diagram

figure; 
tau = 0;
opt.contour= 1;
SplotProp(P, prop, tau, opt); 
