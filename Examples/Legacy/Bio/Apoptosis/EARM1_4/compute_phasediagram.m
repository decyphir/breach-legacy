function Psep = compute_phasediagram(Sys, ngrid, prop, tspan, cell_line, ranges, var1, var2)

%
% script to compute and plot separatrices in the plan flip/casp3
%


Pxc3 = CreateParamSet(Sys, {var1, var2}, ranges);
Pxc3.pts = cell_line.pts; 

i_1 = FindParam(Sys, var1);
i_2  = FindParam(Sys, var2);

Pxc3.pts(i_1,1) = (ranges(1,2)+ranges(1,1)) ./ 2;
Pxc3.pts(i_2,1) = (ranges(2,2)+ranges(2,1)) ./2;


Psep = Refine(Pxc3, ngrid); 
Psep = SFindPropBoundary(Sys, Psep, prop,  tspan, 0);
