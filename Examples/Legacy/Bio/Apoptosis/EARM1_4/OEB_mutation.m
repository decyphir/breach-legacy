function POEB = OEB_mutation(P)
  
  i_bcl2 = FindParam(P, 'Bcl2');
  POEB = SetParam(P, i_bcl2, 10*P.pts(i_bcl2,1));
