function PDELX = DELX_mutation(P)
  
  	i_XIAP = FindParam(P, 'XIAP');
    PDELX = SetParam(P, i_XIAP, 0.*P.pts(i_XIAP,1));
end
	
