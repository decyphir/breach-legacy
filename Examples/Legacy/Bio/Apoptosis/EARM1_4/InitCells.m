% Create parameter sets for type I (HCT 116) cells and type II (SKW)
% based on Table S2 and Box I in Aldridge-Sorger 2011 paper
%
 
try 
  Init_EARM1_4;
catch 
  CreateEARM1_4;
  Init_EARM1_4;
end
    
% Base values for hct116 cells 
L      = 1000;  
R      = 5000; 
flip   = 1E2;
pC8    = 3500;  
Bar    = 1E3;
pC3    = 1E5;
pC6    = 1e4;  
XIAP   = 30000; 
PARP   = 1E6; % aka EC-S
Bid    = 4E4; 
Bcl2c  = 1E3; % aka Mcl-1
Bax    = 8E4; 
Bcl2   = 2E4; 
M      = 5E5; 
CytoCm = 5E5;
Smacm  = 1E5;
Apaf   = 1E5;
pC9    = 1E5;

% indices for all proteins
iprot = FindParam(Sys, {'L'    ,...
                        'R'    ,...
                        'flip' ,...
                        'pC8'  ,...
                        'Bar'  ,...
                        'pC3'  ,...
                        'pC6'  ,...
                        'XIAP' ,...
                        'PARP' ,...
                        'Bid'  ,...
                        'Bcl2c',...
                        'Bax'  ,...
                        'Bcl2' ,...
                        'M'    ,...
                        'CytoCm',...
                        'Smacm',...
                        'Apaf' ,...
                        'pC9'  ,...
                             });
    

%%%%%%%%%%%%% HCT116 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameter set for HCT cells

% indices for varying proteins
ivar = FindParam(Sys,{'XIAP', 'pC3', 'PARP', 'pC8', 'pC6', 'Bax', 'Bcl2c', ...
                       'Bid', 'Smacm', 'flip', 'Bcl2'});
Phct = CreateParamSet(Sys, ivar);

Phct.pts(iprot,:)= [ L ; R  ; flip;  pC8 ;  Bar   ;  pC3   ;  pC6 ;   XIAP  ; ...
                PARP  ;   Bid   ;   Bcl2c ;   Bax   ;   Bcl2  ;   M     ; ...
                  CytoCm;   Smacm ;   Apaf  ;   pC9];

Xbase = [XIAP, pC3, PARP, pC8, pC6, Bax, Bcl2c, ...
                       Bid, Smacm, flip, Bcl2 ]'; 

epsi_hct = Xbase.*[ .12 ; .32 ; .01; .02; .17; .14; .87; .01; .03; .48; .2];
Phct.epsi = epsi_hct;

Phct.time_mult = 1/3600; % plots in hours

%%%%%%%%%%%%% SKW6.4 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Param set for SKW cells
%
%

Pskw = Phct;
ratio_Xskw = [.38 8.86 1.12 .93 .67 9.83 1.25 1.87 1.77 5.91 20]';
ratio_epsi_skw = [.11 2.83 .35 .34 .07 3.43 .85 .34 .16 2.64 20*.2]';

Xskw = ratio_Xskw .* Xbase;
epsi_skw = ratio_epsi_skw.*Xbase;

Pskw= SetParam(Pskw, ivar, Xskw);
Pskw.epsi = epsi_skw;


%%%%%%%%%%%%% T47D %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Param set for T47D cells 

Pt47d = Phct;
ratio_Xt47d = [.64 3.51 1.04 .48 2.25 1.42 4.64 1.34 1.39 .48 5.2]';
ratio_epsi_t47d = [.05 0.13 .02 .07 .67 0.12 2.00 .07 .03 0.06 20*.22]';

Xt47d = ratio_Xt47d .* Xbase;
epsi_t47d = ratio_epsi_t47d.*Xbase;

Pt47d= SetParam(Pt47d, ivar, Xt47d);
Pt47d.epsi = epsi_t47d;
  
%%%%%%%% Mutants OEBcl2

PhctOEB = OEB_mutation(Phct);
PskwOEB = OEB_mutation(Pskw);
Pt47dOEB = OEB_mutation(Pt47d);

% Mutants Delta-XIAP

PhctDELX = DELX_mutation(Phct);
PskwDELX = DELX_mutation(Pskw);
Pt47dDELX = DELX_mutation(Pt47d);

% Mutants Delta-XIAP OEBCL2

PhctOEB_DELX = DELX_mutation(PhctOEB);
PskwOEB_DELX = DELX_mutation(PskwOEB);
Pt47dOEB_DELX =DELX_mutation(Pt47dOEB);





