#include "dynamics.h"

#define F(v,temp,va) (temp)*(v)/((v)+((temp)*(va)))
#define F2(v,temp,va) (temp)*(v)*(v)/((v)*(v)+((va)*(va)*(temp)))
#define Fs(v,temp,va)  ((temp)*(va))/((v)+((temp)*(va)))
#define Fca(v,temp,va) ((temp)*(va)*(va))/((v)*(v)+((temp)*(va)*(va)))
#define square(ti,tf,tau) (heav((t)/(tau)-(ti))-heav((t)/(tau)-(tf)))


int f(realtype t, N_Vector x, N_Vector xdot, void *f_data) {

  // cout << "Entering f..." << endl;

  Fdata* data = (Fdata*) f_data;

  double b = Ith(x,0);
  double cai = Ith(x,1);
  double car = Ith(x,2);
  double d = Ith(x,3);
  double il6 = Ith(x,4);
  double ma = Ith(x,5);
  double mr = Ith(x,6);
  double n = Ith(x,7);
  double no = Ith(x,8);
  double nod = Ith(x,9);
  double o2 = Ith(x,10);
  double p = Ith(x,11);
  double pc = Ith(x,12);
  double pe = Ith(x,13);
  double tf = Ith(x,14);
  double th = Ith(x,15);
  double tnf = Ith(x,16);

  double ba = data->p[17];
  double caa = data->p[18];
  double cara = data->p[19];
  double da = data->p[20];
  double dur = data->p[21];
  double il6a = data->p[22];
  double k026 = data->p[23];
  double k6 = data->p[24];
  double k6m = data->p[25];
  double k6th = data->p[26];
  double k6tnf = data->p[27];
  double kab = data->p[28];
  double kape = data->p[29];
  double katnf = data->p[30];
  double kb = data->p[31];
  double kbh = data->p[32];
  double kbno = data->p[33];
  double kbth = data->p[34];
  double kbtnf = data->p[35];
  double kca = data->p[36];
  double kca6 = data->p[37];
  double kcam = data->p[38];
  double kcan = data->p[39];
  double kcano = data->p[40];
  double kcao2 = data->p[41];
  double kcapc = data->p[42];
  double kcar = data->p[43];
  double kcatnf = data->p[44];
  double kd = data->p[45];
  double kdb = data->p[46];
  double kdeq = data->p[47];
  double kdno = data->p[48];
  double kdo2 = data->p[49];
  double kds = data->p[50];
  double kdth = data->p[51];
  double kdtnf = data->p[52];
  double km6 = data->p[53];
  double kma = data->p[54];
  double kmd = data->p[55];
  double kmm = data->p[56];
  double kmp = data->p[57];
  double kmpe = data->p[58];
  double kmr = data->p[59];
  double kn = data->p[60];
  double kn6 = data->p[61];
  double knd = data->p[62];
  double knno = data->p[63];
  double kno = data->p[64];
  double kno2 = data->p[65];
  double kno6 = data->p[66];
  double knod = data->p[67];
  double knom = data->p[68];
  double knon = data->p[69];
  double knp = data->p[70];
  double knpe = data->p[71];
  double kns = data->p[72];
  double kntnf = data->p[73];
  double ko2 = data->p[74];
  double ko2m = data->p[75];
  double ko2n = data->p[76];
  double ko2np = data->p[77];
  double kpc = data->p[78];
  double kpcth = data->p[79];
  double kpe = data->p[80];
  double kpg = data->p[81];
  double kpm = data->p[82];
  double kpno = data->p[83];
  double kpo2 = data->p[84];
  double kps = data->p[85];
  double ktf = data->p[86];
  double ktf6 = data->p[87];
  double ktfpe = data->p[88];
  double ktftnf = data->p[89];
  double kth = data->p[90];
  double kthl = data->p[91];
  double kthn = data->p[92];
  double ktnf = data->p[93];
  double ktnfm = data->p[94];
  double ktnfmr = data->p[95];
  double ktnfn = data->p[96];
  double ktnftnf = data->p[97];
  double maa = data->p[98];
  double mra = data->p[99];
  double na = data->p[100];
  double noa = data->p[101];
  double o2a = data->p[102];
  double p0 = data->p[103];
  double pa = data->p[104];
  double pca = data->p[105];
  double pea = data->p[106];
  double rate = data->p[107];
  double resist = data->p[108];
  double sm = data->p[109];
  double sn = data->p[110];
  double spc = data->p[111];
  double spe = data->p[112];
  double tab = data->p[113];
  double tape = data->p[114];
  double tau = data->p[115];
  double tboff = data->p[116];
  double tbon = data->p[117];
  double tdoff = data->p[118];
  double tdon = data->p[119];
  double temp = data->p[120];
  double temp2 = data->p[121];
  double temp6 = data->p[122];
  double tempca = data->p[123];
  double temptnf = data->p[124];
  double temptnf6 = data->p[125];
  double temptnfca = data->p[126];
  double tfa = data->p[127];
  double tfatnf = data->p[128];
  double tha = data->p[129];
  double tiatnf = data->p[130];
  double tnfa = data->p[131];
  double tpcoff = data->p[132];
  double tpcon = data->p[133];

  double ca =  cai+kcapc*pc;

  Ith(xdot,0)  = (kb*(ba - b) - ((kbno/noa)*no*Fs(o2,temp,o2a) + (kbtnf/tnfa)*tnf + (kbth/tha)*th)*b - (b-kbh*ba)*square(tbon, tboff,tau))/tau;
  Ith(xdot,1)  = ((caa/cara)*car-kca*cai)/tau;
  Ith(xdot,2)  = (((kcan/na)*n+(kcam/maa)*ma)*(kcatnf*F(tnf,temptnf,tnfa)+kca6*F(il6,temp6,il6a)+kcano*F(no,temp,noa)+kcao2*F(o2,temp,o2a))*cara - kcar*car)/tau;
  Ith(xdot,3)  = (kdb*da*(1-(b/ba)) + da*(kdtnf/tnfa)*tnf + da*(kdo2/o2a)*o2 + da*(kdno/noa)*no*Fs(no,temp2,noa) + da*(kdeq/o2a)*o2*exp(-(10/(noa*noa*o2a*o2a))*pow((o2a*no-noa*o2),2)) +(da*kdth/tha)*th - kd*d + da*kds*square(tdon,tdoff,tau))/tau;
  Ith(xdot,4)  = ((k6m/maa)*ma*il6a*(1+k6th*F(th,temp,tha))*Fca(ca,temp2,caa) - k6*il6)/tau;
  Ith(xdot,5)  = (((kmp/pa)*p+(kmpe/pea)*pe+(kmd/da)*d)*(F2(tnf,temptnf,tnfa)+km6*F2(il6,temp6,il6a))*Fca(ca,tempca,caa)*(mr/mra)*maa - kma*ma)/tau;
  Ith(xdot,6)  = (-((kmp/pa)*p+(kmpe/pea)*pe+(kmd/da)*d)*(sm+F2(tnf,temptnf,tnfa)+km6*F2(il6,temp6,il6a))*Fca(ca,tempca,caa)*mr+kmm*mra*F(noa*pea*tnf+tnfa*pea*no+noa*tnfa*pe,temp,tnfa*noa*pea)-kmr*mr+sm*mra)/tau;
  Ith(xdot,7)  = ((knp*F(p,temp2,pa)+knpe*F(pe,temp2,pea)+kntnf*F(tnf,temptnf*2.0,tnfa)+kn6*F(il6,temp6,il6a)+knd*F(d,temp2,da))*n*(1-kns*(n/na))-((knno/noa)*no+(kno2/o2a)*o2)*n-kn*(Fs(tnf,temptnf,tnfa)+Fs(il6,temp6,il6a))*n+sn*na)/tau ;
  Ith(xdot,8)  = (kno*(nod*noa - no))/tau;
  Ith(xdot,9)  = (((knon/na)*n+(knom/maa)*ma)*Fca(ca,tempca,caa)*(F(tnf,temptnf,tnfa)+kno6*F(il6,temp6,il6a)) - knod*nod)/tau;
  Ith(xdot,10) = ((((ko2n/na)*n+(ko2m/maa)*ma)*(F(tnf,temptnf,tnfa)+k026*F(il6,temp6,il6a))*o2a+(ko2np/na)*n*F(p,temp,pa))*Fca(ca,tempca,caa) - ko2*o2)/tau;
  Ith(xdot,11) = (kpg*p*(1-kps*(p/pa))*heav(p/pa-p0)-((kpm/maa)*ma+(kpno/noa)*no+(kpo2/o2a)*o2+kab*heav(t/tau-tab)*exp(-(t/tau)*resist))*p)/tau;
  Ith(xdot,12) = ((kpcth/tha)*th*pca - kpc*pc + spc*pca*square(tpcon,tpcoff,tau))/tau;
  Ith(xdot,13) = (((kpm/maa)*ma+(kpno/noa)*no+(kpo2/o2a)*o2+kab*heav(t/tau-tab)*exp(-(t/tau)*resist))*(p/pa)*pea - kpe*pe - kape*heav(t/tau-tape)*pe - rate*heav(spe/rate-(t/tau)))/tau;
  Ith(xdot,14) = (((ktfpe/pea)*pe + (ktftnf/tnfa)*tnf+ (ktf6/il6a)*il6)*Fs(pc,temp,pca)*tfa - ktf*tf)/tau;
  Ith(xdot,15) = (tf*((kthl/tfa)*tha+(kthn/tfa)*th)  - kth*th)/tau;
  Ith(xdot,16) = (((ktnfn/na)*n+(ktnfmr/mra)*mr+(ktnfm/maa)*ma)*Fca(ca,temptnfca,caa)*tnfa*(1+ktnftnf*F(tnf,temptnf,tnfa)) - ktnf*tnf - katnf*square(tiatnf,tiatnf+dur,tau)*tnf)/tau;

  //  cout << "Leaving f ..." << endl;
 return(0);

 }


void InitFdata(void * &f_data, const mxArray * mxData){

  Fdata* data = (Fdata*) f_data;

  data->dimx = DIMX;
  data->dimu = 0;
  data->dimp = 134;
  data->p = mxGetPr(mxGetField(mxData,0,"p"));

}

int UpdateFdata(realtype t, N_Vector x, void * &f_data, int Ns, N_Vector* xS)
{ return 0;}

int g(realtype t, N_Vector x, realtype *gout, void *f_data) 
{ return 0;}

int Jac(long int N, DenseMat J, realtype t,
               N_Vector x, N_Vector fx, void *jac_data,
	N_Vector tmp1, N_Vector tmp2, N_Vector tmp3){

  return 0;
}

