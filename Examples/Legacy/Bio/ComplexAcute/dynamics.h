#ifndef DYNAMICS_H
#define DYNAMICS_H

#include "breach.h"

class Fdata: public FdataCommon {
 public:
  double *p;
};

int  f(realtype t, N_Vector y, N_Vector ydot, void *f_data);
void InitFdata(void * &f_data, const mxArray * mxData);
int  UpdateFdata(realtype t, N_Vector y, void * &f_data, int Ns, N_Vector* yS);
int g(realtype t, N_Vector x, realtype *gout, void *f_data);

inline double heav(double x) {
  double k = 1e6;
  return (1./(1.+exp(-2.*k*x))); 
}

#endif
