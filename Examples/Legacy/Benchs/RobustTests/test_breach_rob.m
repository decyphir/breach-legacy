%
% Stress test on breach robust algo
%
clear
rng(0);
InitBreach;

nphis = 10;
ntraj = 1;
sz_phis= 5;
n_samples = 1000;

% generates formulas
PHIS = cell(1,nphis);

%ops = {'alw'}
%ops = {'alw', 'and'};

for i=1:nphis
    PHIS{i} = rand_formula(sz_phis);
end

% generate trajectory
traj = test_traj(n_samples, nphis+1);
Tbr = zeros(1,nphis);

for i=1:nphis
    for j = 1:ntraj
        %  save;
        fprintf('%s ->', PHIS{i});
        [rob, T] = breach_rob(PHIS{i}, traj);
        res_rob(i, j) = rob(1);
        res_T(i,j) = T;
        fprintf(' rob: %6.3f Time: %6.3f \n',  rob(1), T);
        % pause;
    end
end
