function T = test_breach_online_rob_txt(st_phi, sz_traj)

% Always use the same random numbers
%s = RandStream('mcg16807','Seed',0);
%RandStream.setGlobalStream(s);

traj = test_traj(sz_traj, 2);
%st_phi = 'alw (alw (mu1))'
%st_phi = 'alw_[0,5] (mu1)'
%phi = 'mu1 and mu2'

[rob T] = breach_online_rob_txt(st_phi, traj);
return;
t_rob = rob(:,1)';
rob_phi = rob(:,2:3)';

rob_max = rob_phi(2,:);
rob_min = rob_phi(1,:);

bigM = 4;
rob_max(rob_max==inf) = bigM;
rob_min(rob_min==-inf) = -bigM;

figure
hold on;
eps_ = .01;

plot(traj.time, traj.X(1,:),'b', traj.time, 0*traj.X(1,:), 'k--');
%plot(traj.time, traj.X(1,:),'b', traj.time, 0*traj.X(1,:)+bigM+eps_, 'k--');
%plot(traj.time, traj.X(1,:),'b', traj.time, 0*traj.X(1,:)-bigM-eps_, 'k--');


text(0.1, bigM+eps_-.1, '+\infty', 'FontSize', 24); 
text(0.1, -bigM-eps_+.2, '-\infty','FontSize', 24); 

stairs(t_rob,rob_max,'r');
stairs(t_rob,rob_min, 'k')


