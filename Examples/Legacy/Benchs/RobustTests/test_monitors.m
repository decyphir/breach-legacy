function failed = test_monitors(depth)
    
    traj = test_traj(20, 2);

    %tau  = traj.time; 
    dtau = traj.time(2)- traj.time(1)/10;     
     
    failed = {};
 
    
    % defines predicates and formula
    npreds = 2;
    ops = {'', 'not','or','and','evI','alwI','untilI'};
    
    PHIs = all_formulas(npreds,depth, ops);
    
    for i = 1:npreds 
        STL_Formula( ['mu' num2str(i)] , ['y' num2str(i) '[t]>0']); 
    end
 
    % defines system 
    Sys = CreateExternSystem('test', npreds, 0);
    P   = CreateParamSet(Sys);
    
    for i_phi = 1:numel(PHIs) 
        
        st_phi = PHIs{i_phi};
    
        phi = STL_Formula('phi', st_phi);
        rob_classic =  STL_EvalClassic(Sys, phi,P,  traj, 0, dtau);
        rob_online  =  STL_EvalClassicOnline(Sys, phi,P,  traj, 0, dtau);
       
        fprintf('%s\nrho=%g  rho_up = %g, rho_low=%g  ', st_phi, rob_classic(1), rob_online(1,1),rob_online(2,1))
        if (rob_online(2,1) - rob_online(1,1) > 0)
            fprintf('PARTIAL\n');
        elseif abs(rob_online(2,1)-rob_classic(1)) > 0
            fprintf('ERROR\n');
            failed{end+1} = st_phi;
        else
            fprintf('PASSED\n')
        end
        
   end
  
