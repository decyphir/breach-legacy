clear;
%s = RandStream('mcg16807','Seed',0);
%RandStream.setGlobalStream(s);
InitBreach;

nphis = 10;
sz_phis= 4; 
n_samples = 20:200:1000;

% generates formulas

PHIS = cell(1,nphis); 

for iphi=1:nphis
    PHI_BREACH{iphi} = rand_formula(sz_phis, {'evI', 'alwI', 'or' } );
end

% generate trajectory

for it = 1:numel(n_samples)
    traj(it) = test_traj(n_samples(it), nphis+1);
end

Tnaive = zeros(1,nphis);
Talgo = zeros(1,nphis);

for it = 1:numel(traj)
    for iphi=1:nphis
        [~, Talgo(it,iphi)] = breach_online_rob_txt(PHI_BREACH{iphi}, traj(it));
        [~, Tnaive(it,iphi)] = breach_online_rob_naive(PHI_BREACH{iphi}, traj(it));
        % fprintf('%s -> Tnaive: %6.3f Talgo: %6.3f \n', PHI_BREACH{i}, Tnaive(i), Talgo(i));
    end
    Talgo_ave(it) = mean(Talgo(it,:));
    Tnaive_ave(it) = mean(Tnaive(it,:));
    fprintf('Traj %g: Average -> Tnaive: %6.3f Talgo: %6.3f \n',it, mean(Tnaive(it,:)), mean(Talgo(it,:)));
end
