% Stress test on online algo
clear
InitBreach;
compile_stl_mex;

%% Always use the same random numbers
seed=0;
s = RandStream('mcg16807','Seed',seed);
RandStream.setGlobalStream(s);

%% generates formulas
nphis = 20;
sz_phis= 3;

% defines predicates and formula
for i = 1:2*sz_phis
    STL_Formula( ['mu' num2str(i)] , ['x' num2str(i) '[t]>0']);
end

PHIS = cell(1,nphis);
for iphi=1:nphis
    st_phi =  rand_formula(sz_phis, {'evI', 'alwI', 'and', 'or'} );
    PHI_BREACH{iphi} = STL_Formula('phi', st_phi);
end

% generate trajectory
%n_samples = 20:20:100;

n_samples = [20 100] ;
for it = 1:numel(n_samples)
    trajs(it) = test_traj(n_samples(it), 2*sz_phis);
end

report_bug = []
for iphi=1:nphis
    phi = PHI_BREACH{iphi}
    for it = 1:numel(trajs)
        reports(it,iphi)= breach_online_mex(trajs(it), phi);%,1); drawnow
        if ~all(reports(it,iphi).status(1:end))
            if isempty(report_bug)
                report_bug = reports(it,iphi);
            else
                report_bug(end+1)= reports(it,iphi);               
            end
            warning(interpret_status(report_bug(end)));
        end
    end
end