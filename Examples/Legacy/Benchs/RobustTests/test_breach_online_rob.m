function test_breach_online_rob(st_phi, seed)

if nargin==1
    seed = 0;    
end


% Always use the same random numbers
s = RandStream('mcg16807','Seed',seed);
RandStream.setGlobalStream(s);

traj = test_traj(25, 2);
%st_phi = 'alw (alw (mu1))'
%st_phi = 'alw_[0,5] (mu1)'
%phi = 'mu1 and mu2'

dtau = (traj.time(2)- traj.time(1))/100; 

%rob_phi = breach_online_rob(phi, traj, tau, dtau)

rob_phi = [inf;-inf];

for it_f = 2:numel(traj.time)

    traji.time = traj.time(1:it_f);   
    traji.X = traj.X(:,1:it_f);
    rob_phii = breach_online_rob(st_phi, traji, 0, dtau, false);
    rob_phi(:,end+1)= rob_phii;

end

rob_max = rob_phi(1,:);
rob_min = rob_phi(2,:);

bigM = 4;
rob_max(rob_max==inf) = bigM;
rob_min(rob_min==-inf) = -bigM;

figure
hold on;
eps_ = .01;

plot(traj.time, traj.X(1,:),'b', traj.time, 0*traj.X(1,:), 'k--');
plot(traj.time, traj.X(2,:),'g');
%plot(traj.time, traj.X(1,:),'b', traj.time, 0*traj.X(1,:)-bigM-eps_, 'k--');

text(0.1, bigM+eps_-.1, '+\infty', 'FontSize', 24); 
text(0.1, -bigM-eps_+.2, '-\infty','FontSize', 24); 

stairs(traji.time(1:end),rob_max,'r');
stairs(traji.time(1:end),rob_min, 'k')


