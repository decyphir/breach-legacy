function traji = trim_traj(traj, tmax)

it_f = find(traj.time<=tmax);
traji.time = traj.time(1:it_f(end));
traji.X = traj.X(:,1:it_f(end));
