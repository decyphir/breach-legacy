%% Add paths necessary for the tests
cr__ = pwd;
addpath([cr__ filesep 'trace_gen' filesep]);
addpath([cr__ filesep 'prop_gen' filesep]);
addpath([cr__ filesep 'monitors' filesep]);
clear cr__
