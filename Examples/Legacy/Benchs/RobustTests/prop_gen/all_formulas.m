function [ st, preds, Is ] = all_formulas(preds, depth, op, I)
%ALL_FORMULAS Creates all formulas of depth depth  and predicates in preds
%
% Synopsis: [ st_breach, preds ] = all_formula(nb_preds, depth, op)
%
% by default op is a subset  {'or','and','=>','ev','alw', 'until', 'evI', 'alwI',
% 'untilI'} (contains all operators if not specified)
%
% returns all formulas with predicates mu1, ..., mun in Breach format
%

if (~exist('op', 'var'))||isempty(op)
    op = {'', 'not','or','and','=>','ev','alw','until','evI','alwI','untilI'};
end

if (~exist('I', 'var'))
    I = '[0,1]';
end


if (depth==0)
    if isnumeric(preds)
        nb_preds = preds;
        preds = {};
        for k = 1:nb_preds
            preds= {preds{:}, ['mu' num2str(k)]};
        end
    end
    
    for k=1:numel(preds)
        st = preds;
    end
    
else
    st={};
    for k_op = 1:numel(op)
        switch op{k_op}
            %% No operator
            case '' 
                st1 = all_formulas(preds, depth-1,op);
                for k_st1 = 1:numel(st1)
                    st_pred  =  [st1{k_st1}];
                    st = {st{:}, st_pred};
                end
            
            %% Unary operators
            case 'not'
                st1 = all_formulas(preds, depth-1,op);
                for k_st1 = 1:numel(st1)
                    st_not  =  ['not (' st1{k_st1} ')'];
                    st = {st{:}, st_not};
                end
                
            case 'ev'
                st1 = all_formulas(preds, depth-1,op);
                for k_st1 = 1:numel(st1)
                    st_ev  =  ['ev (' st1{k_st1} ')'];
                    st = {st{:}, st_ev};
                end
                
            case 'alw'
                st1 = all_formulas(preds, depth-1,op);
                for k_st1 = 1:numel(st1)
                    st_alw  =  ['alw (' st1{k_st1} ')'];
                    st = {st{:}, st_alw};
                end
                
            case 'evI'
                st1 = all_formulas(preds, depth-1,op);
                for k_st1 = 1:numel(st1)
                    st_ev  =  ['ev_' I ' (' st1{k_st1} ')'];
                    st = {st{:}, st_ev};
                end
                
            case 'alwI'
                st1 = all_formulas(preds, depth-1,op);
                for k_st1 = 1:numel(st1)
                    st_alw  =  ['alw_' I ' (' st1{k_st1} ')'];
                    st = {st{:}, st_alw};
                end
                
                %% Binary operators (symmetric)
            case 'and'
                st1 = all_formulas(preds, depth-1, op);
                for k_st1 = 1:numel(st1)
                    for k_st2 = 1:numel(st1)
                        if k_st2 >= k_st1  % eliminates symmetric formulas
                            st_and = ['(' st1{k_st1}  ') and (' st1{k_st2} ')'];
                            st = {st{:}, st_and};
                        end
                    end
                end
                
            case 'or'
                st1 = all_formulas(preds, depth-1, op);
                for k_st1 = 1:numel(st1)
                    for k_st2 = 1:numel(st1)
                        if k_st2 >= k_st1  % eliminates symmetric formulas
                            st_or = ['(' st1{k_st1}  ') or (' st1{k_st2} ')'];
                            st = {st{:}, st_or};
                        end
                    end
                end
                
                %% Binary operators (assymmetric)
                
            case '=>'
                st1 = all_formulas(preds, depth-1, op);
                for k_st1 = 1:numel(st1)
                    for k_st2 = 1:numel(st1)
                        st_implies = ['(' st1{k_st1}  ') => (' st1{k_st2} ')'];
                        st = {st{:}, st_and};
                    end
                end
                
            case 'until'
                st1 = all_formulas(preds, depth-1, op);
                for k_st1 = 1:numel(st1)
                    for k_st2 = 1:numel(st1)
                        st_until = ['(' st1{k_st1}  ') until (' st1{k_st2} ')'];
                        st = {st{:}, st_until};
                    end
                end
                
            case 'untilI'
                st1 = all_formulas(preds, depth-1, op);
                for k_st1 = 1:numel(st1)
                    for k_st2 = 1:numel(st1)
                        st_until = ['(' st1{k_st1}  ') until_' I '(' st1{k_st2} ')'];
                        st = {st{:}, st_until};
                    end
                end
                
        end
        
    end
    
end
