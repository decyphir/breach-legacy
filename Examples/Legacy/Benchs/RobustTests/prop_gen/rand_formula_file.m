function [st_breach] =  rand_formula_file(fname, size_phi, operators)

    fid = fopen(fname,'w');   
 
    if (fid == -1) 
        error(['Can''t open file ' fname]);
    end
    
    if nargin == 2
        operators = {'or', 'and', 'ev', 'alw', 'until', 'evI', 'alwI', 'untilI'};
    end
        
    
    [st_breach, ~, preds] = rand_formula(size_phi, operators);
    
    signals =  {'x', 'y', 'z', 'pitch', 'roll', ...
    'yaw', 'dist', 'angle', 'ax', 'ay', 'az', ...
    'bump_l', 'bump_r', 'drop_l', 'drop_r', ...
    'drop_f', 'cliff_l', 'cliff_fl', 'cliff_fr', ...
    'cliff_r', 'lws', 'rws', 'state'};

    for ii = 1:numel(preds)
        fprintf(fid, 'mu%i := %s[t]>0. \n', ii, signals{randi(numel(signals))});
    end
    
    fprintf(fid, 'phi:= %s', st_breach);
    fclose(fid)
