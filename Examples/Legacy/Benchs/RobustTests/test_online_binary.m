clear
close all;

traj = test_traj(20, 2);
traj.X(1, :) = traj.X(1, :)+ 2.1;
traj.X(2, :) = traj.X(2, :)- 2.1;

st_phi = '(mu1) until_[2, 5] (mu2)'

tau  = traj.time; 
dtau = traj.time(2)- traj.time(1); 


%rob_phi = breach_online_rob(phi, traj, tau, dtau)

rob_phi = [];

for it_f = 2:numel(traj.time)

    traji.time = traj.time(1:it_f);   
    traji.X = traj.X(:,1:it_f);
    rob_phii = breach_online_rob(st_phi, traji, 0, dtau);
    rob_phi(:,end+1)= rob_phii;

end

rob_max = rob_phi(1,:);
rob_min = rob_phi(2,:);

bigM = 5;
rob_max(rob_max==inf) = bigM;
rob_min(rob_min==-inf) = -bigM;

figure
hold on;
eps_ = .01;

plot(traj.time, traj.X(1,:),'b', traj.time, 0*traj.X(1,:), 'k--');
plot(traj.time, traj.X(2,:),'m');

plot(traj.time, traj.X(1,:),'b', traj.time, 0*traj.X(1,:)+bigM, 'k--');
plot(traj.time, traj.X(1,:),'b', traj.time, 0*traj.X(1,:)-bigM, 'k--');
text(0.1, bigM+eps_-.1, '+\infty', 'FontSize', 24); 
text(0.1, -bigM-eps_+.2, '-\infty','FontSize', 24); 

stairs(traji.time(1:end-1),rob_max,'r');
stairs(traji.time(1:end-1), rob_min, 'k')
set(gca, 'YLim', [-bigM-.1, bigM+.1]);

%figure; 


