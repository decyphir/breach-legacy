function [rob_low, tau_low,rob_up, tau_up, T] = breach_online_mex_rob(st_phi, traj)
% BREACH_ROB Simple function computing the robustness of an STL formula
%            using Breach algorithm
%
%  [rob_low, tau_low,rob_up, tau_up, T] = breach_online_mex_rob(phi, traj)
%
%   Note: - each atomic predicate in phi are assumed to be of the form mu_i>0
%         - the trajectory should contain as many dimensions as the number
%         of predicates in phi
%
%   returns the robustness value rob and computation time T
%
%  ex: 
%
%  [phi, ~,  preds] = rand_formula(2)
%  traj = test_traj(1000, numel(preds))
%  
%  [rob T] = breach_rob(phi, traj)

  if (nargin==1)
      traj = test_traj(20, 2);
  end
  
  % defines predicates and formula
  npreds = numel(regexp(st_phi,'mu'));
  for i = 1:npreds 
    STL_Formula( ['mu' num2str(i)] , ['x' num2str(i) '[t]>0']); 
  end
  phi = STL_Formula('phi', st_phi);
 
  % defines system 
  st_phi = disp(phi)
  tic;
  [tau, rob, tau, rob_low, tau_low, rob_up, tau_up]= stl_eval_mex(st_phi, [traj.time; traj.X]); 
  T = toc;
  
