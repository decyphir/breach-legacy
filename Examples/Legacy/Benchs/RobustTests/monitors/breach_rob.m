function [rob, T] = breach_rob(phi, traj)
% BREACH_ROB Simple function computing the robustness of an STL formula
%            using Breach algorithm
%
%  [rob T] = breach_rob(phi, traj)
%
%   Note: - each atomic predicate in phi are assumed to be of the form mu_i>0
%         - the trajectory should contain as many dimensions as the number
%         of predicates in phi
%
%   returns the robustness value rob and computation time T
%
%  ex: 
%
%  [phi, ~,  preds] = rand_formula(2)
%  traj = test_traj(1000, numel(preds))
%  
%  [rob T] = breach_rob(phi, traj)


  % defines predicates and formula
  npreds = numel(regexp(phi,'mu'));
  for i = 1:npreds 
    STL_Formula( ['mu' num2str(i)] , ['x' num2str(i) '[t]>0']); 
  end
  phi = STL_Formula('phi', phi);
 
  % defines system 
  Sys = CreateExternSystem('test', npreds, 0);
  P = CreateParamSet(Sys);
  
  tic;
  [rob, tau]= STL_EvalThom(Sys, phi,P,  traj);
  T = toc;
  
