function [rob, T] = dp_talirob(phi, traj)
%DP_TALIROB simple function computing the robustness of an STL formula
%           using DP_Taliro algorithm
%
%  [rob, T] = dp_talirob(phi, traj)
%
%   Note: - each atomic predicate in phi are assumed to be of the form mu_i>0
%         - the trajectory should contain as many dimensions as the number
%         of predicates in phi
%
%   returns the robustness value rob and computation time T
%
%  ex: 
%
%  [~, phi, preds] = rand_formula(2)
%  traj = test_traj(1000, numel(preds))
%  
%  [rob T] = dp_talirob(phi, traj)
%

seqS = traj.X';
seqT = traj.time';

n = size(traj.X,1);
for i = 1:n
    pred(i).str = ['mu' num2str(i)];
    pred(i).A = zeros(n);
    pred(i).A(i,i) = -1;
    pred(i).b = zeros(n,1);
end

tic;
rob = dp_taliro(phi, pred, seqS, seqT);
T = toc;

end
