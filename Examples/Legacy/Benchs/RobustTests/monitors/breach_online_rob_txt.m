function [rob, T] = breach_online_rob_txt(st_phi, traj, t,dt)
% BREACH_ROB Simple function computing the robustness of an STL formula
%            using Breach algorithm
%
%  [rob T] = breach_rob(phi, traj)
%
%   Note: - each atomic predicate in phi are assumed to be of the form mu_i>0
%         - the trajectory should contain as many dimensions as the number
%         of predicates in phi
%
%   returns the robustness value rob and computation time T
%
%  ex: 
%
%  [phi, ~,  preds] = rand_formula(2)
%  traj = test_traj(1000, numel(preds))
%  
%  [rob T] = breach_online_rob(phi, traj)


  dlmwrite('trace.txt', [traj.time' traj.X'], ' ');

  % defines predicates and formula
  npreds = numel(regexp(st_phi,'mu'));
  for i = 1:npreds 
    STL_Formula( ['mu' num2str(i)] , ['x' num2str(i) '[t]>0']); 
  end
  
  phi = STL_Formula('phi', st_phi);
  
  st_phi = disp(phi);
  fid = fopen('formula.stl', 'w+t');
  
  fprintf(fid, 'signal ');
  
  for i=1:npreds-1
     fprintf(fid,['x' num2str(i) ', ']); 
  end
  fprintf(fid,['x' num2str(npreds)]);
  
  fprintf(fid,'\n');
  fprintf(fid, 'phi:=');
  fprintf(fid, st_phi);
  fclose(fid);
  
  tic;
  system('./stl_monitor -t trace.txt formula.stl > rob.out');
  T = toc;
  rob = load('rob.out');
  
  
