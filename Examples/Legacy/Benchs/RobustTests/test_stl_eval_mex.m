function [failed, traj_failed, rob_failed] = test_stl_eval_mex(depth)
    
    addpath('/Users/alex/workspace/STLMonitor/src');
    traj = test_traj(20, 2);

    %tau  = traj.time; 
    dtau = traj.time(2)- traj.time(1)/10;     
     
    failed = {};
    traj_failed = traj(1:0);
    rob_failed.rob = 0;
    rob_failed.rob_mex = 0;
    rob_failed = rob_failed(1:0); %dirty but who cares ?
     
    % defines predicates and formula
    npreds = 2;
    ops = {'', 'not','or','and','evI','alwI','untilI'};
    
    PHIs = all_formulas(npreds,depth, ops);
    
    for i = 1:npreds 
        STL_Formula( ['mu' num2str(i)] , ['x' num2str(i) '[t]>0']); 
    end
 
    % defines system 
    Sys = CreateExternSystem('test', npreds, 0);
    P   = CreateParamSet(Sys);
    
    for i_phi = 1:numel(PHIs) 
        
        st_phi = PHIs{i_phi};
    
        phi = STL_Formula('phi', st_phi);
        rob =  STL_Eval(Sys, phi,P,  traj, 0, dtau);   
        fprintf('%s\n', st_phi);
        %pause;
        [t_mex, rob_mex]  =  stl_eval_mex(disp(phi), [traj.time; traj.X]);
          
        fprintf('rho=%g  rho_mex = %g  ', rob, rob_mex(1));
        if abs(rob-rob_mex(1)) > 0
            fprintf('ERROR\n');
            traj_failed(end+1) = traj;
            failed{end+1} = st_phi;
            rob_failed(end+1).rob = rob;
            rob_failed(end+1).rob_mex = rob_mex;
        else
            fprintf('PASSED\n')
        end
        
   end
  
