%
%  This scripts compares the performance of DP_TaLiRo, FW_TaLiRo and Breach
%  algorithm for computing the robust satifaction of a set of formulas for
%  a given trajectory with a number of samples.
%

InitBreach;

nphis = 10;
sz_phis= 2; 
n_samples = 1000;

% generates formulas


PHIS_TALIRO= cell(1,nphis); 
PHIS_BREACH= cell(1,nphis); 

for i=1:nphis
  [ PHI_BREACH{i},PHI_TALIRO{i}] = rand_formula(sz_phis);
end

% generate trajectory
traj = test_traj(n_samples, nphis+1);

Tdp = zeros(1,nphis);
Tfw = zeros(1,nphis);
Tbr = zeros(1,nphis);

for i=1:nphis
  [~, Tdp(i)] = dp_talirob(PHI_TALIRO{i}, traj);
  [~, Tfw(i)] = fw_talirob(PHI_TALIRO{i}, traj);
  [~, Tbr(i)] = breach_rob(PHI_BREACH{i}, traj);
fprintf('%s -> Tdp: %6.3f Tfw: %6.3f Tbr: %6.3f \n', PHI_TALIRO{i}, Tdp(i), Tfw(i), Tbr(i));

end

fprintf('Average -> Tdp: %6.3f Tfw: %6.3f Tbr: %6.3f \n', mean(Tdp), mean(Tfw), mean(Tbr));

